using Clonk;
using UnityEngine;

public class ItemPickup : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        Entity enteringEntity = other.gameObject.GetComponent<Entity>();

        if (enteringEntity != null && enteringEntity.Team == 1)
        {
            StartCoroutine(PowerUpManager.instance.ChoosePowerup());

            gameObject.SetActive(false);
        }
    }
}
