using UnityEngine;

namespace DitzelGames.FastIK
{
    public class PickupRotator : MonoBehaviour
    {

        void Update()
        {
            //just rotate the object
            transform.Rotate(0, 0, Time.deltaTime * 90);
        }
    }
}