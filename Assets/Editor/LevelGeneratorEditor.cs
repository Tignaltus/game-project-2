using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Clonk.LevelGenerator
{

    [CustomEditor(typeof(LevelGenerator))]
    public class LevelGeneratorEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            LevelGenerator myTarget = (LevelGenerator)target;

            if (GUILayout.Button("Generate Level"))
            {
                myTarget.GenerateLevel();
            }

            if (GUILayout.Button("Trim Loose"))
            {
                myTarget.TrimQuotaEnds();
            }

            if (GUILayout.Button("Clear Level"))
            {
                myTarget.ClearLevel();
            }
        }
    }

}