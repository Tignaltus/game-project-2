using System;
using UnityEngine;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using Object = System.Object;


namespace Clonk.EditorExtensions
{
    public class PowerUpEditorWindow : EditorWindow
    {
        [MenuItem("Tools/Powerup Editor Window")]
        public static void ShowWindow()
        {
            var window = GetWindow<PowerUpEditorWindow>();
            window.titleContent = new GUIContent("Powerup Editor");
            window.minSize = new Vector2(800, 600);
        }

        private void OnEnable()
        {
            ReloadWindow();
        }

        private void ReloadWindow()
        {
            VisualTreeAsset original =
                AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Editor/PowerUpEditorWindow.uxml");
            
            TemplateContainer treeAsset = original.CloneTree();
            rootVisualElement.Add(treeAsset);
            
            StyleSheet styleSheet = 
                AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Editor/PowerUpEditorStyles.uss");
            rootVisualElement.styleSheets.Add(styleSheet);

            
            Action action = () =>
            {
                var nameTextField = rootVisualElement.Q<TextField>("powerup-new-name");
                CreateNewPowerUp(nameTextField.text);
            };

            var powerupButton = rootVisualElement.Q<Button>("powerup-button");
            powerupButton.RegisterCallback<MouseUpEvent>((evt) => action());
            
            
            
            CreatePowerupListView();
        }

        private void CreateNewPowerUp(string _name)
        {
            PowerUpData asset = CreateInstance<PowerUpData>();
            asset.name = _name;
            AssetDatabase.CreateAsset(asset, $"Assets/PowerUps/{_name}.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();

            Selection.activeObject = asset;
            CreatePowerupListView();
        }
        
        
        
        private void CreatePowerupListView()
        {
            FindAllPowerUps(out PowerUpData[] powerUps);

            ListView powerUpList = rootVisualElement.Query<ListView>("powerup-list").First();
            powerUpList.makeItem = () => new Label();
            powerUpList.bindItem = (element, i) => (element as Label).text = powerUps[i].name;


            powerUpList.itemsSource = powerUps;
            powerUpList.itemHeight = 16;
            powerUpList.selectionType = SelectionType.Single;


            powerUpList.onSelectionChange += (enumerable) =>
            {
                foreach (Object it in enumerable)
                {
                    Box powerUpInfoBox = rootVisualElement.Query<Box>("powerup-info");
                    powerUpInfoBox.Clear();

                    PowerUpData powerUp = it as PowerUpData;

                    SerializedObject serializedPowerUp = new SerializedObject(powerUp);
                    SerializedProperty powerUpProperty = serializedPowerUp.GetIterator();
                    powerUpProperty.Next(true);

                    while (powerUpProperty.NextVisible(false))
                    {
                        PropertyField prop = new PropertyField(powerUpProperty);

                        prop.SetEnabled(powerUpProperty.name != "m_Script");
                        prop.Bind(serializedPowerUp);
                        powerUpInfoBox.Add(prop);

                        if (powerUpProperty.name == "powerUpImage")
                        {
                            prop.RegisterCallback<ChangeEvent<UnityEngine.Object>>((changeEvt) =>
                                LoadPowerUpImage(powerUp.powerUpImage.texture));
                        }

                    }
                    
                    LoadPowerUpImage(powerUp.powerUpImage.texture);
                }
            };
            

        }




        private void FindAllPowerUps(out PowerUpData[] powerUps)
        {
            var guids = AssetDatabase.FindAssets("t:PowerUpData");

            powerUps = new PowerUpData[guids.Length];
            
            
            for(int i = 0; i < guids.Length; i++)
            {
                var path = AssetDatabase.GUIDToAssetPath(guids[i]);
                powerUps[i] = AssetDatabase.LoadAssetAtPath<PowerUpData>(path);
            }

        }

        private void LoadPowerUpImage(Texture texture)
        {
            var PowerUpPreviewImage = rootVisualElement.Query<Image>("preview").First();
            PowerUpPreviewImage.image = texture;
        }





    }
}