using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clonk.EditorExtensions
{

    [System.Serializable]
    public struct PowerUpStat
    {
        public PowerUpData powerUpData;
        [Range(0, 100)] public int rarity;

    }

    [CreateAssetMenu(menuName = "Scriptable Objects/PowerUpPack")]
    public class PowerUpPack : ScriptableObject
    {

        //public PowerUpData spawnRoom;



        public List<PowerUpStat> standardPowerUps;

        public List<PowerUpStat> specialPowerUps;

        //public GameObject doorPrefab;

        public PowerUpData GetRandomPowerUp()
        {
            List<PowerUpStat> weightedPowerUpList = new List<PowerUpStat>();

            foreach (PowerUpStat powerUpStat in standardPowerUps)
            {
                for (int i = 0; i < powerUpStat.rarity; i++)
                {
                    weightedPowerUpList.Add(powerUpStat);
                }
            }

            return weightedPowerUpList.GetRandomItem().powerUpData;
        }
    }

}
