Shader "Universal Render Pipeline/Custom/WOS" {
    Properties{
        _Thickness("Thickness", Float) = 1
        _Color("Color", Color) = (1, 1, 1, 1)
        [ShowAsVector2] _ReferenceResolution("Reference Resolution", Vector) = (1920, 1080, 0, 0)
        _DistanceScaling("DistanceScaling", Float) = 1
    }
    SubShader{
        Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" }

        Pass {
            Name "Outlines"
            // Cull front faces
            Cull Front

            HLSLPROGRAM
            // Standard URP requirements
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x

            // Register our functions
            #pragma vertex Vertex
            #pragma fragment Fragment

            #pragma multi_compile_fog

            // Include our logic file
            #include "WOS.hlsl"    

            ENDHLSL
        }
    }
}