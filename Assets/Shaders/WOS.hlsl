#ifndef WOS_INCLUDED
#define WOS_INCLUDED

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

// Data from the meshes
struct Attributes {
    float4 positionOS       : POSITION; // Position in object space
    float3 normalOS         : NORMAL; // Normal vector in object space
    float3 outlineNormals : TEXCOORD4;
};

// Output from the vertex function and input to the fragment function
struct VertexOutput {
    float4 positionCS   : SV_POSITION;
    float fogFactor     : TEXCOORD3;
};

// Properties
float _Thickness;
float4 _Color;
float2 _ReferenceResolution;

VertexOutput Vertex(Attributes input) {
    VertexOutput output = (VertexOutput)0;

    float4 positionCS = GetVertexPositionInputs(input.positionOS.xyz).positionCS;

    float fogFactor = ComputeFogFactor(positionCS.z);

    output.fogFactor = fogFactor;

    float3 normal = input.normalOS;

    if(length(input.outlineNormals) != 0)
    {
    normal = input.outlineNormals;
    }


    float3 normalCS = mul((float3x3) UNITY_MATRIX_VP, mul((float3x3) UNITY_MATRIX_M, normal));


    float distanceScaling = 1;

    float2 resolutionScaled;

    resolutionScaled.x = _ScreenParams.x * (_ReferenceResolution.x / _ScreenParams.x);
    resolutionScaled.y = _ScreenParams.y * (_ReferenceResolution.y / _ScreenParams.y);

    float2 normalOffset = normalize(normalCS.xy) / resolutionScaled.xy * _Thickness * positionCS.w * 2;
    
    positionCS.xy += normalOffset;


    output.positionCS = positionCS;


    return output;
}

half3 Fragment(VertexOutput input) : SV_Target {
    
    half3 col = MixFog(_Color, input.fogFactor);

    return col;
}

#endif