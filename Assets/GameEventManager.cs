using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// G = Global event.
/// L = Local event.
/// </summary>
public enum GameEvent
{
    G_RegenerateLevel,
    G_ClearEnemies
}


public class GameEventManager : MonoBehaviour
{

    public static void FireEvent(GameEvent gameEvent)
    {

    }

}
