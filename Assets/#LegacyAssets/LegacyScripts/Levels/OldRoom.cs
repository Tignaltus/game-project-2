using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using Clonk;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.AI;
//using NavMeshBuilder = UnityEditor.AI.NavMeshBuilder;
using Random = UnityEngine.Random;

public class OldRoom : MonoBehaviour
{
    public DoorOpener[] doors;
    public CharacterScriptableObject[] spawnableEnemies;
    public SpawnPoint[] spawnPoints;
    public SpawnPoint bossSpawnPoint;
    //public LightFader LightFader;
    public BoxCollider roomTrigger;

    [Range(1, 10)]public int roomDifficulty;
    private bool roomPlaced = false;
    private bool isDespawning = false;
    private int roomIndex;


    private void Start()
    {
        List<SpawnPoint> newSpawnPoints = new List<SpawnPoint>();
        foreach (var spawnPoint in spawnPoints)
        {
            if (!newSpawnPoints.Contains(spawnPoint))
            {
                newSpawnPoints.Add(spawnPoint);
            }
        }

        spawnPoints = newSpawnPoints.ToArray();
    }

    public void Initialize()
    {
        roomPlaced = true;
    }

    public void DespawnRoom()
    {
        if (isDespawning) return;
        isDespawning = true;
        StartCoroutine(LowerRoomCoroutine());
    }

    public void spawnBossRoom()
    {
        StartCoroutine(LowerBossRoomCoroutine());
    }
    
    IEnumerator LowerRoomCoroutine()
    {
        float spd = 1;
        while (transform.localPosition.y > -30)
        {
            spd += Time.deltaTime*2;
            transform.localPosition += Vector3.down * (spd * Time.deltaTime);
            yield return null;
        }
        Destroy(gameObject);
    }

    IEnumerator LowerBossRoomCoroutine()
    {
        roomPlaced = false;
        transform.localPosition += new Vector3(0, 30, 0);
        
        float spd = 1;
        while (transform.localPosition.y > 0)
        {
            spd += Time.deltaTime*2;
            transform.localPosition += Vector3.down * (spd * Time.deltaTime);
            yield return null;
        }
        //LightFader.SetFade(true);
        roomPlaced = true;
        Debug.Log("Room placed: " + roomPlaced);
    }

    public void EnemyDataSend(bool timesUp)
    {
        if (timesUp)
        {
            GameManager.instance.AIManager.CreateBoss(bossSpawnPoint.spawnableEnemies[Random.Range(0, bossSpawnPoint.spawnableEnemies.Length)],
                bossSpawnPoint.transform.position.Flatten(0), Quaternion.identity);
        }
        else
        {
            foreach (var point in spawnPoints)
            {
                Vector3 spawnPosition = point.transform.position.Flatten(0);
                GameManager.instance.AIManager.CreateEnemy(point.spawnableEnemies[Random.Range(0, point.spawnableEnemies.Length)], spawnPosition, Quaternion.identity);
            }
        }

    }
    public void RoomCheck(bool NoEnemies)
    {
        if(doors.Length > 1)
        {
            doors[1].Open = NoEnemies;
        }
    }

    public int GetIndex
    {
        get => roomIndex;
        set => roomIndex = value;
    }

    public bool IsRoomPlaced()
    {
        return roomPlaced;
    }
}
