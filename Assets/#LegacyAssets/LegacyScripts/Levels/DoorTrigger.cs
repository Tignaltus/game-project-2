using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{

    public GameObject spawnPoints;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Entity>().Team == 1)
        {
            StartCoroutine(PowerUpManager.instance.ChoosePowerup());

            foreach (SpawnPoint point in spawnPoints.GetComponentsInChildren<SpawnPoint>())
            {
                Vector3 spawnPosition = point.transform.position.Flatten(0);
                GameManager.instance.AIManager.CreateEnemy(point.spawnableEnemies[Random.Range(0, point.spawnableEnemies.Length)], spawnPosition, Quaternion.identity);
            }

            Destroy(gameObject);
        }
    }
}
