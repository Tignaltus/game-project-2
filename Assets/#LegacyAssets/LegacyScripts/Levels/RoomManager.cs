using System;
using System.Collections;
using System.Collections.Generic;
using Clonk;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class RoomManager : MonoBehaviour
{
    public List<OldRoom> roomPrefabList;
    public List<OldRoom> bossRoomList;
    public List<OldRoom> placedRooms; //<-- In case we want to use it
    public OldRoom startingRoom;
    public Disp_Room_Info displayRoomInfo;
    public Countdown_Clock countdownClock;

    private OldRoom currentRoom;
    private OldRoom previousRoom;
    
    private int amtOfRooms;
    private bool timerOut;
    private bool callSwitch = true;
    
    private void Awake()
    {
        //StartGame();
    }

    public void StartGame()
    {
        countdownClock.OnCountdownDone += () =>
        {
            timerOut = true;
            InitializeBossRoom();
            Debug.Log("Timer has run out!");
        };

        if (currentRoom != null || previousRoom != null)
        {
            Destroy(currentRoom);
            Destroy(previousRoom);
        }

        if (startingRoom != null)
        {
            currentRoom = Instantiate(startingRoom, new Vector3(0,0,0), Quaternion.identity);
        }
        else
        {
            Debug.LogError("A Starting Room has not been assigned. Please assign a starting room before playing. The game will now pick randomly from the list.");
            currentRoom = Instantiate(roomPrefabList[Random.Range(0, roomPrefabList.Count)], new Vector3(0,0,0), Quaternion.identity);
        }

        currentRoom.roomTrigger.gameObject.SetActive(false);
        //currentRoom.LightFader.SetFade(true);

        SpawnNewRoom(roomPrefabList[Random.Range(0, roomPrefabList.Count)]);
    }

    private void FixedUpdate()
    {
        if (GameManager.instance.AIManager.enemyList.Count == 0) // Doors Open
        {
            callSwitch = false;
            
            //currentRoom.RoomCheck(true);
            //previousRoom.RoomCheck(true);

            foreach(DoorOpener doorOpener in FindObjectsOfType<DoorOpener>())
            {
                doorOpener.Open = true;
            }

        }
        else if (GameManager.instance.AIManager.enemyList.Count > 0) // Doors Closed
        {
            callSwitch = true;

            foreach (DoorOpener doorOpener in FindObjectsOfType<DoorOpener>())
            {
                doorOpener.Open = false;
            }

            //currentRoom.RoomCheck(false);
            //previousRoom.RoomCheck(false);
        }
    }

    private void InitializeBossRoom()
    {
        currentRoom.DespawnRoom();
        currentRoom = previousRoom;
        currentRoom.RoomCheck(false);
        
        SpawnNewRoom(bossRoomList[Random.Range(0, bossRoomList.Count)]);
        currentRoom.spawnBossRoom();
    }

    private void SpawnNewRoom(OldRoom prefabRoom)
    {
        Vector3 currentDoorOut = currentRoom.doors[1].transform.position;

        var newRoom = Instantiate(prefabRoom, currentDoorOut, Quaternion.identity);
        
        Vector3 newDoorIn = newRoom.doors[0].transform.position;
        
        Vector3 delta = currentDoorOut - newDoorIn;
        newRoom.transform.position += delta;
        
        newRoom.doors[0].gameObject.SetActive(false);
        
        previousRoom = currentRoom;
        currentRoom = newRoom;

        currentRoom.GetIndex = 1;
        previousRoom.GetIndex = 2;
        
        currentRoom.Initialize();
    }

    IEnumerator RoomEnterCoroutine()
    {
        yield return StartCoroutine(PowerUpManager.instance.ChoosePowerup());
        InitializeRoom();
    }

    public void CurrentRoomEntered()
    {
        StartCoroutine(RoomEnterCoroutine());
    }

    private void  InitializeRoom()
    {
        currentRoom.EnemyDataSend(timerOut);
        //currentRoom.LightFader.SetFade(true);
        amtOfRooms++;
        displayRoomInfo.Display(amtOfRooms, currentRoom.roomDifficulty);

        previousRoom.DespawnRoom();
        SpawnNewRoom(roomPrefabList[Random.Range(0, roomPrefabList.Count)]);

        previousRoom.doors[0].gameObject.SetActive(true);
    }

    public int GetAmmountOfRooms()
    {
        return amtOfRooms;
    }
}
