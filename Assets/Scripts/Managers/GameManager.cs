using System;
using System.Collections;
using System.Collections.Generic;
using Clonk;
using Clonk.CharacterInput;
using Unity.Mathematics;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public CameraManager CameraManager;
    public CharacterSpawner Spawner;

    private CharacterHandler PlayerCharacterHandler;
    public  CharacterScriptableObject playerCharacterSO;

    public PowerUpManager PowerUpManager;
    public AIManager AIManager;
    public RoomManager RoomManager;
    public EffectManager EffectManager;
    public PlayerHealthbar PlayerHealthbar;
    public ScoreManager ScoreManager;
    public GameplayUIManager GameplayUIManager;

    public Transform JoystickParent;

    public GameObject options;
    
    public bool isGamePaused {get; private set;}

    private float smoothPauseTime = 3f;
    private IEnumerator smoothPause;

    private void Awake()
    {

        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }

        CameraManager = GetComponent<CameraManager>();
        PowerUpManager = GetComponent<PowerUpManager>();
        AIManager = GetComponentInChildren<AIManager>();
        RoomManager = GetComponent<RoomManager>();
        EffectManager = GetComponent<EffectManager>();
        ScoreManager = GetComponent<ScoreManager>();
        options = GameObject.Find("Options");

        smoothPause = SmoothTimeScaleTo(0f);
    }

    private void Start()
    {
        //if the option game object is not there then it playerCharacterSO will be set to the keyboard controls
        //this is here to make it so when you are editing in the gameplay scene there will always be keyboard controls incase the input is gamepad
        if (options != null) 
        {
            playerCharacterSO = options.GetComponent<Options>().playerCharacterSO;
        }
        PlayerCharacterHandler = Spawner.Spawn(playerCharacterSO, Vector3.zero, Quaternion.identity, 1);
        CameraManager.Target = PlayerCharacterHandler.Character.transform;
        PowerUpManager.player = PlayerCharacterHandler;
        Debug.Log(playerCharacterSO.CharacterPrefab.HealthCurrent);
        AudioManager.Instance.Play(AudioManager.Sounds.Music_Clonk);

        Character playerCharacter = PlayerCharacterHandler.Character;

        PlayerHealthbar.UpdateHealthbarInstant(playerCharacter.HealthCurrent, playerCharacter.HealthMax);
        PlayerCharacterHandler.Character.OnHealthUpdated += PlayerHealthbar.UpdateHealthbar;
        PlayerCharacterHandler.Character.OnTookDamage += (damage) =>
        {
            ScoreManager.ResetComboAndMultiplier();
        };
        if (PlayerCharacterHandler.Input is TouchInput touchinput)
        {
            touchinput.CreateVisualJoysticks(JoystickParent);
        }

    }

    public void PauseGame()
    {
        isGamePaused = true;
        Time.timeScale = 0;
    }

    public void PauseGameSmooth()
    {
        StopCoroutine(smoothPause);
        smoothPause = SmoothTimeScaleTo(0f);
        StartCoroutine(smoothPause);
    }

    public void UnPauseGame()
    {
        isGamePaused = false;
        Time.timeScale = 1;
    }

    public void UnPauseGameSmooth()
    {
        StopCoroutine(smoothPause);
        smoothPause = SmoothTimeScaleTo(1f);
        StartCoroutine(smoothPause);
    }

    float currentvel;

    private IEnumerator SmoothTimeScaleTo(float target)
    {

        if (target == 1f) isGamePaused = false;

        if (Time.timeScale == 0f) Time.timeScale = .001f;

        float startingTimeScale = Time.timeScale;
        for (float t = 0f; Mathf.Abs(Time.timeScale - target) > .001f;)
        {
            //Debug.Log(Time.deltaTime);

            Time.timeScale = Mathf.SmoothStep(startingTimeScale, target, t);

            t += smoothPauseTime * (Time.deltaTime / (Time.timeScale * 2f));

            yield return null;
        }

        Time.timeScale = target;

        if(target == 0f) isGamePaused = true;
    }

    public CharacterHandler GetPlayer()
    {
        return PlayerCharacterHandler;
    }
}
