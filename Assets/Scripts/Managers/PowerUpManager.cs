using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Clonk;
using Clonk.Stats;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using Clonk.EditorExtensions;


#if UNITY_EDITOR

[CustomEditor(typeof(PowerUpManager))] 
public class PowerupManagerEditor: Editor{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Populate List"))
        {
            FindAllPowerUps();
        }
    }

    private void FindAllPowerUps()
    {

        PowerUpManager manager = (PowerUpManager) target;
        
        var guids = AssetDatabase.FindAssets("t:PowerUpData");
        var powerUps = new PowerUpData[guids.Length];

        for(int i = 0; i < guids.Length; i++)
        {
            var path = AssetDatabase.GUIDToAssetPath(guids[i]);
            powerUps[i] = AssetDatabase.LoadAssetAtPath<PowerUpData>(path);
        }
        manager.setPowerups(powerUps);
    }
}
#endif
public class PowerUpManager : MonoBehaviour
{
    public GameObject selecting;
    
    public static PowerUpManager instance;
    [SerializeField] private GameObject PowerUpPanelPrefab;
    [SerializeField] private RectTransform PowerUpPanelParent;

    public CharacterHandler player;
    
    [SerializeField] public PowerUpData[] powerups;
    public PowerUpPack powerUpPack;
    private List<PowerUpData> bossPowerUps = new List<PowerUpData>();

    private List<PowerUpData> enemyPowerUps = new List<PowerUpData>();

    private bool IsChoosing = false;

    public Action<List<PowerUpData>> onPowerupChosen;

    private void Awake()
    {
        
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
            //EventSystem.current.SetSelectedGameObject(PowerUpPanelPrefab);
        }
    }

    public void setPowerups(PowerUpData[] p)
    {
        powerups = p;
    }

    public static PowerUpData GetRandomPowerUp()
    {
        return instance.powerups[Random.Range(0, instance.powerups.Length)];
    }
    public static List<PowerUpData> GetBossPowerUps()
    {
        return instance.bossPowerUps;
    }

    public static List<PowerUpData> GetEnemyPowerUps()
    {
        return instance.enemyPowerUps;
    }

    public static void AddBossPowerUpsToEnemyPowerUps()
    {
        instance.enemyPowerUps.Clear();

        instance.enemyPowerUps.AddRange(instance.bossPowerUps);
    }

    float smoothVel;

    public IEnumerator ChoosePowerup(Character character = null,int panels = 2)
    {
        IsChoosing = true;
        SpawnNewPowerups(character,panels);

        while(IsChoosing)
        {

            yield return null;
            //Debug.Log("AAAAA");
        }
    }



    private void SpawnNewPowerups(Character character = null,int panels = 2)
    {
        if (character == null)
            character = player.Character;
        if(selecting != null)
        {
            EventSystem.current.SetSelectedGameObject(selecting);
        }
        GameManager.instance.PauseGameSmooth();
        List<PowerUpData> powerUpList= powerups.ToList();
        List<PowerUpPanel> powerUpPanels = new List<PowerUpPanel>();

        List<PowerUpData> pickedPowerUps = new List<PowerUpData>();

        for (int i = 0; i <= panels - 1; i++)
        {
            PowerUpData itemData = powerUpPack.GetRandomPowerUp();

            if(pickedPowerUps.Contains(itemData))
            {
                
                for (int loops = 0; pickedPowerUps.Contains(itemData) && loops < 100; itemData = powerUpPack.GetRandomPowerUp())
                {

                    loops++;
                    Debug.Log("loops: " + loops);
                }
                
            }

            pickedPowerUps.Add(itemData);
            PowerUpPanel powerUpPanel = Instantiate(PowerUpPanelPrefab, PowerUpPanelParent).GetComponent<PowerUpPanel>();

            powerUpPanel.Initialize(powerUpList[itemData.id],
                (p) =>
                {
                    //character.AddUpgrade(p);
                    //character.AddHealing(p);
                    character.AddUpgrade(p);
                    if (itemData.typeID == 1)
                    {
                        character.AddHealing(p);
                    }
                    /*else
                    {
                        character.AddUpgrade(p);
                    }*/
                    
                    foreach (Transform child in PowerUpPanelParent)
                    {
                        var childP = child.GetComponent<PowerUpPanel>().powerUpData;
                        var childH = child.GetComponent<PowerUpPanel>().powerUpData;
                        //boss weapon is childP
                        if (childP != p)
                        {
                            bossPowerUps.Add(childP);
                        }


                        //Debug.Log("childP: " + childP + "childH: " + childH);

                        /*Debug.Log("childP: " + childP);
                        Debug.Log("childH: " + childH);*/
                        Destroy(child.gameObject);
                    }
                    GameManager.instance.UnPauseGameSmooth();
                    IsChoosing = false;
                    /*if (itemData.typeID == 1)
                    {
                        character.AddHealing(p);
                    }*/
                    /*if (itemData.typeID == 0)
                    {
                        onPowerupChosen?.Invoke(character.GetUpgrades());
                    }*/
                    onPowerupChosen?.Invoke(character.GetUpgrades());
                    //onPowerupChosen?.Invoke(character.AddHealing(2));

                }
            );
            powerUpPanels.Add(powerUpPanel);
            //powerUpList.RemoveAt(listIndex);
            

        }

        
        
    }




}
