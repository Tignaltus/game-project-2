using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AudioManager : MonoBehaviour
{
    public enum Sounds
    {
        noSound = 0,
        Enemy_Death_Explosion = 1,
        Turret_Gun_Shoot = 2,
        Player_Gun_Shoot = 3,
        PlayerSteps = 4,
        Bullet_Hit = 5,
        Boombot_Explode = 6,
        Grinder = 7,
        Rev_Engine = 8,
        Alarmed_BoomBot = 20,
        Alarmed_Juggernaut = 21,
        Impact_Juggernaut = 30,
        Music_Clonk = 100,
        Music_Bogus = 101,
        Music_TorN = 102
    }

    public static AudioManager Instance;

    private Dictionary<Sounds, float> soundTimerDictionary;

    public Sound[] sounds;

    public float pitchMultiplier = 1f;
    public float volumeMultiplier = 1f;


    void Awake()
    {
        if (Instance != null)
        {
            Debug.Log("trying to create new instance of singleton");
            Destroy(gameObject);
            return;
        }

        Instance = this;

        soundTimerDictionary = new Dictionary<Sounds, float>
        {
            [Sounds.PlayerSteps] = 0
        };


        foreach (Sound s in sounds)
        {
            for (int i = 0; i < s.amountOfSources; i++)
            {
                var source = gameObject.AddComponent<AudioSource>();
                source.clip = s.getRandomClip();
                source.volume = s.volume;
                source.pitch = s.pitch;
                source.loop = s.loop;
                s.sources.Add(source);
            }

        }
        DontDestroyOnLoad(this);
    }



    public void UpdateGlobalPitch(float newMultiplier)
    {
        pitchMultiplier = Mathf.Max(.4f, newMultiplier);


        foreach (Sound sound in sounds)
        {
            if (sound.source != null && !sound.unscaled)
            {
                sound.source.pitch = sound.pitch * pitchMultiplier;
            }

        }
    }

    private void Update()
    {
        UpdateGlobalVolume();
    }

    public void UpdateGlobalVolume()
    {
        foreach (Sound sound in sounds)
        {
            if (sound.source != null)
            {
                sound.source.volume = sound.volume * volumeMultiplier;
            }

        }
    }

    private bool CanPlaySound(Sounds sound)
    {
        switch (sound)
        {
            default:
                return true;
            case Sounds.PlayerSteps:
                if (soundTimerDictionary.ContainsKey(sound))
                {
                    float lastTimePlayed = soundTimerDictionary[sound];
                    float playerStepsTimerMax = .5f;
                    if (lastTimePlayed + playerStepsTimerMax < Time.time)
                    {
                        soundTimerDictionary[sound] = Time.time;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
        }
    }



    public void StopAllSounds()
    {

        foreach (Sound sound in sounds)
        {
            if (sound.source != null)
            {
                sound.source.Stop();
            }

        }
    }

    public void Stop(Sounds name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s?.source.Stop();
    }

    public void Play(Sounds name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s == null) { return; }

        s.source.clip = s.getRandomClip();
        s.source.volume = s.volume;
        s.source.pitch = s.pitch + Random.Range(0f,s.randomizePitch);
        s.source.loop = s.loop;
        s.source.Play();
    }

    public void Play(Sounds sound, float delay)
    {
        StartCoroutine(playSoundDelayedCoroutine(delay, sound));
    }

    IEnumerator playSoundDelayedCoroutine(float delay, Sounds sound)
    {
        yield return new WaitForSecondsRealtime(delay);
        Play(sound);
    }


}
