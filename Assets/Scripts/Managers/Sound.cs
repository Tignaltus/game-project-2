using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

[Serializable]
public class Sound
{
    [SerializeField] private string _name;
    public AudioManager.Sounds name;
    public List<AudioClip> clips;
    [Range(0f,1f)]
    public float volume;
    [Range(0.1f,3f)]
    public float pitch;
    [Range(0f,1f)]
    public float randomizePitch = 0f;

    public int amountOfSources = 1;
    public bool unscaled;
    public bool loop;

    [HideInInspector]
    public List<AudioSource> sources;

    private int sourceIndex = 0;
    public AudioSource source {
        get
        {
            var _source = sources[sourceIndex];
            sourceIndex++;
            if (sourceIndex > sources.Count - 1)
            {
                sourceIndex = 0;
            }
            return _source;
        }
    }

    public AudioClip getRandomClip()
    {
        var random = new Random();
        int index = random.Next(clips.Count);
        return clips[index];
    }
    

}