using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using Math = UnityEngine.ProBuilder.Math;

public class ScoreManager : MonoBehaviour
{
    public ScoreDisplay scoreDisplay;
    
    private int currentScore;
    private int multiplier;
    private int combo;
    public int MaxCombo { get; private set;}
    private float multiplierTimer;
    private int multiplierKillsUntilNext = 1;
    public float multiplierCountDownTime = 5f;

    private bool comboing = false;

    public Action onResetCombo;
    public Action<int,int,int,float> onAddScore;
    
    
    private void Start()
    {
    }

    private void Update()
    {
        if (comboing) 
        {
            
            if (GameManager.instance.isGamePaused)
            {
                multiplierTimer += Time.deltaTime;
            }
            if (Time.time > multiplierTimer)
            {
                ResetComboAndMultiplier();
            }
        }
    }


    public void ResetScore()
    {
        currentScore = 0;
        ResetComboAndMultiplier();
    }

    public void ResetComboAndMultiplier()
    {
        combo = 0;
        multiplier = 1;
        multiplierKillsUntilNext = 1;
        comboing = false;
        onResetCombo?.Invoke();
    }


    public void AddScore(int scoreAmmount)
    {
        comboing = true;
        combo++;
        multiplierKillsUntilNext--;
        multiplierTimer = Time.time + multiplierCountDownTime;
        if (multiplierKillsUntilNext <= 0)
        {
            multiplier++;
            multiplierKillsUntilNext = multiplier;
        }
        currentScore += scoreAmmount * multiplier;
        MaxCombo = Mathf.Max(MaxCombo, combo); 
        onAddScore?.Invoke(currentScore, multiplier, combo, multiplierCountDownTime);
    }
    
    public int GetCurrentScore()
    {
        return currentScore;
    }
}
