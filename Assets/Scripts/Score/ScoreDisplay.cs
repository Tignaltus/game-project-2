using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour
{
    private ScoreManager scoreManager;
    
    public TMP_Text displayScore;
    public TMP_Text multiplierText;
    public TMP_Text comboValueText;
    
    public Image multiplierCircle;
    public GameObject multiplierCircleandComboContainer;


    private float timeUntilMultiplierIsLost = 1;
    private float multiplierTimeStart = 0;

    private bool showComboAndMultiplier = false;

    public float fillAmount = 0;

    private void Start()
    {
        scoreManager = GameManager.instance.ScoreManager;
        scoreManager.onAddScore += UpdateScoreComboAndMultiplier;
        scoreManager.onResetCombo += () =>
        {
            showComboAndMultiplier = false;
            multiplierCircleandComboContainer.gameObject.SetActive(false);
        };
        showComboAndMultiplier = false;
        multiplierCircleandComboContainer.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (showComboAndMultiplier)
        {
            if (Time.time - multiplierTimeStart < timeUntilMultiplierIsLost)
            {
                if (GameManager.instance.isGamePaused)
                {
                    timeUntilMultiplierIsLost += Time.deltaTime;
                }

                fillAmount = 1 - ((Time.time - multiplierTimeStart) / timeUntilMultiplierIsLost);
                multiplierCircle.fillAmount = fillAmount;
            }
            else
            {
                showComboAndMultiplier = false;
                multiplierCircleandComboContainer.gameObject.SetActive(false);
            }
            
            comboValueText.transform.localScale = Vector3.Lerp(comboValueText.transform.localScale,Vector3.one,.1f);
        }
    }


    public void UpdateScoreComboAndMultiplier(int currentScore,int multiplier,int combo,float timeUntilMultiplierIsLost)
    {
        displayScore.text = "Score:" + currentScore.ToString("D10");
        multiplierCircleandComboContainer.gameObject.SetActive(true);
        comboValueText.text = combo.ToString();
        comboValueText.transform.localScale = Vector3.one*1.3f;
        multiplierText.text = multiplier.ToString();
        multiplierTimeStart = Time.time;
        this.timeUntilMultiplierIsLost = timeUntilMultiplierIsLost;
        showComboAndMultiplier = true;


    }



}
