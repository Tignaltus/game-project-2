using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;



public class CharacterSpawner : MonoBehaviour
{
    public GameObject CharacterHandlerPrefab;

    public  CharacterHandler Spawn(CharacterScriptableObject characterSO, Vector3 position, Quaternion rotation, int team)
    {
        CharacterHandler cHandler = Instantiate(CharacterHandlerPrefab,position,rotation).GetComponent<CharacterHandler>();
        //CharacterHandler cHandler = ObjectPooler.SharedInstance.GetPooledObject(CharacterHandlerPrefab, position, rotation).GetComponent<CharacterHandler>();
        cHandler.Initialize(characterSO);
        cHandler.Character.SetTeam(team);

        //Debug.Log(cHandler.Character);
        
        return cHandler;
    }
    
}
