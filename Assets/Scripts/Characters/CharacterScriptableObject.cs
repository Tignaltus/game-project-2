using System.Collections;
using System.Collections.Generic;
using Clonk;
using Clonk.CharacterInput;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Character")]
public class CharacterScriptableObject : ScriptableObject
{
    public Character CharacterPrefab;
    public PublisherCharacterInput InputPrefab;
    public bool isBoss;
}