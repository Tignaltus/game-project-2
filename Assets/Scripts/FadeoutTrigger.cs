using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class FadeoutTrigger : MonoBehaviour
{
    private BoxCollider trigger;
    private FadeoutController fadeoutController;

    private void Start()
    {
        trigger = GetComponent<BoxCollider>();
        trigger.isTrigger = true;

        fadeoutController = transform.parent.GetComponentInChildren<FadeoutController>();
    }

    public void SetupTrigger(Transform parent, Vector3 center, Vector3 size)
    {
        if (trigger == null) trigger = GetComponent<BoxCollider>();



        transform.parent = parent;
        trigger.center = center;
        trigger.size = size;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (fadeoutController != null)
        {
            Entity enteringEntity = other.gameObject.GetComponent<Entity>();

            if (enteringEntity != null && enteringEntity.Team == 1)
            {
                fadeoutController.FadeIn();
            }
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (fadeoutController != null)
        {
            Entity enteringEntity = other.gameObject.GetComponent<Entity>();

            if (enteringEntity != null && enteringEntity.Team == 1)
            {

                fadeoutController.FadeOut();
            }
        }
    }
}
