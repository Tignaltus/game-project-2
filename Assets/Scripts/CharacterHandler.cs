using System;
using Clonk.CharacterInput;
using UnityEngine;

namespace Clonk
{
    public class CharacterHandler : MonoBehaviour
    {

        public string characterName = "Player";
        public CharacterScriptableObject myCharacterSO;
        private Character character;
        private PublisherCharacterInput input;
        public Character Character => character;
        public PublisherCharacterInput Input => input;

        public void Initialize(CharacterScriptableObject characterSO)
        {
            myCharacterSO = characterSO;
            characterName = myCharacterSO.name;
            character = Instantiate(myCharacterSO.CharacterPrefab, transform);
            input = Instantiate(myCharacterSO.InputPrefab, character.transform);
            input.Initialize(character);
            character.Subscribe(input);
            character.OnDeath += () => {Destroy(gameObject);};
            //character.OnDeath += () => { ObjectPooler.SharedInstance.Deactivate(gameObject); };
        }

        private void Awake()
        {
            if (myCharacterSO != null)
            {
                Initialize(myCharacterSO);
            }
        }

        private void OnEnable()
        {
            if (character == null) return;
            character.Subscribe(input);
        }

        private void OnDisable()
        {
            if (character == null) return;
            character.Unsubscribe(input);
        }
    }
}