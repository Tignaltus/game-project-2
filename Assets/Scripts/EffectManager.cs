using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EffectType
{
    Explosion = 0,
    HitEffectPlayer = 10,
    HitEffectEnemy = 11,
    HitEffectDonut = 12,
    ShootEffectPlayer = 20,
    ShootEffectEnemy = 21,
    GrindEffect = 30,
    AlertEffect = 40,
    StunEffect = 50,
    BulletExplosion = 60
}

public class EffectManager : MonoBehaviour
{
    [SerializeField]
    private Effect[] Effects;


    public void CreateEffect(EffectType type, Vector3 position, Quaternion rotation, Vector3 scale, Transform parent = null)
    {
        var effect = Array.Find(Effects, e => e.Type == type);
        if (effect == null)
        {
            Debug.LogWarning($"No Effect with the type {type.ToString()} could be found");
            return;
        }

        //var effectInstance = Instantiate(effect.Prefab, position, rotation,parent);
        var effectInstance = ObjectPooler.SharedInstance.GetPooledObject(effect.Prefab, position, rotation);
        effectInstance.transform.localScale = scale;

        //Destroy(effectInstance, effect.Duration);

        ObjectPooler.SharedInstance.Deactivate(effectInstance,effect.Duration);

    }
    

    [Serializable]
    public class Effect
    {
        [SerializeField]private string _name;
        public EffectType Type;
        public GameObject Prefab;
        public float Duration = 1f;

    }
}
