using System.Collections;
using System.Collections.Generic;
using Clonk.CharacterInput;
using UnityEngine;

public class Options : MonoBehaviour
{
    public enum controlType
    {
        keyboard = 0,
        gamepad = 1,
        mobile = 2
    }
    [HideInInspector]
    public PublisherCharacterInput playerControls;

    public PublisherCharacterInput keyboardControls;
    public PublisherCharacterInput gamepadControls;
    public PublisherCharacterInput mobileControls;

    public CharacterScriptableObject playerCharacterSO;

    public float rotationSpeed = 70;
    public static Options instance;

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(this);
        }
        instance = this;
    }
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        rotationSpeed = playerCharacterSO.CharacterPrefab.rotationSpeed;
        if (playerCharacterSO.InputPrefab == null)
        {
            playerCharacterSO.InputPrefab = keyboardControls;
        }
    }

    public void UpdateInput()
    {
        if(playerControls == null)
        {
            playerControls = keyboardControls;
        }
        playerCharacterSO.InputPrefab = playerControls;
    }

    public void SwitchInput(controlType type)
    {
        switch (type)
        {
            case controlType.keyboard:
                playerControls = keyboardControls;
                playerCharacterSO.CharacterPrefab.rotationSpeed = 70;
                break;
            case controlType.gamepad:
                playerControls = gamepadControls;
                playerCharacterSO.CharacterPrefab.rotationSpeed = 7;
                break;
            case controlType.mobile:
                playerControls = mobileControls;
                playerCharacterSO.CharacterPrefab.rotationSpeed = 12;
                break;
        }
        UpdateInput();
    }

}
