using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace Clonk.LevelGenerator
{
    public class LevelGenerator : MonoBehaviour
    {
        public static LevelGenerator instance;

        public bool debugMode;

        public LevelPack levelPack;

        public int levelSize;

        public List<Room> rooms;

        public bool generateOnAwake;


        private readonly List<DoorConnection> doorConnections = new List<DoorConnection>();

        private void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
            }
            else
            {
                instance = this;
            }


        }

        private void Start()
        {
            if (generateOnAwake) GenerateLevel();
        }

        public void ClearLevel()
        {
            rooms.Clear();

            doorConnections.Clear();

            List<Transform> tempList = transform.Cast<Transform>().ToList();
            foreach (Transform child in tempList)
            {
                DestroyImmediate(child.gameObject);
            }
        }

        public List<Doorhole> GetAllDoorholes()
        {
            List<Doorhole> doorholes = new List<Doorhole>();

            foreach (Room room in rooms) doorholes.AddRange(room.doors);

            return doorholes;
        }

        public List<Doorhole> GetAllUnconnectedDoorholes()
        {
            List<Doorhole> doorholes = new List<Doorhole>();

            foreach (Room room in rooms) doorholes.AddRange(room.unconnectedDoors);

            return doorholes;
        }
        public void GenerateLevel()
        {

            StartCoroutine(LevelGeneration());
        }


        private IEnumerator LevelGeneration()
        {

            LevelGenerationFirstPass();

            if (debugMode) yield return new WaitForFixedUpdate();

            LevelGenerationSecondPass();

            LevelGenerationThirdPass();

        }


        private void LevelGenerationFirstPass()
        {
            ClearLevel();

            PlaceSpawnPoint();

            int fails = 0;

            for (int i = 0; i < levelSize; i++)
            {

                Room newRoom = InstantiateRoom(levelPack.GetRandomRoom());

                DoorConnection doorCon = FindSuitableDoor(newRoom);

                if (doorCon != null)
                {


                    doorCon.Bind();

                    doorConnections.Add(doorCon);

                    Vector3 roomOffset = doorCon.doorA.GetDoorPos();

                    newRoom.transform.position = doorCon.doorB.GetDoorPos() - roomOffset;

                    rooms.Add(newRoom);


                    doorCon.doorA.parentRoom.Init();
                    doorCon.doorB.parentRoom.Init();

                }
                else
                {
                    DestroyImmediate(newRoom.gameObject);

                    fails++;
                    i--;

                    if (fails > levelSize)
                    {
                        i++;
                        fails = 0;
                    }

                }
            }

        }




        private void LevelGenerationSecondPass()
        {
            foreach (SpecialRoomStat specialRoomStat in levelPack.GetSpecialRooms())
            {

                Room newRoom = InstantiateRoom(specialRoomStat.room);

                DoorConnection doorCon = FindSuitableDoor(newRoom);

                if (doorCon != null)
                {

                    doorCon.Bind();

                    doorConnections.Add(doorCon);

                    Vector3 roomOffset = doorCon.doorA.GetDoorPos();

                    newRoom.transform.position = doorCon.doorB.GetDoorPos() - roomOffset;

                    rooms.Add(newRoom);

                    if (specialRoomStat.specialDoorPrefab != null)
                    {
                        doorCon.doorGameObject = Instantiate(specialRoomStat.specialDoorPrefab, doorCon.position, Quaternion.LookRotation(doorCon.doorA.direction), transform);
                    }


                    doorCon.doorA.parentRoom.Init();
                    doorCon.doorB.parentRoom.Init();

                }
            }


        }

        public void LevelGenerationThirdPass()
        {
            List<Doorhole> unconnectedDoorholes = GetAllUnconnectedDoorholes();

            foreach (Doorhole doorA in unconnectedDoorholes)
            {
                Room parentRoom = doorA.parentRoom;

                if (!doorA.parentRoom.HasReachedDoorLimit())
                {

                    foreach (Doorhole doorB in unconnectedDoorholes)
                    {

                            if (doorA != doorB)
                            {

                                if (Vector3.Distance(doorA.GetDoorPos(), doorB.GetDoorPos()) < .5f)
                                {
                                    DoorConnection doorConnection = new DoorConnection(doorA, doorB);

                                    doorConnection.Bind();

                                    doorConnections.Add(doorConnection);
                                }
                            }

                    }

                }
            }

            //TrimLooseEnds();
            for (int i = 0; i < levelSize; i++)
            {
                TrimQuotaEnds();
            }


            foreach (DoorConnection dc in doorConnections)
            {
                if (dc.doorGameObject == null)
                {
                    dc.doorGameObject = Instantiate(levelPack.doorPrefab, dc.position, Quaternion.LookRotation(dc.doorA.direction), transform);

                }
                if (!dc.doorA.parentRoom.doorConnections.Contains(dc)) dc.doorA.parentRoom.doorConnections.Add(dc);
                if (!dc.doorB.parentRoom.doorConnections.Contains(dc)) dc.doorB.parentRoom.doorConnections.Add(dc);

                dc.doorA.inDoorHole = dc.doorGameObject;
                dc.doorB.inDoorHole = dc.doorGameObject;
            }

            foreach (Doorhole door in GetAllUnconnectedDoorholes())
            {
                door.inDoorHole = Instantiate(levelPack.doorHoleFiller, door.GetDoorPos(), Quaternion.LookRotation(door.direction, Vector3.up), transform);
            }

            //UIMinimap.mapRooms.Clear();

            foreach (Room room in rooms)
            {

                GameManager.instance.GameplayUIManager.minimap.AddMapRoom(room.localBounds.center.FlatVector3ToVector2(), room.localBounds.size.FlatVector3ToVector2());

            }
        }


        public void TrimLooseEnds()
        {
            List<Room> roomsToRemove = new List<Room>();

            foreach (Room room in rooms)
            {
                if (room.doors.Count <= 2 && room.unconnectedDoors.Count >= 1)
                {
                    roomsToRemove.Add(room);

                }
            }

            foreach (Room room in roomsToRemove)
            {
                rooms.Remove(room);
                DestroyImmediate(room.gameObject);
            }

            rooms = GetComponentsInChildren<Room>().ToList();
        }


        public void TrimQuotaEnds()
        {
            foreach (Room room in rooms)
            {
                if (room.removeIfUnderDoorQuota && room.connectedDoors.Count < room.doorQuota)
                {
                    foreach (Doorhole door in room.connectedDoors)
                    {
                        door.doorConnection.Unbind();

                        doorConnections.Remove(door.doorConnection);
                    }

                    room.Init();

                    DestroyImmediate(room.gameObject);
                }
            }

            rooms = GetComponentsInChildren<Room>().ToList();
        }

        private void PlaceSpawnPoint()
        {
            Room spawnRoom = InstantiateRoom(levelPack.spawnRoom);

            spawnRoom.transform.position = Vector3.zero;

            rooms.Add(spawnRoom);
        }


        private DoorConnection FindSuitableDoor(Room roomToMatch)
        {
            List<DoorConnection> potentialConnections = new List<DoorConnection>();

            foreach (Doorhole doorToMatch in roomToMatch.doors)
            {


                Doorhole.DoorWall targetDoorWall = doorToMatch.GetCompatibleDoorWall();

                foreach (Doorhole door in GetAllUnconnectedDoorholes())
                {




                    if (door.doorWall == targetDoorWall)
                    {
                        bool doesNotIntersect = true;

                        Bounds testBounds = new Bounds(door.GetDoorPos() - doorToMatch.GetDoorPos(), roomToMatch.localBounds.size * .99f);

                        for (int i = 0; i < rooms.Count; i++)
                        {
                            Room exRoom = rooms[i];

                            if (testBounds.Intersects(exRoom.localBounds))
                            {
                                //Debug.Log("intersects");
                                doesNotIntersect = false;

                                break;
                            }
                        }

                        if (doesNotIntersect) potentialConnections.Add(new DoorConnection(doorToMatch, door));

                    }


                }
            }

            return potentialConnections.Count != 0 ? ListExtension.GetRandomItem(potentialConnections) : null;

        }


        private Room InstantiateRoom(Room room)
        {
            Room roomInstance = Instantiate(room, transform);

            roomInstance.Init();


            return roomInstance;
        }




    }


    public class DoorConnection
    {
        public DoorConnection(Doorhole a, Doorhole b)
        {
            doorA = a;
            doorB = b;
        }

        public Doorhole doorA;
        public Doorhole doorB;

        public GameObject doorGameObject;


        public Vector3 position => doorA.GetDoorPos();

        public void Bind()
        {
            doorA.doorConnection = this;
            doorB.doorConnection = this;


            doorA.parentRoom.Init();
            doorB.parentRoom.Init();

        }

        public void Bind(GameObject doorGameObject)
        {
            doorA.doorConnection = this;
            doorB.doorConnection = this;

            doorA.parentRoom.Init();
            doorB.parentRoom.Init();

            this.doorGameObject = doorGameObject;

            this.doorGameObject.transform.position = position;
        }

        public void Unbind()
        {
            //doorA.doorConnection = null;
            //doorB.doorConnection = null;

            //GameObject.Destroy(doorGameObject);

            //doorB.parentRoom.Init();
        }
    }

}
