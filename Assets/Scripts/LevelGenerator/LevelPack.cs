using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Clonk.LevelGenerator
{

    [System.Serializable]
    public struct RoomStat
    {
        public Room room;
        [Range(0, 100)] public int rarity;

    }

    [System.Serializable]
    public struct SpecialRoomStat
    {
        public Room room;
        public GameObject specialDoorPrefab;

        public int minPerLevel;
        public int maxPerLevel;
    }

    [CreateAssetMenu(menuName = "LevelGeneration/LevelPack")]
    public class LevelPack : ScriptableObject
    {

        public Room spawnRoom;



        public List<RoomStat> standardRooms;
        
        public List<SpecialRoomStat> specialRooms;

        public GameObject doorPrefab;
        public GameObject doorHoleFiller;

        public Room GetRandomRoom()
        {
            List<RoomStat> weightedRoomList = new List<RoomStat>();

            foreach (RoomStat roomStat in standardRooms)
            {
                for (int i = 0; i < roomStat.rarity; i++) weightedRoomList.Add(roomStat);
            }

            return weightedRoomList.GetRandomItem().room;
        }

        public List<SpecialRoomStat> GetSpecialRooms()
        {
            List<SpecialRoomStat> specialRoomPrefabs = new List<SpecialRoomStat>();

            foreach(SpecialRoomStat specialRoomStat in specialRooms)
            {
                int amount = Random.Range(specialRoomStat.minPerLevel, specialRoomStat.maxPerLevel);

                for (int i = 0; i < amount; i++) specialRoomPrefabs.Add(specialRoomStat);


            }

            return specialRoomPrefabs;
        }
    }

}