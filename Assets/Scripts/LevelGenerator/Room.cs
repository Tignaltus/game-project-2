using System.Collections.Generic;
using UnityEngine;


namespace Clonk.LevelGenerator
{

    public class Room : MonoBehaviour
    {

        public Bounds bounds;

        public bool generateTrigger = true;

        public bool removeIfUnderDoorQuota;
        public int doorQuota;


        public bool limitMaxDoors;
        public int maxDoors;

        public bool HasReachedDoorLimit() => (!limitMaxDoors || doorConnections.Count < maxDoors);


        public Vector3 localBoundsPos => transform.TransformPoint(bounds.center);


        public Bounds localBounds => new Bounds(transform.TransformPoint(bounds.center), bounds.size);


        public List<Doorhole> doors = new List<Doorhole>();

        public List<Doorhole> unconnectedDoors = new List<Doorhole>();

        public List<Doorhole> connectedDoors = new List<Doorhole>();

        public List<DoorConnection> doorConnections = new List<DoorConnection>();

        private void Awake()
        {

            FadeoutTrigger fadeoutTrigger = new GameObject("Fadeout Trigger").AddComponent<FadeoutTrigger>();

            fadeoutTrigger.SetupTrigger(transform, bounds.center, bounds.size);

            if (generateTrigger)
            {
                FirstTimeRoomTrigger firstTimeTrigger = gameObject.AddComponent<FirstTimeRoomTrigger>();

                firstTimeTrigger.SetupTrigger(bounds.center, bounds.size - new Vector3(3f, 0f, 3f));
            }
        }

        public void Init()
        {
            unconnectedDoors.Clear();
            connectedDoors.Clear();
            for (int i = 0; i < doors.Count; i++)
            {
                Doorhole door = doors[i];

                door.name = door.doorWall.ToString();

                door.parentRoom = this;
                door.doorIndex = i;

                if (door.doorConnection != null) connectedDoors.Add(door);
                else unconnectedDoors.Add(door);

            }

        }


        public bool HasDoorOnWall(Doorhole.DoorWall desiredWall)
        {
            bool result = false;

            for (int i = 0; i < doors.Count; i++)
            {
                Doorhole door = doors[i];

                if (door.doorWall == desiredWall)
                {
                    result = true;

                    break;
                }
            }

            return result;
        }

        

        public Doorhole GetRandomDoorOnWall(Doorhole.DoorWall wall)
        {
            List<Doorhole> matchingDoors = new List<Doorhole>();

            foreach (Doorhole d in doors)
            {
                if (d.doorWall == wall) matchingDoors.Add(d);
            }


            return ListExtension.GetRandomItem(matchingDoors);
        }


        private void OnDrawGizmos()
        {

            Gizmos.color = new Color(1f, 1f, 1f, .75f);

            Gizmos.DrawWireCube(localBounds.center, localBounds.size);

            Gizmos.color = new Color(1f, 0f, 1f, .25f);
            foreach (Doorhole d in doors)
            {
                Gizmos.DrawWireCube(d.GetDoorPos(), Vector3.one * 2f);
            }

            /*
            Gizmos.color = Color.red;
            foreach (Door d in unconnectedDoors)
            {
                Gizmos.DrawWireCube(d.GetDoorPos(), Vector3.one * 1f);
            }
            */

            Gizmos.color = Color.green;
            foreach (Doorhole d in connectedDoors)
            {
                Gizmos.DrawWireCube(d.GetDoorPos(), Vector3.one * 1f);
            }
        }
    }

}