using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Clonk.LevelGenerator
{

    [System.Serializable]
    public class Doorhole
    {
        public string name;
        public Room parentRoom;
        public int doorIndex;

        public GameObject inDoorHole;

        public DoorConnection doorConnection = null;

        public enum DoorWall
        {
            Front,
            Back,
            Left,
            Right
        }

        public DoorWall doorWall;

        public int shift;
        public int height;

        private Vector3[] doorDirections = new Vector3[] { Vector3.forward, Vector3.back, Vector3.left, Vector3.right };

        public Vector3 direction => doorDirections[(int)doorWall];

        public Vector3 GetDoorPos()
        {

            Vector3 extents = parentRoom.bounds.size * .5f;

            Vector3 dir = new Vector3();

            switch (doorWall)
            {
                case DoorWall.Front: dir = (Vector3.forward * extents.z) + (Vector3.right * shift); break;
                case DoorWall.Back: dir = (Vector3.back * extents.z) + (Vector3.right * shift); break;
                case DoorWall.Left: dir = (Vector3.left * extents.x) + (Vector3.forward * shift); break;
                case DoorWall.Right: dir = (Vector3.right * extents.x) + (Vector3.forward * shift); break;
            }


            return parentRoom.localBoundsPos + dir + (Vector3.up * height);
        }

        public DoorWall GetCompatibleDoorWall()
        {
            DoorWall compatibleDoorWall = DoorWall.Front;

            switch (doorWall)
            {
                case DoorWall.Front: compatibleDoorWall = DoorWall.Back; break;
                case DoorWall.Back: compatibleDoorWall = DoorWall.Front; break;
                case DoorWall.Left: compatibleDoorWall = DoorWall.Right; break;
                case DoorWall.Right: compatibleDoorWall = DoorWall.Left; break;
            }

            return compatibleDoorWall;
        }

    }

}