using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    public float xSpeed, ySpeed, zSpeed;

    // Update is called once per frame
    void FixedUpdate()
    {
        //transform.Rotate(new Vector3(xSpeed, spinSpeed, xSpeed));
        transform.Rotate(new Vector3(xSpeed * Time.deltaTime, ySpeed * Time.deltaTime, zSpeed * Time.deltaTime));
    }
}
