using Clonk.LevelGenerator;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FadeoutController : MonoBehaviour
{

    private float fadeSpeed = 3f;

    private List<MeshRenderer> meshRenderers = new List<MeshRenderer>();

    private float currentFade = 0f;


    private void Start()
    {
        meshRenderers.AddRange(GetComponentsInChildren<MeshRenderer>().ToList());
        AddRoomDoors(GetComponent<Room>().doors);



        foreach (MeshRenderer meshRenderer in meshRenderers)
        {
            meshRenderer.material.SetFloat("_Fadeout", 0f);
        }
    }

    public void AddRoomDoors(List<Doorhole> doorHoles)
    {
        foreach(Doorhole doorHole in doorHoles) meshRenderers.AddRange(doorHole.inDoorHole.GetComponentsInChildren<MeshRenderer>().ToList());
    }

    public void FadeOut()
    {
        StopAllCoroutines();
        StartCoroutine(FadeTo(.85f));
    }

    public void FadeIn()
    {
        StopAllCoroutines();
        StartCoroutine(FadeTo(1f));
    }

    private IEnumerator FadeTo(float targetFade)
    {
        float startingFade = currentFade;

        bool isFadeOut = startingFade > targetFade;

        for (float t = 0f; t < 1f; t += fadeSpeed * Time.deltaTime)
        {
            currentFade = Mathf.SmoothStep(startingFade, targetFade, t);

            

            foreach (MeshRenderer meshRenderer in meshRenderers)
            {
                float matCurrentFade = meshRenderer.material.GetFloat("_Fadeout");

                bool shouldUpdate = (isFadeOut && matCurrentFade > currentFade) || (!isFadeOut && matCurrentFade < currentFade);

                if (shouldUpdate)
                {
                    meshRenderer.material.SetFloat("_Fadeout", currentFade);
                }
            }

            yield return null;
        }

        foreach (MeshRenderer meshRenderer in meshRenderers)
        {
            meshRenderer.material.SetFloat("_Fadeout", targetFade);
        }
    }

}
