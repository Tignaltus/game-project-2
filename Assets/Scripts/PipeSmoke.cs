using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSmoke : MonoBehaviour
{
    public float force = 3f;
    private void HitEntity(Entity otherEntity)
    {
        Vector3 dir = transform.right * force;
        
        if (otherEntity is Character character)
        {
            otherEntity.GetComponent<CharacterMovement>().AddKnockback(dir);
        }
        
        //otherEntity.GetComponent<CharacterMovement>().Move(new Vector3(xDir, 0, zDir));
    }

    private void OnTriggerStay(Collider other)
    {
        var hitEntity = other.GetComponent<Entity>();
        HitEntity(hitEntity);
    }
}
