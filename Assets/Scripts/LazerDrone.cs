using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerDrone : MonoBehaviour
{
    public float speed = 0.1f;


    public List<Vector3> path;

    public bool isLooping = true;

    public bool reverseDir;

    /*[Range(0, 8)] */public int currentValue;

    private Vector3 smoothPos;

    private Vector3 smoothVel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //transform.Translate(Vector3.forward * speed * Time.deltaTime);
        float val = isLooping ? Mathf.Repeat(currentValue, path.Count) : Mathf.PingPong(currentValue, path.Count - 1);
        transform.localPosition = Vector3.MoveTowards(transform.localPosition, path[currentValue], speed * Time.deltaTime);

        

        smoothPos = Vector3.SmoothDamp(smoothPos, path[Mathf.RoundToInt(val)], ref smoothVel, .1f);

        if(transform.localPosition == path[currentValue])
        {
            //currentValue++;
            ChangeDirection();
        }

        
    }
    [ContextMenu("ChangeDirection")]
    public void ChangeDirection()
    {
        if (isLooping)
        {
            if (currentValue >= path.Count - 1)
            {
                currentValue = 0;
            }
            else
            {
                currentValue++;
            }
        } 
        else
        {
            if (reverseDir)
            {
                if (currentValue <= 0)
                {
                    currentValue = path.Count - 1;
                }
                else if (currentValue == 1)
                {
                    currentValue--;
                    reverseDir = false;
                }
                else
                {
                    currentValue--;
                }
            }
            else
            {
                if (currentValue >= path.Count - 1)
                {
                    currentValue = 0;
                    reverseDir = true;
                }
                else
                {
                    currentValue++;
                }
            }
            
        }
        
    }

    private void OnDrawGizmos()
    {

        Gizmos.color = Color.white;
        foreach (Vector3 v3 in path)
        {
            Gizmos.DrawWireSphere(v3, .25f);
        }

        Gizmos.color = Color.blue;


        Gizmos.DrawSphere(smoothPos, .3f);
    }
}
