using System;
using System.Collections;
using System.Collections.Generic;
using Clonk;
using TMPro;
using UnityEngine;

public class WeaponUI : MonoBehaviour
{
    public Character Character;
    public WeaponStats WeaponStats;
    public TextMeshProUGUI StatsText;
    public TextMeshProUGUI PowerupName;
    public TextMeshProUGUI PowerupDescription;

    private float hideTime;
    public static WeaponUI instance { get; private set; }


    void Start()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    private void OnEnable()
    {
        Character.OnAddedUpgrade += UpdateStats;
    }

    private void OnDisable()
    {
        Character.OnAddedUpgrade -= UpdateStats;
    }

    public void Init(Character character)
    {
        Character = character;
        WeaponStats = character.myWeapon.WeaponStats;
        UpdateStats(WeaponStats);
        Character.OnAddedUpgrade += UpdateStats;
    }

    public void UpdateStats(WeaponStats newStats)
    {
        WeaponStats = newStats;
        StatsText.text = WeaponStats.ToString();
    }

    public void ShowPowerup(PowerUpData powerup)
    {
        PowerupName.text = powerup.name;
        PowerupDescription.text = powerup.description;
        hideTime = Time.time + 2.9f;
        StartCoroutine(hidePowerup());

    }

    private IEnumerator hidePowerup()
    {
        yield return new WaitForSeconds(3);
        if (hideTime < Time.time)
        {
            PowerupName.text = "";
            PowerupDescription.text = "";
        }
    }


}
