using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupIconManager : MonoBehaviour
{
    private List<PowerUpData> powerUpDatas;

    private Dictionary<string, powerupIconInfo> powerupAmountDictionary;
    public PowerupIcon iconPrefab;

    public bool BossBar = false;
    

    private void Start()
    {
        if (BossBar)
        {
            GameManager.instance.AIManager.onBossSpawned += UpdateList;
        }
        else
        {
            GameManager.instance.PowerUpManager.onPowerupChosen += UpdateList;
        }

    }

    public void UpdateList(List<PowerUpData> powerups)
    {
        powerUpDatas = powerups;
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        powerupAmountDictionary = new Dictionary<string, powerupIconInfo>();
        
        foreach (var powerUpData in powerUpDatas)
        {
            powerupIconInfo powerupIcon;
            if (powerupAmountDictionary.TryGetValue(powerUpData.name,out powerupIcon))
            {
                powerupIcon.amount++;
            }
            else
            {
                powerupAmountDictionary.Add(powerUpData.name,
                    new powerupIconInfo()
                    {
                        name = powerUpData.name,
                        icon = powerUpData.powerUpImage,
                        amount = 1
                    });
            }
        }

        foreach (var pair in powerupAmountDictionary)
        {
            var icon = Instantiate(iconPrefab, transform);
            icon.amount.text = pair.Value.amount.ToString();
            icon.icon.sprite = pair.Value.icon;
        }
        
    }


    class powerupIconInfo
    {
        public string  name;
        public Sprite  icon;
        public int     amount;
    }





}
