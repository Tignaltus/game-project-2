using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Clonk;
using UnityEngine;
using Random = UnityEngine.Random;

public class PowerupPickup : MonoBehaviour
{
    // Start is called before the first frame update

    private PowerUpData _powerUpData;
    
    
    void Start()
    {
        _powerUpData = PowerUpManager.GetRandomPowerUp();

    }
    private void OnTriggerEnter(Collider other)
    {
        var hitEntity = other.GetComponent<Character>();
        if (hitEntity != null)
        {
            //PowerUpManager.instance.SpawnNewPowerups(hitEntity);
            Destroy(gameObject);
        }
    }
}
