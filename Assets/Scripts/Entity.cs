using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace Clonk
{
    public class Entity : MonoBehaviour
    {
        #region ----------Actions-----------

        public Action OnDeath;
        public Action<float, float> OnHealthUpdated;
        public Action<float> OnTookDamage;

        #endregion

        #region ----------Varibles----------

        /// <summary>
        /// Protected Variables
        ///
        /// These are for this object only,
        /// We dont want any outside class changing these directly
        /// 
        /// </summary>
        [SerializeField] protected float healthMaxBase = 1;
        [SerializeField] protected float healthCurrent = 1;
        [SerializeField] protected int team = 0;
        protected float modifiedHealthMax;

        /// <summary>
        /// Public Variables
        ///
        /// these are to give other classes information about this entity
        ///
        /// </summary>
        public float HealthMax => modifiedHealthMax;
        public float HealthCurrent => healthCurrent;
        public int Team => team;

        public void SetTeam(int team)
        {
            this.team = team;
        }

        public bool IsDead { get; private set; } = false; //This Is a public bool with a private setter. it can be reached from anywhere, but only change from this class

        #endregion

        private void Start()
        {
            modifiedHealthMax = healthMaxBase;
        }

        protected virtual void OnEnable()
        {
            //healthCurrent = 1;
            modifiedHealthMax = healthMaxBase;
            IsDead = false;
        }

        public void TriggerDeath()
        {
            if (!IsDead)
            {
                OnDeath?.Invoke();
                IsDead = true;
            }
        }

        public void ChangeHealth(float change, bool relative = false)
        {
            var health = math.clamp(relative ? healthCurrent + change : change, 0, modifiedHealthMax);

            if(change < 0f) OnTookDamage?.Invoke(change);


            healthCurrent = health;
            OnHealthUpdated?.Invoke(healthCurrent,modifiedHealthMax);
           
            if (healthCurrent == 0)
            {
                TriggerDeath();
            }
        }
        
        public void ChangeMaxHealth(float change, bool relative = false)
        {
            var newHealthMax = relative ? modifiedHealthMax + change : change;
            modifiedHealthMax = newHealthMax;
            if (healthCurrent > modifiedHealthMax)
            {
                ChangeHealth(modifiedHealthMax);
            }
            OnHealthUpdated?.Invoke(healthCurrent,modifiedHealthMax);
        }
    }
}
