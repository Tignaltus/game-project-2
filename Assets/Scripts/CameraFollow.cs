using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject target;
    public Vector3 offset = new Vector3(0, 10, 10);

    private void Start()
    {
        target = GameObject.Find("PlayerCharacterNew(Clone)");
    }

    private void Update()
    {
        gameObject.transform.position = target.transform.position + offset;
    }
}
