using Clonk.LevelGenerator;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BossDeath : MonoBehaviour
{
    public int score;
    public float delay;

    public GameEvent[] gameEvents;

    private void Start()
    {
        GameManager.instance.ScoreManager.AddScore(score);
        StartCoroutine(TriggerGameOver());
    }
    
    IEnumerator TriggerGameOver()
    {
        yield return new WaitForSeconds(delay);
        //GameManager.instance.GameplayUIManager.TriggerGameOver("YOU win");
        //GameEventManager.FireEvent()

        LevelGenerator.instance.GenerateLevel();
        GameManager.instance.GetPlayer().Character.transform.position = Vector3.zero;

        PowerUpManager.AddBossPowerUpsToEnemyPowerUps();
        //GameManager.instance.AIManager
        foreach (AIInputManager ai in GameManager.instance.AIManager.enemyList) ai.GetCharacter().TriggerDeath();

        GameManager.instance.GameplayUIManager.BossHealthbar.gameObject.SetActive(false);
    }

}
