using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    //public int damage;
    //public int bulletAmount;
    //public float shotSpeed;
    //public float range;
    //public float fireRate;
    //public float homingAmount;
    public float Spread;
    public float projectileSize;
    public float projectileFriction;
    public float timeToLive;
*/



public interface IPowerUpHealthMaxModifier
{
    int ModifyHealthMax(int HealthMax);
}

public interface IPowerUpMovementSpeedModifier
{
    float ModifyMovementSpeed(float MovementSpeed);
}

public interface IPowerUpRotationSpeedModifier
{
    float ModifyRotationSpeed(float RotationSpeed);
}

public interface IPowerUpDamageModifier
{
    int ModifyDamage(int Damage);
}
public interface IPowerUpBulletAmountModifier
{
    int ModifyBulletAmount(int BulletAmount);
}
public interface IPowerUpShotSpeedModifier
{
    float ModifyShotSpeed(float ShotSpeed);
}
public interface IPowerUpRangeModifier
{
    float ModifyRange(float Range);
}
public interface IPowerUpFireRateModifier
{
    float ModifyFireRate(float FireRate);
}
public interface IPowerUpHomingAmountModifier
{
    float ModifyHomingAmount(float HomingAmount);
}
public interface IPowerUpSpreadModifier
{
    float ModifySpread(float Spread);
}
public interface IPowerUpProjectileSizeModifier
{
    float ModifyProjectileSize(float ProjectileSize);
}
public interface IPowerUpProjectileFrictionModifier
{
    float ModifyProjectileFriction(float ProjectileFriction);
}
public interface IPowerUpTimeToLiveModifier
{
    float ModifyTimeToLive(float TimeToLive);
}
public interface IPowerUpBounceModifier
{
}