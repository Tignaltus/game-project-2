using System;using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Clonk.Stats;

#if UNITY_EDITOR
using UnityEditor;
#endif


[Serializable]
[CreateAssetMenu(menuName = "Scriptable Objects/PowerUp")]
public class PowerUpData : ScriptableObject
{
    public string name;
    [TextArea]
    public string description;
    public Sprite powerUpImage;
    [Space]
    [Header("MODIFIERS:")]
    public PowerUpStatModifier[] modifiers;
    [Header("FLAGS:")]
    public PowerUpFlagModifier flagModifier;
    public int id;
    public int typeID;

    public StatModifier[] GetStatModifiers()
    {
        StatModifier[] _modifiers = new StatModifier[modifiers.Length];
        for (int i = 0; i <= _modifiers.Length-1; i++)
        {
            _modifiers[i] = new StatModifier(modifiers[i].Value, modifiers[i].ModType,this);
        }
        return _modifiers;
    }
    public StatType GetStatType(int index)
    {
        StatType statType = StatType.INVALID;
        if (modifiers[index] != null)
        {
            statType = modifiers[index].StatType;
        }

        return statType;
    }

    public FlagModifier GetFlags()
    {
        var _flag = new FlagModifier(flagModifier.flags,this);
        return _flag;
    }


}

[Serializable]
public class PowerUpStatModifier
{
    public StatType StatType;
    public StatModType ModType;
    public  float Value;
}
[Serializable]
public class PowerUpFlagModifier
{
    [EnumFlags] public SpecialBulletProperties flags;
}

