using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BogusIdle : AIState
{
    public BogusChase moveState;
    public BogusAction actionState;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        inputManager.isPreformingAction = false;
        inputManager.movementCurrent = Vector3.zero;

        if (inputManager.target == null)
        {
            return this;
        }
        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) < inputManager.triggerAreaDistance)
        {
            //return moveState;
        }
        return this;
    }
}
