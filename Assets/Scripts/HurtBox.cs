using Clonk;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class HurtBox : MonoBehaviour
{
    public float damage = 0.2f;
    public float delay = .1f;

    public List<Entity> entityQueue;

    private void HitEntity(Entity otherEntity)
    {
        otherEntity.ChangeHealth(-damage, true);
        if (otherEntity is Character character)
        {
            character.Animator.DoSquash();
        }
    }

    IEnumerator removeCoroutine(float delay, Entity entityToRemove)
    {
        yield return new WaitForSeconds(delay);
        entityQueue.Remove(entityToRemove);
    }

    private void OnTriggerStay(Collider other)
    {
        
        var hitEntity = other.GetComponent<Entity>();

        if(hitEntity != null && !entityQueue.Contains(hitEntity))
        {
            GameManager.instance.EffectManager.CreateEffect(EffectType.GrindEffect,hitEntity.transform.position.Flatten(0),Quaternion.identity,Vector3.one);
            AudioManager.Instance.Play(AudioManager.Sounds.Grinder);
            HitEntity(hitEntity);
            entityQueue.Add(hitEntity);
            StartCoroutine(removeCoroutine(delay,hitEntity));
        }
    }
}
