using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BogusChase : AIState
{
    /*private float timeUntilSpawn = 6;
    private bool spawnDone = true;*/

    public BogusIdle idleState;
    public BogusAction actionState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        //var node = inputManager.navigator.path.corners[1];
        //inputManager.movementCurrent = (node - character.transform.position).normalized;
        //inputManager.GetCharacter().Animator.SetBool("Moving", false);

        if (inputManager.target == null)
        {
            return idleState;
        }

        if (inputManager.target != null)
        {
            var node = inputManager.navigator.path.corners[1];
            inputManager.movementCurrent = (node - character.transform.position).normalized;
        }

        if (Vector3.Distance(character.transform.position, inputManager.target.transform.position) > inputManager.triggerAreaDistance)
        {
            inputManager.target = null;
            return idleState;
        }

        if (character != null && inputManager.target != null)
        {
            inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);
        }

        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) < character.myWeapon.WeaponStats.GetStatValue(StatType.Range) - 0.5f)
        {
            //return attackState;
        }

        inputManager.GetCharacter().Animator.SetBool("Moving", true);
        return this;
    }
}
