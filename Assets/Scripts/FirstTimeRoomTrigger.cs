using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class FirstTimeRoomTrigger : MonoBehaviour
{
    private BoxCollider trigger;

    private void Start()
    {
        trigger = GetComponent<BoxCollider>();
        trigger.isTrigger = true;
    }

    public void SetupTrigger(Vector3 center, Vector3 size)
    {
        if(trigger == null) trigger = GetComponent<BoxCollider>();

        trigger.center = center;
        trigger.size = size;
    }

    private void OnTriggerEnter(Collider other)
    {
        Entity enteringEntity = other.gameObject.GetComponent<Entity>();

        if (enteringEntity != null && enteringEntity.Team == 1)
        {
            //StartCoroutine(PowerUpManager.instance.ChoosePowerup());

            foreach (SpawnPoint point in GetComponentsInChildren<SpawnPoint>())
            {
                Vector3 spawnPosition = point.transform.position.Flatten(0);
                CharacterScriptableObject characterScriptable = point.spawnableEnemies[Random.Range(0, point.spawnableEnemies.Length)];
                if (characterScriptable.isBoss)
                {
                    GameManager.instance.AIManager.CreateBoss(characterScriptable, spawnPosition, Quaternion.identity);
                }
                else
                {
                    GameManager.instance.AIManager.CreateEnemy(characterScriptable, spawnPosition, Quaternion.identity);
                }
            }

            trigger.enabled = false;
        }
    }
}
