using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtension
{
    /*
    public static InputType GetRandomItem<InputType> (List<InputType> list)
    {
        int index = Random.Range(0, list.Count);

        InputType result = list[index];

        return result;
    }
    */


    public static InputType GetRandomItem<InputType>(this List<InputType> list)
    {
        int index = Random.Range(0, list.Count);

        InputType result = list[index];

        return result;
    }

}
