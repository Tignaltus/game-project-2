using Clonk;
using System.Collections.Generic;
using UnityEngine;

public class UIMinimap : MonoBehaviour
{

    public RectTransform rotator;
    public RectTransform minimapRoomPrefab;


    public RectTransform roomContainer;
    public RectTransform playerMarker;

    public float scale;
    public float sizeScale;

    [System.Serializable]
    public struct MapRoom
    {
        public Vector2 position;
        public Vector2 size;
        public RectTransform uiObject;

        
        public MapRoom(Vector2 position, Vector2 size, RectTransform uiObject)
        {
            this.position = position;
            this.size = size;
            this.uiObject = uiObject;
        }
    }

    public List<MapRoom> mapRooms = new List<MapRoom>();

    private Camera mainCam;
    private Transform playerTransform;

    private void Start()
    {
        mainCam = Camera.main;
    }

    private void Update()
    {
        rotator.rotation = Quaternion.LookRotation(Vector3.down, Vector3.right);

        CharacterHandler player = GameManager.instance.GetPlayer();

        if (player != null)
        {
            Vector2 playerPos = player.Character.transform.position.FlatVector3ToVector2();

            foreach (MapRoom mapRoom in mapRooms)
            {
                mapRoom.uiObject.localPosition = (mapRoom.position - playerPos) * scale;
                mapRoom.uiObject.sizeDelta = mapRoom.size * scale;
            }
        }
    }

    public void AddMapRoom(Vector2 position, Vector2 size)
    {
        RectTransform rectTransform = Instantiate(minimapRoomPrefab, roomContainer);


        MapRoom mapRoom = new MapRoom(position, size, rectTransform);

        mapRooms.Add(mapRoom);
    }
}


