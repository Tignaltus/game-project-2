namespace Clonk.Stats
{
    public class FlagModifier
    {
        public readonly SpecialBulletProperties Flag;
        public readonly object Source;

        public FlagModifier(SpecialBulletProperties flag ,object source)
        {
            Flag = flag;
            Source = source;
        }
        
        public FlagModifier(SpecialBulletProperties flag): this(flag,null) { }

    }
}