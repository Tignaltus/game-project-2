using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace Clonk.Stats
{
    [Serializable]
    public class FlagManager
    {
        public SpecialBulletProperties BaseFlags;
        
        public virtual SpecialBulletProperties Flags {
            get {
                if (isDirty || BaseFlags != lastBaseValue)
                {
                    lastBaseValue = BaseFlags;
                    _flags =  CalculateFinalValue();
                    isDirty = false;
                }
                return _flags;
            }
        }

        protected bool isDirty = true;
        protected SpecialBulletProperties _flags;
        protected SpecialBulletProperties lastBaseValue = SpecialBulletProperties.None;
        
        protected readonly List<FlagModifier> flagModifiers;
        public readonly ReadOnlyCollection<FlagModifier> FlagModifiers;

        public FlagManager()
        {
            flagModifiers = new List<FlagModifier>();
            FlagModifiers = flagModifiers.AsReadOnly();
        }

        public virtual void AddModifier(FlagModifier mod)
        {
            isDirty = true;
            flagModifiers.Add(mod);
        }

        public virtual bool RemoveModifier(FlagModifier mod)
        {
            if (flagModifiers.Remove(mod))
            {
                isDirty = true;
                return true;
            }

            return false;
        }

        public virtual bool RemoveAllModifiersFromSource(object source)
        {
            bool didRemove = false;
            
            for(int i = flagModifiers.Count - 1; i >= 0;i--)
            {
                if (flagModifiers[i].Source == source)
                {
                    isDirty = true;
                    didRemove = true;
                    flagModifiers.RemoveAt(i);
                }
            }

            return didRemove;

        }
        
        protected virtual SpecialBulletProperties CalculateFinalValue()
        {
            SpecialBulletProperties finalFlags = BaseFlags;
            for (int i = 0; i < flagModifiers.Count; i++)
            {
                FlagModifier mod = flagModifiers[i];
                finalFlags |= mod.Flag;
            }
            return finalFlags;

        }
        
    }

    [Flags] public enum SpecialBulletProperties
    {
        None = 0,
        Homing          = 1 << 0,
        Piercing        = 1 << 1,
        Phasing         = 1 << 2,
        Bounce          = 1 << 3,
        Fire            = 1 << 4,
        Electricity     = 1 << 5,
        Water           = 1 << 6,
        Explosive       = 1 << 7,
        ArmorPiercing   = 1 << 8
    }
}