using UnityEngine;


/*

[CreateAssetMenu(menuName = "Scriptable Objects/Stats/StatType")]
public class StatType : ScriptableObject
{
    public float defaultValue = 0;
}
*/


public enum StatType
{
    INVALID = 0,
    BulletAmount,
    BulletSize,
    BulletSpeed,
    BulletSpread,
    Damage,
    FireRate,
    MovementSpeed,
    Range,
    HomingAmount,
    BounceAmount,
    Health,
    MaxHealth,
    Piercing,
    Knockback,
    LifeSteal,
    ExplodingBullet,
}


