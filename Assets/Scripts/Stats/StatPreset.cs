using System;
using System.Collections.Generic;
using UnityEngine;

namespace Clonk.Stats
{
    [CreateAssetMenu(menuName = "Scriptable Objects/Stats/StatsPreset")]
    public class StatPreset : ScriptableObject
    {
        public PresetStat[] Stats;
    }

    [Serializable]
    public class PresetStat
    {
        public StatType Type;
        public float Value;

        PresetStat(StatType type, float value = 0)
        {
            Type = type;
            Value = value;
        }

    }
    
}