using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Clonk.Stats.Utilities
{
    public static class StatsUtilities
    {
       
        public static Stat getStatOfType(this Stat[] stats,StatType type)
        {
            Stat returnStat = null;
            foreach (Stat stat in stats)
            {
                if (stat.StatType == type)
                {
                    returnStat = stat;
                }
            }

            if (returnStat == null)
            {
                Debug.LogWarning($"Did Not find any Stat With the name {type.ToString()} in this context");
            }

            return returnStat;
        }

    }
    
}