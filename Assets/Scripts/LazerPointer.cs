using Clonk;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LazerPointer : MonoBehaviour
{
    public LayerMask layerMask;
    public float maxRange = 100;
    private LineRenderer lr;
    public Character character;

    private void Awake()
    {
        lr = GetComponent<LineRenderer>();
        lr.SetPosition(0, new Vector3(0, 0, 0));
    }

    void Update()
    {
        if(character != null)
        {
            maxRange = character.myWeapon.WeaponStats.GetStatValue(StatType.Range);
        }
        
        lr.SetPosition(0,transform.position);
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, maxRange, layerMask))
        {
            if (hit.collider)
            {
                lr.SetPosition(1, hit.point);
            }
        }
        else
        {
            lr.SetPosition(1,transform.position+transform.forward*maxRange);
        }



    }
}
