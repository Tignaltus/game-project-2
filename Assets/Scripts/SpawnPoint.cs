using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    
    public CharacterScriptableObject[] spawnableEnemies;

    public Color col = new Color(1, 0, 0, .5f);

    
    
    
    private void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position + Vector3.up ,"spawnpointIcon.png",false,Color.white);
        Gizmos.color = col;
        Gizmos.DrawWireCube(transform.position + Vector3.up,new Vector3(2,2,2));
    }
}
