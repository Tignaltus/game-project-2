using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectPool
{
    public GameObject prefab;
    public int poolSize = 1;
    public List<GameObject> pooledObjects = new List<GameObject>();
    public Queue<GameObject> queue = new Queue<GameObject>();
    public Transform objectParent;

    public int key;

    public ObjectPool(GameObject prefab, Transform poolParent)
    {
        this.prefab = prefab;

        key = prefab.GetInstanceID();

        objectParent = new GameObject($"{prefab.name} POOL").transform;

        objectParent.parent = poolParent;
    }

    public bool HasInactiveObjects()
    {
        foreach (GameObject gameObject in pooledObjects)
        {
            if (!gameObject.activeInHierarchy)
            {
                return true;
            }
        }

        return false;
    }

    public bool TryGetInactiveObject(out GameObject inactiveObject)
    {
        inactiveObject = null;

        foreach (GameObject instance in pooledObjects)
        {
            if (!instance.activeInHierarchy)
            {
                inactiveObject = instance;
                return true;
            }
        }

        return false;
    }
}

public class ObjectPooler : MonoBehaviour
{
    public bool useObjectPooler = true;

    public List<ObjectPool> objectPools;

    public Dictionary<int, ObjectPool> objectPoolDictionary = new Dictionary<int, ObjectPool>();

    public static ObjectPooler SharedInstance;

    private void Awake()
    {
        SharedInstance = this;
    }

    private void Start()
    {
        foreach (ObjectPool pool in objectPools)
        {
            FillObjectPool(pool);
        }
    }

    public ObjectPool CreateNewPool(GameObject prefab)
    {
        ObjectPool pool = new ObjectPool(prefab, transform);

        objectPoolDictionary.Add(pool.key, pool);

        return pool;
    }

    public GameObject FillObjectPool(ObjectPool objectPool, bool returnActive = false)
    {
        objectPool.pooledObjects = new List<GameObject>();
        GameObject newObject;
        for (int i = 0; i < objectPool.poolSize; i++)
        {
            newObject = Instantiate(objectPool.prefab, objectPool.objectParent);
            newObject.SetActive(false);

            objectPool.pooledObjects.Add(newObject);

            //objectPool.poolQueue.Enqueue(tmp);
        }

        int poolKey = objectPool.prefab.GetInstanceID();

        objectPoolDictionary.Add(poolKey, objectPool);

        objectPool.pooledObjects[0].SetActive(returnActive);

        return objectPool.pooledObjects[0];
        //return objectPool.poolQueue.Dequeue();
    }

    private GameObject AddNewInstanceToPool(ObjectPool pool)
    {
        GameObject instance = Instantiate(pool.prefab, pool.objectParent);

        instance.SetActive(false);

        pool.pooledObjects.Add(instance);
        pool.queue.Enqueue(instance);

        return instance;
    }

    public GameObject GetPooledObject(GameObject prefab, bool returnActive = true)
    {
        GameObject instance;

        if (useObjectPooler)
        {
            int poolKey = prefab.GetInstanceID();

            bool poolExists = objectPoolDictionary.TryGetValue(poolKey, out ObjectPool pool);


            // if pool does not exist, create a new pool.
            if (!poolExists)
            {
                pool = CreateNewPool(prefab);
            }

            /*if (pool.queue.Count == 0)
            {
                instance = AddNewInstanceToPool(pool);
            }


            instance = pool.queue.Dequeue();*/

            if (!pool.TryGetInactiveObject(out instance))
            {
                instance = AddNewInstanceToPool(pool);
            }

            if (instance != null)
            {

                instance.SetActive(returnActive);

            }
        }
        else
        {
            instance = Instantiate(prefab);
        }

        return instance;
    }

    public GameObject GetPooledObject(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        GameObject instance;

        if (useObjectPooler)
        {
            instance = GetPooledObject(prefab, false);

            if (instance != null)
            {
                instance.transform.SetPositionAndRotation(position, rotation);

                instance.SetActive(true);
            }
        }
        else
        {
            instance = Instantiate(prefab, position, rotation);
        }

        return instance;
    }

    public void Deactivate(GameObject instance)
    {
        if (useObjectPooler)
        {
            instance.SetActive(false);
        }
        else
        {
            Destroy(instance);
        }
    }

    public void Deactivate(GameObject instance, float time)
    {
        if (useObjectPooler)
        {
            StartCoroutine(DeactivateAfterTime(instance, time));
        }
        else
        {
            Destroy(instance, time);
        }
    }

    public IEnumerator DeactivateAfterTime(GameObject instance, float time)
    {
        yield return new WaitForSeconds(time);

        Deactivate(instance);
        //gameObject.SetActive(false);
    }

}