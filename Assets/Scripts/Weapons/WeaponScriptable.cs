using System;
using System.Collections;
using System.Collections.Generic;
using Clonk;
using Clonk.Stats;
using Clonk.Stats.Utilities;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "Scriptable Objects/Weapon")]
public class WeaponScriptable : ScriptableObject
{
    public WeaponStats WeaponStats;
} 




[Serializable]
public struct WeaponStats
{
    public StatPreset statPreset;
    public Stat[] Stats;
    public GameObject BaseProjectile;
    public Action<Entity> OnHit;
    public FlagManager FlagManager;
    public AudioManager.Sounds ShootEffect;
    public GameObject ShootEffectPrefab;

    public float GetStatValue(StatType type)
    {
        float value = 0;
        Stat stat = Stats.getStatOfType(type);
        if (stat != null)
        {
            value = stat.Value;
        }
        return value;
    }



    public override string ToString()
    {
        string tmp = "";
        foreach (var stat in Stats)
        {
            if (stat.StatType == StatType.HomingAmount)
            {
                if (FlagManager.Flags.HasFlag(SpecialBulletProperties.Homing))
                {
                    tmp += stat.StatType + ": " + stat.Value + "\n";
                }
            }
            else
            {
                tmp += stat.StatType + ": " + stat.Value + "\n";
            }

        }
        tmp += (FlagManager.Flags.HasFlag(SpecialBulletProperties.Bounce)?"Bouncing\n":"");
        return tmp;

    }
}




