using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Clonk;
using Clonk.Stats;
using Clonk.Stats.Utilities;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

//TODO: Make Movement of Projectile in direction
//TODO: Set projectiles team to the same as the shooters
//TODO: Check collision on other "Entity"s that are not the same team
//TODO: Deal Damage and Destroy the Projectile Object

public class SpawnProjectile : Entity
{
    private float projectileSpeed;
    private Vector3 projectileOrigin;
    private Vector3 projectileDirection;
    
    
    public Transform ProjectileHeight;

    private float height = 0;
    public CharacterScriptableObject spawnableEnemy;
    public float gravity = 5f;
    public float zSpeed;
    public bool isBoss = false;

    [SerializeField]private LayerMask bounceMask;
    

    //=== Private Methods ===
    
    private void Start()
    {
        OnDeath += () =>
        {
            //Destroy(gameObject);
            ObjectPooler.SharedInstance.Deactivate(gameObject);
            if (isBoss)
            {
                GameManager.instance.AIManager.CreateBoss(spawnableEnemy, transform.position.Flatten(0), transform.rotation);
            }
            else
            {
                GameManager.instance.AIManager.CreateEnemy(spawnableEnemy, transform.position.Flatten(0), transform.rotation);
            }

        };
    }
    
    
    //======== PROJECTILE MOVEMENT METHODS =================
    
    
    private void Move()
    {

        projectileDirection = CalculateBounce(projectileDirection);

        CalculatePhysics();

        transform.position += projectileDirection * (projectileSpeed * Time.deltaTime);
        transform.rotation = Quaternion.LookRotation(projectileDirection,Vector3.up);


    }

    private void CalculatePhysics()
    {
        zSpeed -= gravity * Time.deltaTime;
        ProjectileHeight.localPosition += Vector3.up * zSpeed * Time.deltaTime;
        if(ProjectileHeight.localPosition.y <= 0)
        {
            ChangeHealth(0, false);
            ProjectileHeight.localPosition = ProjectileHeight.localPosition.Flatten(0);
        }
    }

    

    private Vector3 CalculateBounce(Vector3 direction)
    {
        var reflectDir = direction;
        
        Ray ray = new Ray(transform.position, direction);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Time.deltaTime * projectileSpeed + .1f, bounceMask))
        {
            reflectDir = Vector3.Reflect(ray.direction, hit.normal);
        }

        return reflectDir;

    }

    

    //======================================================

    private void Update()
    {
        ProjectileUpdate();
    }


    

    public void InitializeProjectile(float projectileSpeed, Vector3 position, Quaternion direction, int team)
    {
        this.team = team;
        this.projectileSpeed = projectileSpeed;
        projectileOrigin = position;
        var tempDirection = direction * Vector3.forward;
        projectileDirection = tempDirection.Flatten(0);
        zSpeed = tempDirection.y * projectileSpeed;
        this.projectileSpeed = projectileSpeed * projectileDirection.magnitude;
        height = position.y;
        ProjectileHeight.localPosition = new Vector3(0,height,0);
    }
    
    //=== Public Methods ===
    public void ProjectileUpdate()
    {
        Move();
    }
}
