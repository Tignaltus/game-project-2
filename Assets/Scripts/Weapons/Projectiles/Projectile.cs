using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Clonk;
using Clonk.Stats;
using Clonk.Stats.Utilities;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

//TODO: Make Movement of Projectile in direction
//TODO: Set projectiles team to the same as the shooters
//TODO: Check collision on other "Entity"s that are not the same team
//TODO: Deal Damage and Destroy the Projectile Object

public class Projectile : Entity
{
    private static int activeProjectileCount = 0;

    private float projectileSpeed;
    private float projectileLifeTime;
    private Vector3 projectileOrigin;
    private Vector3 projectileDirection;
    
    public Transform ProjectileHeight;
    public GameObject normalProjectile;
    public GameObject piercingProjectile;

    private float height = 0;
    
    

    [SerializeField]private float homingRange = 10;
    [SerializeField]private Entity homingTarget = null;
    [SerializeField]private LayerMask homingMask;
    private int bounceAmount = 0;
    private int piercingAmount = 0;
    private int knockbackAmount = 0;



    [SerializeField]private LayerMask bounceMask;

    [SerializeField] private GameObject trail;
    [SerializeField] private EffectType hitEffect;
    public ExplosionHitBox explosionPrefab;

    public WeaponStats WeaponStats;
    public Character owner;


    private int updates = 0;
    private int raycastSpacing = 5;

    private bool raycastHit = false;

    //=== Private Methods ===
    
    private void Start()
    {
        OnDeath += () =>
        {
            //Destroy(gameObject);
            ObjectPooler.SharedInstance.Deactivate(gameObject);
            if (trail != null)
            {
                //trail.transform.parent = null;
                //Destroy(trail,1);
                //ObjectPooler.SharedInstance.Deactivate(gameObject, 1f);
            }
        };
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        updates = 0;

        // InitializeProjectile(WeaponStats, transform.position, transform.rotation, team);
        activeProjectileCount++;
    }

    private void OnDisable()
    {
        activeProjectileCount--;
    }


    //======== PROJECTILE MOVEMENT METHODS =================


    private void Move()
    {
        
        //-------  CHANGE DIRECTION DEPENDING ON SPECIFIC PROPERTIES  --------
        #region SpecificProperties
        if (WeaponStats.FlagManager.Flags.HasFlag(SpecialBulletProperties.Homing))
        {
            projectileDirection = CalculateHoming(projectileDirection);
        }
        if (WeaponStats.FlagManager.Flags.HasFlag(SpecialBulletProperties.Bounce) && bounceAmount > 0)
        {
            projectileDirection = CalculateBounce(projectileDirection);
        }
        else
        {

            int spacing = 1 + (Mathf.RoundToInt(activeProjectileCount * .01f));

            if (updates % spacing == 0)
            {
                Ray ray = new Ray(transform.position, projectileDirection);
                raycastHit = Physics.Raycast(ray, Time.deltaTime * spacing * projectileSpeed + .1f, bounceMask);
            }

            updates++;

            //Ray ray = new Ray(transform.position, projectileDirection);
            //if (Physics.Raycast(ray, Time.deltaTime * projectileSpeed + .1f, bounceMask))
            if (raycastHit)
            {
                ChangeHealth(0);
                if(WeaponStats.GetStatValue(StatType.ExplodingBullet) <= 0 && bounceAmount >= 0)
                {
                    GameManager.instance.EffectManager.CreateEffect(hitEffect, transform.position, Quaternion.identity, transform.localScale);
                    AudioManager.Instance.Play(AudioManager.Sounds.Bullet_Hit);
                } else
                {
                    explosionPrefab.projectileOwner = this;
                    Instantiate(explosionPrefab, transform.position, transform.rotation);
                    //AudioManager.Instance.Play(AudioManager.Sounds.Boombot_Explode);
                    AudioManager.Instance.Play(AudioManager.Sounds.Bullet_Hit);
                }
                
            }
        }

        #endregion
        //--------------------------------------------------------------------
        
        if (projectileSpeed < 0)
        {
            projectileSpeed = 0;
            ChangeHealth(0);
        }

        transform.position += projectileDirection * (projectileSpeed * Time.deltaTime);
        transform.rotation = Quaternion.LookRotation(projectileDirection,Vector3.up);


    }


    private Vector3 CalculateHoming(Vector3 direction)
    {
        
        //Check a sphere around you for targets, and home towards the closest one 
        
        var foundColliders = Physics.OverlapSphere(transform.position,homingRange,homingMask);
        var closestDistance = homingRange;
        var homingDirection = direction;

        if (homingTarget == null)
        {
            Entity target = null;
            foreach (var collider in foundColliders)
            {
                if (Vector3.Angle(direction,collider.transform.position-transform.position) > 45f)
                {
                    continue;
                }

                var entity = collider.GetComponent<Entity>();
                if (entity != null)
                {
                    if (entity.Team != Team && entity != this && !entity.IsDead)
                    {
                        var distance = Vector3.Distance(transform.position, entity.transform.position);
                        if (distance < closestDistance)
                        {
                            closestDistance = distance;
                            target = entity;
                        }
                    }
                }
            } 
            homingTarget = target;
        }
        else if (homingTarget.IsDead)
        {
            homingTarget = null;
        }

        if (homingTarget != null)
        {
            homingDirection = Vector3.LerpUnclamped(direction,(homingTarget.transform.position.Flatten() - transform.position.Flatten()).normalized,WeaponStats.GetStatValue(StatType.HomingAmount));
        }

        homingTarget = null;

        return homingDirection.normalized;
    }

    private Vector3 CalculateBounce(Vector3 direction)
    {
        var reflectDir = direction;
        
        Ray ray = new Ray(transform.position, direction);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Time.deltaTime * projectileSpeed + .1f, bounceMask))
        {
            bounceAmount--;
            reflectDir = Vector3.Reflect(ray.direction, hit.normal);
        }

        return reflectDir;

    }

    

    //======================================================

    private void Update()
    {
        ProjectileUpdate();
    }


    private void LifeTimer()
    {
        projectileLifeTime -= Time.deltaTime;

        if (projectileLifeTime <= 0 && !IsDead)
        {
            ChangeHealth(0);
        }

    }

    public void InitializeProjectile(WeaponStats weaponStats, Vector3 position, Quaternion direction, int team, Character character)
    {
        this.team = team;
        WeaponStats = weaponStats;
        projectileSpeed = WeaponStats.GetStatValue(StatType.BulletSpeed);
        projectileOrigin = position;
        projectileDirection = direction * Vector3.forward;
        projectileLifeTime = WeaponStats.GetStatValue(StatType.Range) / WeaponStats.GetStatValue(StatType.BulletSpeed);
        transform.localScale = (Vector3.one * WeaponStats.GetStatValue(StatType.BulletSize)).Flatten(1);
        height = position.y;
        ProjectileHeight.localPosition = new Vector3(0,height,0);

        owner = character;

        bounceAmount = Mathf.RoundToInt(WeaponStats.GetStatValue(StatType.BounceAmount));
        piercingAmount = Mathf.RoundToInt(WeaponStats.GetStatValue(StatType.Piercing));
        knockbackAmount = Mathf.RoundToInt(WeaponStats.GetStatValue(StatType.Knockback));

        if(normalProjectile!= null && piercingProjectile != null && piercingAmount >= 1)
        {
            //ProjectileHeight = piercingProjectile.GetComponent<Transform>();
            normalProjectile.SetActive(false);
            piercingProjectile.SetActive(true);
            
        }
        
        
    }

    //=== Public Methods ===
    public void ProjectileUpdate()
    {
        Move();
        LifeTimer();
    }

    private void HitEntity(Entity otherEntity)
    {
        if (otherEntity is Character character)
        {
            character.Animator.SetSquash(new Vector3(1.4f,.8f,1.4f));
        }

        
        //Destroy(Instantiate(hitEffect, transform.position, Quaternion.identity),1f);
        
        /*if (bounceAmount <= 1)
        {
            
        }*/
        
        
        WeaponStats.OnHit?.Invoke(otherEntity);
        


        if(knockbackAmount > 0)
        {
            if(otherEntity.GetComponent<CharacterMovement>() != null)
            {
                otherEntity.GetComponent<CharacterMovement>().AddKnockback(projectileDirection * knockbackAmount);
            }
            
        }
        
        if(WeaponStats.GetStatValue(StatType.ExplodingBullet) <= 0)
        {
            if (piercingAmount <= 0/* && bounceAmount <= 1*/)
            {
                ChangeHealth(0);
                GameManager.instance.EffectManager.CreateEffect(hitEffect, transform.position, quaternion.identity, transform.localScale);
                otherEntity.ChangeHealth(-WeaponStats.GetStatValue(StatType.Damage), true);
                AudioManager.Instance.Play(AudioManager.Sounds.Bullet_Hit);
            }
            else
            {
                piercingAmount--;
                GameManager.instance.EffectManager.CreateEffect(hitEffect, transform.position, quaternion.identity, transform.localScale);
                otherEntity.ChangeHealth(-WeaponStats.GetStatValue(StatType.Damage), true);
                AudioManager.Instance.Play(AudioManager.Sounds.Bullet_Hit);
                //bounceAmount--;
            }
        }
        else
        {
            if (piercingAmount <= 0/* && bounceAmount <= 1*/)
            {
                ChangeHealth(0);
                explosionPrefab.projectileOwner = this;
                Instantiate(explosionPrefab, transform.position, transform.rotation);
                //AudioManager.Instance.Play(AudioManager.Sounds.Boombot_Explode);
                AudioManager.Instance.Play(AudioManager.Sounds.Bullet_Hit);
            }
            else
            {
                piercingAmount--;
                GameManager.instance.EffectManager.CreateEffect(hitEffect, transform.position, quaternion.identity, transform.localScale);
                otherEntity.ChangeHealth(-WeaponStats.GetStatValue(StatType.Damage), true);
                AudioManager.Instance.Play(AudioManager.Sounds.Bullet_Hit);
                //bounceAmount--;
            }
        }
        

        /*if (WeaponStats.GetStatValue(StatType.ExplodingBullet) <= 0*//* && piercingAmount >= 1*//*)
        {
            GameManager.instance.EffectManager.CreateEffect(hitEffect, transform.position, quaternion.identity, transform.localScale);
            otherEntity.ChangeHealth(-WeaponStats.GetStatValue(StatType.Damage), true);
            AudioManager.Instance.Play(AudioManager.Sounds.Bullet_Hit);
        }
        else
        {
            explosionPrefab.projectileOwner = this;
            Instantiate(explosionPrefab, transform.position, transform.rotation);
            AudioManager.Instance.Play(AudioManager.Sounds.Boombot_Explode);
        }*/



        

        if (owner != null && WeaponStats.GetStatValue(StatType.LifeSteal) != 0 && otherEntity is Character)
        {
            owner.ChangeHealth(WeaponStats.GetStatValue(StatType.LifeSteal), true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if(IsDead)return;
        var hitEntity = other.GetComponent<Entity>();
        if (hitEntity != null)
        {
            if (team != hitEntity.Team) //Subjected to change
            {
                HitEntity(hitEntity);
            }
        }
    }
    
}
