using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Scriptable Objects/Projectile")]
public class ProjectileScriptable : ScriptableObject
{
    public GameObject projectilePrefab;
}
