using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : MonoBehaviour
{
    private List<Projectile> projectileList;

    //=== Private Methods ===
        
    public void CreateProjectile(Transform origin, Vector3 direction, Quaternion rotation, float speed, float lifeTime)
    {
        //Projectile projectile = new Projectile(spawnPos, lifeTime);
        //projectileList.Add(projectile);
    }
    
    //=== Public Methods ===

    private void FixedUpdate()
    {
        foreach (var proj in projectileList)
        {
            proj.ProjectileUpdate();
        }
    }
}
