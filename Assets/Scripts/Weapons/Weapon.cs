using System;
using System.Net.Mail;
using Clonk.Stats;
using Clonk.Stats.Utilities;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Clonk.Weapons
{
    public class Weapon : MonoBehaviour
    {
        
        public WeaponStats WeaponStats;
        public Transform[] ShootFromHere;
        public bool alternateShot = false;
        private Action<Vector3> OnHit;
        private int alternateIndex = 0;
        public Character owner;

        public void Init(WeaponStats weaponStats)
        {
            WeaponStats = weaponStats;
            
            var stats = new Stat[WeaponStats.statPreset.Stats.Length];
            
            for(int i = 0; i <= stats.Length-1;i++)
            {
                stats[i] = new Stat(WeaponStats.statPreset.Stats[i].Type, WeaponStats.statPreset.Stats[i].Value);
            }

            WeaponStats.Stats = stats;

        }
        public void Fire(Vector3 pos,Quaternion aimAngle,int team)
        {
            if (ShootFromHere != null)
            {
                AudioManager.Instance.Play(WeaponStats.ShootEffect);
                
                for (int e = 0; e < ShootFromHere.Length; e++)
                {
                    if (alternateShot && e != alternateIndex)
                    {
                        continue;
                    }

                    pos = ShootFromHere[e].position;
                    aimAngle = ShootFromHere[e].rotation;
                    

                    for (int i = 0; i < WeaponStats.Stats.getStatOfType(StatType.BulletAmount).Value;i++)
                    {
                        var angle = aimAngle;
                        //var projectile = Instantiate(WeaponStats.BaseProjectile, pos.Flatten(), aimAngle).GetComponent<Entity>();

                        var projectile = ObjectPooler.SharedInstance.GetPooledObject(WeaponStats.BaseProjectile, pos.Flatten(), aimAngle).GetComponent<Entity>();

                        if (projectile != null)
                        {
                            if (projectile is Projectile regularProjectile)
                            {
                                if (WeaponStats.ShootEffectPrefab != null && ShootFromHere != null)
                                {
                                    //Destroy(Instantiate(WeaponStats.ShootEffectPrefab, pos, aimAngle), .5f);
                                    GameObject shootEffect = ObjectPooler.SharedInstance.GetPooledObject(WeaponStats.ShootEffectPrefab, pos, aimAngle);

                                    ObjectPooler.SharedInstance.Deactivate(shootEffect, .5f);
                                }
                                var spread = WeaponStats.Stats.getStatOfType(StatType.BulletSpread).Value;
                                angle.eulerAngles = new Vector3(0, angle.eulerAngles.y + Random.Range(-spread, spread), 0);
                                regularProjectile.InitializeProjectile(WeaponStats, pos, angle, team, owner);
                            } else if (projectile is SpawnProjectile spawnProjectile)
                            {
                                
                                spawnProjectile.InitializeProjectile(WeaponStats.GetStatValue(StatType.BulletSpeed), pos, angle, team);
                            }
                        }
                        
                    }
                }

                alternateIndex = (alternateIndex + 1) % ShootFromHere.Length;

            }
        }
    }
}
