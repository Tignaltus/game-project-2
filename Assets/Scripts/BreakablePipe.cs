using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakablePipe : Entity
{
    public GameObject smoke, weakerSmoke, model, brokenModel;
    private PipeSmoke pipeSmoke;
    public bool broken;

    private void Awake()
    {
        pipeSmoke = smoke.GetComponent<PipeSmoke>();
    }
    private void HitEntity(Entity otherEntity)
    {
        /*if (otherEntity is Projectile)
        {
            Debug.Log("proj");
        }*/

        if(healthCurrent == 0)
        {
            BreakPipe();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        var hitEntity = other.GetComponent<Entity>();
        HitEntity(hitEntity);
    }

    [ContextMenu("BreakPipe")]
    public void BreakPipe()
    {
        if (!broken)
        {
            StartCoroutine(ActivateSmoke());
        }
        
    }
    public IEnumerator ActivateSmoke()
    {
        yield return StartCoroutine(One());
        yield return StartCoroutine(Two());
        yield return StartCoroutine(Three());
    }

    IEnumerator One()
    {
        broken = true;
        model.SetActive(false);
        brokenModel.SetActive(true);
        smoke.SetActive(true);
        yield return new WaitForSeconds(2f);
    }
    IEnumerator Two()
    {
        //pipeSmoke.force = pipeSmoke.force / 3;
        smoke.SetActive(false);
        weakerSmoke.SetActive(true);
        yield return new WaitForSeconds(5f);
    }
    IEnumerator Three()
    {
        //pipeSmoke.force = pipeSmoke.force * 3;
        weakerSmoke.SetActive(false);
        yield return null;
    }
}
