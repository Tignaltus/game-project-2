using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioTrigger : MonoBehaviour
{
    public void PlayAudio(AudioManager.Sounds sound)
    {
        AudioManager.Instance.Play(sound);
    }
}
