using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RobotDeath : MonoBehaviour
{
    public float force = 500f;

    public List<Rigidbody> pieces;

    public List<Vector3> positions = new List<Vector3>();
    public List<Quaternion> rotations = new List<Quaternion>();

    // Start is called before the first frame update
    void Start()
    {
        var rbs = GetComponentsInChildren<Rigidbody>();

        foreach (var rigidbody in rbs)
        {
            rigidbody.AddExplosionForce(force, transform.position + Vector3.up, 3f);
        }


        Destroy(gameObject, 5f);

    }
}
