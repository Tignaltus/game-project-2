using System;
using System.Collections.Generic;
using Clonk.Animators;
using Clonk.CharacterInput;
using Clonk.Stats;
using Clonk.Stats.Utilities;
using Clonk.Weapons;
using UnityEngine;
using UnityEngine.Rendering;

namespace Clonk
{
    [RequireComponent(typeof(CharacterMovement))]
    [RequireComponent(typeof(Weapon))]
    public class Character : Entity, ISubscribeCharacterInput
    {
        public WeaponScriptable MyWeaponScriptable;
        public Weapon myWeapon;
        public CharacterAnimator Animator;
        public AudioManager.Sounds DeathSound = AudioManager.Sounds.Enemy_Death_Explosion;
        
        protected Vector3 movement = Vector3.zero;
        protected Quaternion aimAngle = Quaternion.identity;
        protected Quaternion aimAngleTo = Quaternion.identity;
        
        public float rotationSpeed = 7f;
        
        private float modifiedRotationSpeed;

        private List<PowerUpData> upgrades = new List<PowerUpData>();

        private bool isFiring = false;
        private float cooldown = 0;

        public Vector3 CurrentMoveVector { get; private set;}
        public Action<WeaponStats> OnAddedUpgrade;

        public StatPreset statPreset; 
        private Stat[] stats;

        public CharacterMovement characterMovement;
        public GameObject DeathPrefab;

        public Vector3 Movement => movement;
        public Quaternion AimAngle => aimAngle;

        public bool imobilized = false;

        public Action<Character> onInitialized;

        public float speed;
        private void Start()
        {
            modifiedRotationSpeed = rotationSpeed;
            modifiedHealthMax = healthMaxBase;
            InitializeStats();
            myWeapon = GetComponent<Weapon>();
            characterMovement = GetComponent<CharacterMovement>();
            myWeapon?.Init(MyWeaponScriptable.WeaponStats);
            OnDeath += Die;
            onInitialized?.Invoke(this);
        }

        private void InitializeStats()
        {
            stats = new Stat[statPreset.Stats.Length];
            
            for(int i = 0; i <= stats.Length-1;i++)
            {
                stats[i] = new Stat(statPreset.Stats[i].Type, statPreset.Stats[i].Value);
            }
         
            
        }


        public float GetStatValue(StatType type)
        {
            float value = 0;
            Stat stat = stats.getStatOfType(type);
            if (stat != null)
            {
                value = stat.Value;
            }
            return value;
        }


        private void FixedUpdate()
        {
            
        }

        private void Update()
        {
            if (!GameManager.instance.isGamePaused)
            {
                CurrentMoveVector = movement * stats.getStatOfType(StatType.MovementSpeed).Value;

                characterMovement.Move(CurrentMoveVector * Time.deltaTime);

                //rb.MovePosition(rb.position + CurrentMoveVector * Time.fixedDeltaTime);
                aimAngle = Quaternion.Slerp(aimAngle, aimAngleTo, modifiedRotationSpeed * Time.deltaTime);
                Animator?.UpdateAnimator(movement, aimAngle);

                speed = stats.getStatOfType(StatType.MovementSpeed).Value;
            }




            if (!GameManager.instance.isGamePaused)
            {
                if (isFiring && Time.time > cooldown && myWeapon != null)
                {
                    cooldown = Time.time + (60 / myWeapon.WeaponStats.Stats.getStatOfType(StatType.FireRate).Value) / 60;
                    Animator.SetTrigger("Fire");
                    myWeapon.Fire(transform.position, aimAngle, team);
                }
            }
        }

        void Die()
        {
            Instantiate(DeathPrefab, transform.position, transform.rotation);
            //ObjectPooler.SharedInstance.GetPooledObject(DeathPrefab, transform.position, transform.rotation);
            if (team != GameManager.instance.GetPlayer().Character.team)
            {
                GameManager.instance.ScoreManager.AddScore(10);
            }
        }
        
        public void AddHealing(PowerUpData healing)
        {
            //healthCurrent += healing.modifiers[0].Value;
            ChangeHealth(healing.modifiers[0].Value, true);
        }
        public void AddUpgrade(PowerUpData powerUpData)
        {
            upgrades.Add(powerUpData);


            if (myWeapon != null)
            {
                myWeapon.WeaponStats.FlagManager.AddModifier(powerUpData.GetFlags());
            }

            var modifiers = powerUpData.GetStatModifiers();
            for(int i = 0; i <= modifiers.Length-1;i++)
            {
                bool modified = false;
                if (stats != null)
                {
                    foreach (var stat in stats)
                    {
                        if (stat.StatType == powerUpData.GetStatType(i))
                        {
                            stat.AddModifier(modifiers[i]);
                            modified = true;
                        }
                    }
                }
                


                if (myWeapon != null)
                {
                    foreach (var stat in myWeapon.WeaponStats.Stats)
                    {
                                        
                        if (stat.StatType == powerUpData.GetStatType(i))
                        {
                            stat.AddModifier(modifiers[i]);
                            modified = true;
                        }
                    }

                }
                if (!modified)
                {
                    Debug.Log($"Did not find {powerUpData.GetStatType(i)} in either Character stats nor the weapon stats");
                }

            }
        }

        public void RemoveUpgrade(PowerUpData powerUpData)
        {
            
            upgrades.Remove(powerUpData);
            foreach (var stat in stats)
            {
                stat.RemoveAllModifiersFromSource(powerUpData);
            }


            if (myWeapon != null)
            {
                myWeapon.WeaponStats.FlagManager.RemoveAllModifiersFromSource(powerUpData);
                foreach (var stat in myWeapon.WeaponStats.Stats)
                {
                    stat.RemoveAllModifiersFromSource(powerUpData);
                }
            }

        }

        public List<PowerUpData> GetUpgrades()
        {
            return upgrades;
        }

        #region Subscriptions
        protected void OnMove(Vector3 direction)
        {
            movement = direction;
        }
        protected void OnAim(Quaternion AimAngle)
        {
            aimAngleTo = AimAngle;
        }
        protected void OnFire(bool fire)
        {
            isFiring = fire;
        }
        protected void OnPause(bool pause)
        {
            if (pause && !GameManager.instance.isGamePaused)
            {
                GameManager.instance.GameplayUIManager.TriggerPause();
            }
            
            /*if (pause)
            {
                if (!GameManager.instance.isGamePaused)
                {
                    GameManager.instance.PauseGame();
                } else
                {
                    GameManager.instance.UnPauseGame();
                }
                
            }*/


        }
        public void Subscribe(PublisherCharacterInput publisher)
        {
            publisher.Move += OnMove;
            publisher.Aim += OnAim;
            publisher.Fire += OnFire;
            publisher.Pause += OnPause;
        }
        public void Unsubscribe(PublisherCharacterInput publisher)
        {
            publisher.Move -= OnMove;
            publisher.Aim -= OnAim;
            publisher.Fire -= OnFire;
            publisher.Pause -= OnPause;
        }
        #endregion
        
        
        
        
        
        
    }
}