using Clonk.Weapons;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TonkSpawnEnemy : Weapon
{
    public float spawnTime = 20;
    public float timeUntilSpawn;
    private bool spawnDone = true;


    public CharacterScriptableObject spawnableEnemies;
    public Transform spawnPosition;

    void Start()
    {
        //Spawn();
    }

    // Update is called once per frame
    void Update()
    {
        timeUntilSpawn = spawnDone ? spawnTime : timeUntilSpawn - Time.fixedDeltaTime;
        spawnDone = (timeUntilSpawn <= 0);
        if (spawnDone)
        {
            Spawn();
        }
    }

    public void Spawn()
    {
        //GameManager.instance.AIManager.CreateEnemy(spawnableEnemies, spawnPosition.position, Quaternion.identity);
        Init(WeaponStats);
        Fire(ShootFromHere[0].position, ShootFromHere[0].rotation, 2);
        
    }
}
