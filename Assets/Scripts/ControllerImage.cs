using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerImage : MonoBehaviour
{

    public GameObject keyboardImage;
    public GameObject gamepadImage;
    public GameObject mobileImage;
    public Options options;


    private void Awake()
    {
        //options = GameObject.Find("Options");
    }
    void Start()
    {
        options = GameObject.Find("Options").GetComponent<Options>();
    }

    // Update is called once per frame
    void Update()
    {
        if(options != null)
        {
            if(options.playerCharacterSO.InputPrefab == options.keyboardControls)
            {
                keyboardImage.SetActive(true);
                gamepadImage.SetActive(false);
                mobileImage.SetActive(false);
                options.rotationSpeed = 70;
            } 
            else if(options.playerCharacterSO.InputPrefab == options.gamepadControls)
            {
                keyboardImage.SetActive(false);
                gamepadImage.SetActive(true);
                mobileImage.SetActive(false);
                options.rotationSpeed = 7;
            } 
            else if (options.playerCharacterSO.InputPrefab == options.mobileControls)
            {
                keyboardImage.SetActive(false);
                gamepadImage.SetActive(false);
                mobileImage.SetActive(true);
                options.rotationSpeed = 12;
            } 
            else
            {
                Debug.Log("no");
            }
        }
    }
}
