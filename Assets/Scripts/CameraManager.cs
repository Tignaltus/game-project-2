using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public Camera Camera;
    public Transform Target;
    public Transform CameraOffset;

    private Vector3 velocity;

    public Vector3 positionTo = Vector3.zero;

    public float minZoom = 30f;
    public float maxZoom = 12f;

    public float minFov = 40f;
    public float maxFov = 10f;

    public float zoomLimiter = 50f;

    private float zoomTo = 12f;
    private float zoomVelocity;



    // Update is called once per frame
    private void FixedUpdate()
    {

        if (Target != null)
        {
            positionTo = getCenterPoint();
            if (Camera.orthographic)
            {
                zoomTo = Mathf.Lerp(maxZoom, minZoom, getGreatestDistance() / zoomLimiter);
            }
            else
            {
                zoomTo = Mathf.Lerp(maxFov, minFov, getGreatestDistance() / zoomLimiter);

            }
        }


        

    }

    private void LateUpdate()
    {

        CameraOffset.position = Vector3.SmoothDamp(CameraOffset.position, positionTo, ref velocity, .5f);
        if (Camera.orthographic)
        {
            Camera.orthographicSize = Mathf.SmoothDamp(Camera.orthographicSize, zoomTo, ref zoomVelocity, .5f);
        }
        else
        {
            Camera.fieldOfView = Mathf.SmoothDamp(Camera.fieldOfView, zoomTo, ref zoomVelocity, .5f);
        }
    }


    private float getGreatestDistance()
    {

        if (GameManager.instance.AIManager.enemyList.Count < 1)
        {
            return maxZoom;
        }

        Bounds bounds = new Bounds(Target.position, Vector3.zero);
        foreach (AIInputManager enemy in GameManager.instance.AIManager.enemyList)
        {
            if (enemy != null)
            {
                Vector3 pos = enemy.GetCharacter().transform.position;
                if (Vector3.Distance(Target.position, pos) < minZoom * .6f)
                {
                    bounds.Encapsulate(pos);
                }
            }
        }

        return bounds.size.x;
    }

    private Vector3 getCenterPoint()
    {
        if (GameManager.instance.AIManager.enemyList.Count < 1)
        {
            return Target.position;
        }

        Bounds bounds = new Bounds(Target.position, Vector3.zero);
        foreach (AIInputManager enemy in GameManager.instance.AIManager.enemyList)
        {
            if (enemy != null)
            {
                Vector3 pos = enemy.GetCharacter().transform.position;
                if (Vector3.Distance(Target.position, pos) < minZoom * .8f)
                {
                    bounds.Encapsulate(pos);
                }
            }
        }

        return bounds.center;

    }






    public void SetTarget(Transform target)
    {
        Target = target;
    }
}
