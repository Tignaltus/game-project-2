using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CharacterMovement : PhysicsBase
{
    private Vector3 knockBack = Vector3.zero;
    private readonly float friction = 15f;
    private readonly float maxKnockback = 100f;
    public bool knockbackImunity;
    public void Move(Vector3 velocity)
    {
        UpdateRaycastOrigins();
        collisions.Reset();

        velocity += knockBack * Time.deltaTime;
        knockBack = Vector3.MoveTowards(knockBack,Vector3.zero, friction*Time.deltaTime);
        
        if (velocity.x != 0)
        {
            HorizontalCollisions(ref velocity);
        }
        if (velocity.z != 0)
        {
            VerticalCollisions(ref velocity);
        }
        if (Mathf.Abs(velocity.x) < 0.005f){
            velocity.x = 0;
        }
        if (Mathf.Abs(velocity.z) < 0.005f){
            velocity.z = 0;
        }

        transform.position += velocity;
    }

    public void AddKnockback(Vector3 knockBack)
    {
        if (!knockbackImunity)
        {
            this.knockBack += knockBack;
            if (this.knockBack.magnitude > maxKnockback)
            {
                this.knockBack = this.knockBack.SetMagnitude(maxKnockback);
            }
        }
    }


}