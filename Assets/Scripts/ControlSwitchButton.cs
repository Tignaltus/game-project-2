using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlSwitchButton : MonoBehaviour
{
    public Options.controlType controlType;

    public void UpdateControlType()
    {
        Options.instance.SwitchInput(controlType);
    }
}
