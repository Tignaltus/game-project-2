using System;
using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthbar : MonoBehaviour
{
    public Color damageColor;
    public Color healColor;

    public Slider slider;
    public Slider delayedSlider;

    public Image delayedHealth;

    public float maxValue;
    public float value = 0;
    public float speed = 1;
    private float damageTimer = 0;
    public float damageDelayTime = 1;
    private float healingTimer = 0;
    public float healingDelayTime = 1;

    void Awake()
    {
        slider = GetComponent<Slider>();
    }

    private void Update()
    {
        if (damageTimer < Time.time)
        {
            delayedSlider.value = Mathf.Lerp(delayedSlider.value, value, speed * Time.deltaTime);
        }

        if (healingTimer < Time.time)
        {
            slider.value = Mathf.Lerp(slider.value, value, speed * Time.deltaTime);
        }

        /*if (timer < Time.time)
        {
            delayedSlider.value = Mathf.Lerp(delayedSlider.value, value, speed * Time.deltaTime);
        }*/

    }

    public void UpdateHealthbarInstant(float current, float max)
    {
        value = current;
        maxValue = max;

        slider.maxValue = max;
        slider.value = current;
        delayedSlider.maxValue = max;
        delayedSlider.value = max;
    }

    public void UpdateHealthbar(float current,float max)
    {

        if (current < value)
        {
            delayedHealth.color = damageColor;

            slider.maxValue = max;
            slider.value = current;


            delayedSlider.maxValue = max;
            if (delayedSlider.value == 0)
            {
                delayedSlider.value = max;
            }

            damageTimer = Time.time + damageDelayTime;
        }
        else
        {
            delayedHealth.color = healColor;

            delayedSlider.maxValue = max;
            delayedSlider.value = current;

            slider.maxValue = max;
            if (slider.value == 0)
            {
                slider.value = max;
            }

            healingTimer = Time.time + healingDelayTime;
        }
        value = current;
        maxValue = max;


        /*value = current;
        maxValue = max;

        slider.maxValue = max;
        slider.value = current;
        delayedSlider.maxValue = max;
        if (delayedSlider.value == 0)
        {
            delayedSlider.value = max;
        }

        timer = Time.time + delayTime;*/

    }
}
