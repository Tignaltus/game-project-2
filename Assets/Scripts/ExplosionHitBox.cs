using Clonk;
using System.Collections;
using UnityEngine;

public class ExplosionHitBox : Entity
{
    public float damage = 8f;
    public Collider collider;
    public int delayFrames;
    public Projectile projectileOwner;

    private void Start()
    {

        //GameManager.instance.EffectManager.CreateEffect(EffectType.Explosion, transform.position, transform.rotation, transform.localScale);
        //collider = GetComponent<Collider>();
        //StartCoroutine(ExpolodeCoroutine());
        
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        if (projectileOwner != null)
        {
            damage = projectileOwner.WeaponStats.GetStatValue(StatType.Damage);
            transform.localScale = transform.localScale * projectileOwner.WeaponStats.GetStatValue(StatType.ExplodingBullet);
            /*transform.localScale = new Vector3(
            transform.localScale.x + projectileOwner.WeaponStats.GetStatValue(StatType.ExplodingBullet),
            transform.localScale.y + projectileOwner.WeaponStats.GetStatValue(StatType.ExplodingBullet),
            transform.localScale.z + projectileOwner.WeaponStats.GetStatValue(StatType.ExplodingBullet));*/

        }
        GameManager.instance.EffectManager.CreateEffect(EffectType.Explosion, transform.position, transform.rotation, transform.localScale);
        collider = GetComponent<Collider>();

        collider.enabled = false;
        StartCoroutine(ExpolodeCoroutine());
    }

    private void OnTriggerEnter(Collider other)
    {
        Entity hitEntity = other.GetComponent<Entity>();

        if (hitEntity != null)
        {
            HitEntity(hitEntity);
        }
    }

    private void HitEntity(Entity otherEntity)
    {
        otherEntity.ChangeHealth(-damage, true);
    }

    public IEnumerator ExpolodeCoroutine()
    {
        for (int i = 0; i <= delayFrames; i++)
        {
            yield return null;
        }
        collider.enabled = true;
        yield return null;
        //Destroy(gameObject, 0.2f);

        ObjectPooler.SharedInstance.Deactivate(gameObject, 0.2f);
    }
}
