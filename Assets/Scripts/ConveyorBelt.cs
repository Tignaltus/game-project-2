using Clonk;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class ConveyorBelt : MonoBehaviour
{
    /*public float xDir;
    public float zDir;*/
    public List<MeshRenderer> meshRenderers;
    public float force = 0.1f;
    private void HitEntity(Entity otherEntity)
    {
        Vector3 dir = transform.right * force;
        var tmpRb = otherEntity.GetComponent<Rigidbody>();
        //otherEntity.GetComponent<CharacterMovement>().AddKnockback(Vector3.back.SetMagnitude(0.5f).Flatten(0));
        //otherEntity.GetComponent<CharacterMovement>().Move(new Vector3(xDir, 0, zDir));
        otherEntity.GetComponent<CharacterMovement>().Move(dir);
    }

    private void OnTriggerStay(Collider other)
    {
        var hitEntity = other.GetComponent<Entity>();
        HitEntity(hitEntity);
    }

    private void FixedUpdate()
    {
        //var scroll += force * .4f * Time.fixedDeltaTime;

        foreach (MeshRenderer meshRenderer in meshRenderers)
        {

            meshRenderer.material.mainTextureOffset += Vector2.right * force * 2 * Time.fixedDeltaTime;
        }
    }
}
