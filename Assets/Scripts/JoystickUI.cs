using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JoystickUI : MonoBehaviour
{
    public Vector2 direction = Vector2.zero;


    public void UpdateJoystick(Vector2 point, Vector2 direction)
    {
        GetComponent<Image>().enabled = true;
        transform.GetChild(0).GetComponent<Image>().enabled = true;
        var rect = (RectTransform)transform;
        rect.localPosition = point;
        transform.GetChild(0).transform.localPosition = direction * (((RectTransform)transform).sizeDelta/2);
    }
}
