using System.Collections;
using System.Collections.Generic;
using Clonk.Animators;
using UnityEngine;

public class TornAnimator : CharacterAnimator
{
    [System.Serializable]
    public class Rotators //here you drag in the parts of the tower that are supposed to spin and the speed it will rotate in
    {
        [SerializeField] public Transform rotator;
        [SerializeField] public float rotateSpeed;
    }

    public Rotators[] rotators; //this will make it public so you can actually drag and change value

    protected void Update()
    {
        base.Update();
        

        if(rotators != null) //if rotators exist then make them spin
        {
            for (int i = 0; i < rotators.Length; i++)
            {
                var rotation = aimDirection;

                rotation.eulerAngles = new Vector3(aimDirection.eulerAngles.x - offsetAngle, aimDirection.eulerAngles.y * rotators[i].rotateSpeed, aimDirection.eulerAngles.z);

                rotators[i].rotator.rotation = rotation;
            }
        }
        
    }
}
