using Unity.Mathematics;
using UnityEngine;

namespace Clonk.Animators
{
    public class CharacterAnimator : MonoBehaviour
    {
        protected Vector3 direction;
        protected Quaternion aimDirection;
        [SerializeField][Range(0,360)]
        protected float offsetAngle = 0;
    
        
        [SerializeField]protected Transform Scaler;
        [SerializeField]protected float squashSpeed;
        [SerializeField]protected Animator animator;
        
        
        protected Vector3 Squash = new Vector3(1, 1, 1);
        
        protected void Awake()
        {
            aimDirection = quaternion.LookRotation(Vector3.forward,Vector3.up);
            direction = Vector3.forward;
        }
    
        protected void Update()
        {
            UpdateSquash(squashSpeed);
        }


        private void UpdateSquash(float speed)
        {
            Squash = Vector3.Lerp(Squash, Vector3.one, Time.deltaTime * speed);
            if(Scaler != null)
            {
                Scaler.localScale = Squash;
            }
            
        }
        

        public void UpdateAnimator(Vector3 direction, Quaternion aimDir)
        {
            this.direction = direction;
            aimDirection = aimDir;
        }

        public void SetTrigger(string trigger)
        {
            if(animator != null)
            {
                animator?.SetTrigger(trigger);
            }
            
        }
        public void SetBool(string Bool,bool value)
        {
            if (animator != null)
            {
                animator?.SetBool(Bool, value);
            }
            
        }


        public void SetSquash(Vector3 setSquash)
        {
            Squash = setSquash;
        }
        public void DoSquash()
        {
            Squash = new Vector3(1.4f,.7f,1.4f);
        }
    
    
    
    }
}