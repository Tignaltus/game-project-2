using System;
using System.Collections;
using System.Collections.Generic;
using Clonk;
using DitzelGames.FastIK;
using Unity.Mathematics;
using UnityEngine;

public class LegPair : MonoBehaviour
{
    //public GameObject LegPrefab;
    public GameObject TargetPrefab;
    public float distanceuntilMove;
    public float ofshoot = 1.3f;

    public LayerMask layerMask;

    public Leg[] legs;
    
    void Start()
    {
        
        /*legs[0].Target = Instantiate(TargetPrefab,  legs[0].Foot.transform.position, Quaternion.identity).transform;
        legs[1].Target = Instantiate(TargetPrefab,  legs[1].Foot.transform.position, Quaternion.identity).transform;
        
        legs[0].Pole = Instantiate(TargetPrefab, legs[0].transform.position + Vector3.up * 3, Quaternion.identity,transform).transform;
        legs[1].Pole = Instantiate(TargetPrefab, legs[1].transform.position + Vector3.up * 3, Quaternion.identity,transform).transform;*/

        legs[0].Target = ObjectPooler.SharedInstance.GetPooledObject(TargetPrefab, legs[0].Foot.transform.position, Quaternion.identity).transform;
        legs[1].Target = ObjectPooler.SharedInstance.GetPooledObject(TargetPrefab, legs[1].Foot.transform.position, Quaternion.identity).transform;

        legs[0].Pole = ObjectPooler.SharedInstance.GetPooledObject(TargetPrefab, legs[0].transform.position + Vector3.up * 3, Quaternion.identity).transform;
        legs[1].Pole = ObjectPooler.SharedInstance.GetPooledObject(TargetPrefab, legs[1].transform.position + Vector3.up * 3, Quaternion.identity).transform;

        legs[0].Init();
        legs[1].Init();

    }


    private void Update()
    {
       /* if (shouldMoveFoot(0,Vector3.zero))
        {
            MoveFoot(0,Vector3.zero);
        }
        if (shouldMoveFoot(1,Vector3.zero))
        {
            MoveFoot(1,Vector3.zero);
        }
        */

    }
    
    public bool shouldMoveFoot(int legIndex , Vector3 addPosition)
    {
        var newFootPosition = getNewFootPosition(legIndex,addPosition*ofshoot);

        var relativeNewPosition = legs[legIndex].Foot.Target.transform.position.Flatten() - newFootPosition.Flatten();
        if (relativeNewPosition != Vector3.zero)
        {
            if (Vector3.Magnitude(relativeNewPosition) > distanceuntilMove && !legs[legIndex].isMoving)
            {
                return true;
            }
        }
        return false;
    }

    public void MoveFoot(int legIndex, Vector3 addPosition)
    {
        var newFootPosition = getNewFootPosition(legIndex,addPosition*ofshoot).Flatten();
        
        Debug.DrawRay(new Vector3(newFootPosition.x, 2, newFootPosition.z), Vector3.down, Color.red,1);

        legs[legIndex].Move(newFootPosition);
    }

    
    private Vector3 getNewFootPosition(int legIndex)
    {
        RaycastHit hit;
        Vector3 newPos = legs[legIndex].transform.position + legs[legIndex].InitialPosition;
        var flattenedPos = new Vector2(newPos.x, newPos.z);
        Vector2 offshootFootPos =
            Vector2.LerpUnclamped(new Vector2(legs[legIndex].Target.position.x, legs[legIndex].Target.position.z),
                flattenedPos, 1);
        Physics.Raycast(new Vector3(offshootFootPos.x,transform.position.y,offshootFootPos.y), Vector3.down, out hit,layerMask);
        return hit.point;
    }
    private Vector3 getNewFootPosition(int legIndex,Vector3 addPosition)
    {
        RaycastHit hit;
        var rotatedOffset = Quaternion.Euler(0,transform.rotation.eulerAngles.y,0) * legs[legIndex].InitialPosition;
        
        
        
        Vector3 newPos = legs[legIndex].transform.position + rotatedOffset;
        
        
        addPosition = addPosition.Flatten();
        newPos = newPos + addPosition;
        Physics.Raycast(new Vector3(newPos.x,2,newPos.z), Vector3.down, out hit,layerMask);
        return hit.point;
    }
    
}
