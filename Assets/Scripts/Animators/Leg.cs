using System;
using System.Collections;
using System.Collections.Generic;
using Clonk;
using DitzelGames.FastIK;
using UnityEngine;
using UnityEngine.EventSystems;

public class Leg : MonoBehaviour
{
    public FastIKFabric Foot;
    public Transform Target;
    public Transform Pole;

    public float speed = 5;
    
    public bool isMoving = false;

    private Vector3 newPosition;
    private Vector3 startPosition;
    private Vector3 initialPosition;

    public Vector3 InitialPosition => initialPosition;

    public float t;

    public void Init()
    {
        initialPosition = (Foot.transform.position - transform.position).Flatten();
        Foot.Target = Target;
        Foot.Pole = Pole;
    }

    public void Move(Vector3 newPosition)
    {
        startPosition = Target.position;
        this.newPosition = newPosition;
        isMoving = true;
        t = 0;
        //Debug.Log("MOVING to " + newPosition.ToString());
        
        //Target.position = newPosition;
    }

    public void Update()
    {
        if (isMoving)
        {
            if (t<1)
            {
                if (t < .5)
                {
                    
                    Target.position = Vector3.Lerp(startPosition, newPosition+Vector3.up*1, t);
                }
                else
                {
                    Target.position = Vector3.Lerp(startPosition+Vector3.up*1, newPosition, t);
                    
                }
                
                t += Time.deltaTime * speed;
            }
            else
            {
                Target.position = newPosition;
                isMoving = false;
            }
        }
    }


}