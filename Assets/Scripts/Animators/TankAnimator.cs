using System;
using System.Collections;
using System.Collections.Generic;
using Clonk;
using Clonk.Animators;
using Unity.Mathematics;
using UnityEngine;

public class TankAnimator : CharacterAnimator
{
    [SerializeField] private Transform Top;
    [SerializeField] private Transform Bottom;
    private Quaternion bottomDir = quaternion.identity;
    [Range(0,1)]
    public float rotationSpeed;
    public float moveRotation = 50f;
    
    private void Awake()
    {
        base.Awake();
        aimDirection = quaternion.LookRotation(Vector3.forward,Vector3.up);
        direction = Vector3.forward;
        Top.rotation = aimDirection;
        bottomDir = Quaternion.LookRotation(direction, Vector3.up);
        Bottom.rotation = bottomDir;
    }

    void Update()
    {
        base.Update();
        if (direction != Vector3.zero) 
            bottomDir = Quaternion.LookRotation(direction, Vector3.up);
        var bottomRotation = bottomDir;
        bottomRotation.eulerAngles = new Vector3(bottomDir.eulerAngles.x,bottomDir.eulerAngles.y+offsetAngle,bottomDir.eulerAngles.z+direction.magnitude * moveRotation);
        Bottom.rotation = Quaternion.Slerp(Bottom.rotation,bottomRotation,rotationSpeed);
        var topRotation = aimDirection;
        topRotation.eulerAngles = new Vector3(aimDirection.eulerAngles.x,aimDirection.eulerAngles.y+offsetAngle,aimDirection.eulerAngles.z);
        Top.rotation = topRotation;

    }




}
