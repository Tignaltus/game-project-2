using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;
using UnityEngine.UIElements;

public class LegAnimator : MonoBehaviour
{
    public LegPair[] LegPairs;
    public Character Character;
    public float directionLength = 1;

    private int prevmove = -1;
    
    void FixedUpdate()
    {
        MoveLegs(0);
        MoveLegs(1);
    }

    private void MoveLegs(int even)
    {
        bool shouldMove = false;
        bool legsAreMoving = false;
        foreach (var legPair in LegPairs)
        {
            if (legPair.legs[0].isMoving || legPair.legs[1].isMoving)
            {
                legsAreMoving = true;
            }
        }
        if (!legsAreMoving)
        {
            if (prevmove != even)
            {
                prevmove = even;
                for (int i = 0; i < LegPairs.Length; i++)
                {
                    if (LegPairs[i].shouldMoveFoot((i+even) % 2,Character.CurrentMoveVector.Flatten().normalized * directionLength))
                    {
                        shouldMove = true;
                    }
                }
                if (shouldMove)
                {
                    for (int i = 0; i < LegPairs.Length; i++)
                    {
                        LegPairs[i].MoveFoot((i+even) % 2,Character.CurrentMoveVector.Flatten().normalized * directionLength);
                    }
                }
            }
            else
            {
                prevmove = -1;
            }
        }
    }


}
