using System.Collections;
using System.Collections.Generic;
using Clonk.Animators;
using UnityEngine;

public class JuggernautAnimator : CharacterAnimator
{
    [SerializeField]private Transform Rotator;
    
    
    protected void Update()
    {
        base.Update();
        var rotation = aimDirection;
        rotation.eulerAngles = new Vector3(aimDirection.eulerAngles.x,aimDirection.eulerAngles.y+offsetAngle,aimDirection.eulerAngles.z);
        Rotator.rotation = rotation;
    }

    
}
