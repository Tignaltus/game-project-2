using System;
using System.Collections.Generic;
using UnityEngine;

namespace Clonk
{
    public class PhysicsBase : MonoBehaviour
    {
        
        private RaycastOrigins raycastOrigins = new RaycastOrigins() {
            BottomLeft = new Vector2(-0.5f, -0.5f).ConvertVector2ToFlatVector3(),
            TopLeft = new Vector2(-0.5f, 0.5f).ConvertVector2ToFlatVector3(),
            BottomRight = new Vector2(0.5f, -0.5f).ConvertVector2ToFlatVector3(),
            TopRight = new Vector2(0.5f, 0.5f).ConvertVector2ToFlatVector3(),
        };
        
        
        protected const float SKIN_WIDTH = 0.1f;
        private Collider collider;
        public LayerMask collisionLayer;
        public CollisionInfo collisions;
        
        
        private void Awake()
        {
            collider = GetComponent<Collider>();

        }


        protected void UpdateRaycastOrigins()
        {
            Bounds bounds = collider.bounds;
            bounds.Expand(SKIN_WIDTH * -2);

            raycastOrigins.BottomLeft = new Vector2(bounds.min.x, bounds.min.z).ConvertVector2ToFlatVector3();
            raycastOrigins.BottomRight = new Vector2(bounds.max.x, bounds.min.z).ConvertVector2ToFlatVector3();
            raycastOrigins.TopLeft = new Vector2(bounds.min.x, bounds.max.z).ConvertVector2ToFlatVector3();
            raycastOrigins.TopRight = new Vector2(bounds.max.x, bounds.max.z).ConvertVector2ToFlatVector3();
        }

        protected void HorizontalCollisions(ref Vector3 velocity, int rayCount = 4)
        {
            var directionX = Mathf.Sign(velocity.x);
            var rayLength = Mathf.Abs(velocity.x) + SKIN_WIDTH;
            if (velocity.x == 0) return;

            for(int i = 0; i < rayCount; i++)
            {
                if (rayLength < 0.005)
                {
                    continue;
                }

                Vector3 rayOrigin = new Vector3(0,0,0);;
                if(directionX > 0)
                {
                    rayOrigin += Vector3.Lerp(raycastOrigins.TopRight, raycastOrigins.BottomRight, (i / (rayCount - 1f)));
                } 
                else if (directionX < 0)
                {
                    rayOrigin += Vector3.Lerp(raycastOrigins.TopLeft, raycastOrigins.BottomLeft, (i / (rayCount - 1f)));
                }

                Ray ray = new Ray(rayOrigin, Vector3.right * directionX);
                RaycastHit[] hits = Physics.RaycastAll(ray, rayLength, collisionLayer);

                Debug.DrawLine(rayOrigin,rayOrigin+Vector3.right * (directionX * rayLength),Color.red,.1f,false);
                
                if (hits.Length > 0)
                {
                    foreach (var hit in hits)
                    {
                        if (hit.collider.gameObject == gameObject) continue;

                            velocity.x = (hit.distance - SKIN_WIDTH) * directionX;
                            rayLength = hit.distance; // so that we dont calculate a higher velocity than the previous loops

                            if (!collisions.colliders.Contains(hit.collider))
                            {
                                collisions.colliders.Add(hit.collider);
                            }

                            collisions.left = directionX == -1;
                            collisions.right = directionX == 1;
                            
                            
                    }
                }
            }
        }

        protected void VerticalCollisions(ref Vector3 velocity, int rayCount = 4)
        {
            var directionZ = Mathf.Sign(velocity.z);
            var rayLength = Mathf.Abs(velocity.z) + SKIN_WIDTH;
            if (velocity.z == 0) return;

            for (int i = 0; i < rayCount; i++)
            {
                if (rayLength < 0.005)
                {
                    continue;
                }
                Vector3 rayOrigin = new Vector3(velocity.x,0,0);
                if (directionZ > 0)
                {
                    rayOrigin += Vector3.Lerp(raycastOrigins.TopLeft, raycastOrigins.TopRight, (i / (rayCount - 1f)));
                }
                else if (directionZ < 0)
                {
                    rayOrigin += Vector3.Lerp(raycastOrigins.BottomLeft, raycastOrigins.BottomRight, (i / (rayCount - 1f)));
                }

                Ray ray = new Ray(rayOrigin, Vector3.forward * directionZ);
                RaycastHit[] hits = Physics.RaycastAll(ray, rayLength, collisionLayer);
                Debug.DrawLine(rayOrigin,rayOrigin+Vector3.forward * (directionZ * rayLength),Color.red,.1f,false);

                if (hits.Length > 0)
                {
                    foreach (var hit in hits)
                    {
                        if (hit.collider.gameObject == gameObject) continue;

                        velocity.z = (hit.distance - SKIN_WIDTH) * directionZ;
                        rayLength = hit.distance; // so that we dont calculate a higher velocity than the previous loops
                        
                        if (!collisions.colliders.Contains(hit.collider))
                        {
                            collisions.colliders.Add(hit.collider);
                        }
                        collisions.below = directionZ == -1;
                        collisions.above = directionZ == 1;
                    }
                }
            }
        }
    }
    
    
    
    public struct RaycastOrigins
    {
        public Vector3 TopLeft, TopRight, BottomLeft, BottomRight;
    }
    
    public struct CollisionInfo
    {
        public bool above, below;
        public bool left, right;
        public List<Collider> colliders;
        
        public void Reset()
        {
            above = below = false;
            left = right = false;
            colliders ??= new List<Collider>();
            colliders.Clear();
        }

    }
}