using UnityEngine;
using UnityEngine.InputSystem;

namespace Clonk.CharacterInput
{
    public class TestKeyboardMouseInput : PublisherCharacterInput
    {
        public Vector3 moveInput;
        private Quaternion aimAngle = Quaternion.identity;


        void Update()
        {
            Plane playerPlane = new Plane(Vector3.up, transform.position);
            Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
            float hitDist = 0.0f;
            if (playerPlane.Raycast(ray,out hitDist))
            {
                Vector3 targetPoint = ray.GetPoint(hitDist);
                Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
                targetRotation.x = 0;
                targetRotation.z = 0;
                aimAngle = targetRotation;
                //Debug.DrawRay(transform.position, targetPoint);
            }

            MoveInput(moveInput);
            AimInput(aimAngle);
            
            //Pause
        }

        public void OnMoveInput(InputAction.CallbackContext ctx)
        {
            Vector2 inputValue = ctx.ReadValue<Vector2>();

            inputValue = inputValue.Rotate(-GameManager.instance.CameraManager.CameraOffset.rotation.eulerAngles.y);
            
            moveInput = new Vector3(inputValue.x, 0, inputValue.y);

        }

        public void OnFireInput(InputAction.CallbackContext ctx)
        {
            FireInput(ctx.ReadValueAsButton());
        }

        public void OnPauseInput(InputAction.CallbackContext ctx)
        {
            PauseInput(ctx.ReadValueAsButton());
            //GameManager.instance.isGamePaused
            
        }
    }
}