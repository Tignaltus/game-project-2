using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.InputSystem.EnhancedTouch;

namespace Clonk.CharacterInput
{
    public struct Joystick
    {
        public Vector3 positionStart;
        public Vector3 positionCurrent;
        public Action<Vector2,Vector2> onUpdated;

        public Vector3 getDirection(float deadZone = 0)
        {
            var direction = positionCurrent - positionStart;
            if(direction.magnitude < deadZone)
            {
                direction = Vector3.zero;
            }
            direction = (direction).normalized.Flatten(0);
            onUpdated?.Invoke(new Vector2(positionStart.x,positionStart.z),new Vector2(direction.x,direction.z));
            return direction;
        }

    }
    public class TouchInput : PublisherCharacterInput
    {
        public float deadZone = 5;
        public JoystickUI joystickPrefab;
        private Quaternion aimAngle = Quaternion.identity;

        private UnityEngine.InputSystem.TouchPhase touchPhase0;
        private UnityEngine.InputSystem.TouchPhase touchPhase1;

        private Joystick joystickMove = new Joystick() { positionStart = Vector3.zero, positionCurrent = Vector3.zero };
        private Joystick joystickAim = new Joystick() { positionStart = Vector3.zero, positionCurrent = Vector3.zero };

        private InputState touch0InputState = InputState.None;
        private InputState touch1InputState = InputState.None;

        
        
        
        public void CreateVisualJoysticks(Transform parent)
        {
            var movestick = Instantiate(joystickPrefab, parent);
            var aimstick = Instantiate(joystickPrefab, parent);
            
            joystickMove.onUpdated += movestick.UpdateJoystick;
            joystickAim.onUpdated += aimstick.UpdateJoystick;
        }

        void Update()
        {
            bool isMoving = false;
            bool isAiming = false;
            bool isFiring = false;

            if(touchPhase0 == UnityEngine.InputSystem.TouchPhase.Moved)
            {
                switch (touch0InputState)
                {
                    case InputState.Movement:
                        isMoving = true;
                        break;
                    case InputState.Aim:
                        isAiming = true;
                        break;
                }
            }
            else if(touchPhase0 != UnityEngine.InputSystem.TouchPhase.Began)
            {
                touch0InputState = InputState.None;
            }

            if (touchPhase1 == UnityEngine.InputSystem.TouchPhase.Moved)
            {
                switch (touch1InputState)
                {
                    case InputState.Movement:
                        isMoving = true;
                        break;
                    case InputState.Aim:
                        isAiming = true;
                        break;
                }
            }
            else if (touchPhase1 != UnityEngine.InputSystem.TouchPhase.Began)
            {
                touch1InputState = InputState.None;
            }

            if (isMoving)
            {
                var moveDirection = joystickMove.getDirection(deadZone);

                var inputValue = new Vector2(moveDirection.x, moveDirection.z);
                
                inputValue = inputValue.Rotate(-GameManager.instance.CameraManager.CameraOffset.rotation.eulerAngles.y);
                
                MoveInput(new Vector3(inputValue.x,0,inputValue.y));
               
            }
            else
            {
                MoveInput(Vector3.zero);
            }

            if (isAiming)
            {
                var aimDirection = joystickAim.getDirection(deadZone);
                var inputValue = new Vector2(aimDirection.x, aimDirection.z);
                inputValue = inputValue.Rotate(-GameManager.instance.CameraManager.CameraOffset.rotation.eulerAngles.y);
                
                
                aimAngle = Quaternion.LookRotation(new Vector3(inputValue.x,0,inputValue.y), Vector3.up);
                /*var offsetAngle = aimAngle;
                offsetAngle.eulerAngles = new Vector3(offsetAngle.eulerAngles.x,
                    offsetAngle.eulerAngles.y - GameManager.instance.CameraManager.CameraOffset.rotation.eulerAngles.y,
                    offsetAngle.eulerAngles.z);*/
                AimInput(aimAngle.normalized);
                isFiring = true;
            }

            FireInput(isFiring);

        }

        public void Touch0StartPos(InputAction.CallbackContext ctx)
        {
            Vector2 inputValue = ctx.ReadValue<Vector2>();
            if(inputValue.x < Screen.width / 2 && touch1InputState != InputState.Movement)
            {
                touch0InputState = InputState.Movement;
                joystickMove.positionStart = inputValue.ConvertVector2ToFlatVector3(0);
            } else if(touch1InputState != InputState.Aim)
            {
                touch0InputState = InputState.Aim;
                joystickAim.positionStart = inputValue.ConvertVector2ToFlatVector3(0);
            } else
            {
                touch0InputState = InputState.None;
            }
        }
        public void Touch0CurrentPos(InputAction.CallbackContext ctx)
        {
            Vector2 inputValue = ctx.ReadValue<Vector2>();
            switch (touch0InputState)
            {
                case InputState.Movement :
                    joystickMove.positionCurrent = inputValue.ConvertVector2ToFlatVector3();
                    break;
                case InputState.Aim :
                    joystickAim.positionCurrent = inputValue.ConvertVector2ToFlatVector3(0);
                    break;
            }
        }

        public void Touch1StartPos(InputAction.CallbackContext ctx)
        {
            Vector2 inputValue = ctx.ReadValue<Vector2>();
            if (inputValue.x < Screen.width / 2 && touch0InputState != InputState.Movement)
            {
                //movement
                touch1InputState = InputState.Movement;
                joystickMove.positionStart = inputValue.ConvertVector2ToFlatVector3(0);
            }
            else if(touch0InputState != InputState.Aim)
            {
                touch1InputState = InputState.Aim;
                joystickAim.positionStart = inputValue.ConvertVector2ToFlatVector3(0);
            } else
            {
                touch1InputState = InputState.None;
            }
        }
        public void Touch1CurrentPos(InputAction.CallbackContext ctx)
        {
            Vector2 inputValue = ctx.ReadValue<Vector2>();
            switch (touch1InputState)
            {
                case InputState.Movement:
                    joystickMove.positionCurrent = inputValue.ConvertVector2ToFlatVector3(0);
                    break;
                case InputState.Aim:
                    joystickAim.positionCurrent = inputValue.ConvertVector2ToFlatVector3(0);
                    break;
            }
        }
        public void Touch0Phase(InputAction.CallbackContext ctx)
        {
            touchPhase0 = ctx.ReadValue<UnityEngine.InputSystem.TouchPhase>();
            Debug.Log(touchPhase0);
        }

        public void Touch1Phase(InputAction.CallbackContext ctx)
        {
            touchPhase1 = ctx.ReadValue<UnityEngine.InputSystem.TouchPhase>();
            Debug.Log(touchPhase1);

        }
        private enum InputState
        {
            None, 
            Movement, 
            Aim
        }
    }
}

