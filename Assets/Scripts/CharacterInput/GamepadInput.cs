using UnityEngine;
using UnityEngine.InputSystem;

namespace Clonk.CharacterInput
{
    public class GamepadInput : PublisherCharacterInput
    {
        public Vector3 moveInput;
        public Quaternion aimInput = Quaternion.identity;

        void Update()
        {
            MoveInput(moveInput);
            AimInput(aimInput);
        }

        //this is for when using the new input system
        public void OnMoveInput(InputAction.CallbackContext ctx)
        {
            Vector2 inputValue = ctx.ReadValue<Vector2>();
            
            //move the player at the direction you input regardless of what the camera angle is
            inputValue = inputValue.Rotate(-GameManager.instance.CameraManager.CameraOffset.rotation.eulerAngles.y);

            moveInput = new Vector3(inputValue.x, 0, inputValue.y);

        }

        public void OnAimInput(InputAction.CallbackContext ctx)
        {
            Vector2 inputValue = ctx.ReadValue<Vector2>();
            
            
            inputValue = inputValue.Rotate(-GameManager.instance.CameraManager.CameraOffset.rotation.eulerAngles.y);


            if(!(inputValue.x == 0 && inputValue.y == 0))
            {
                var tempAimInput = new Vector3(inputValue.x, 0, inputValue.y);
                aimInput = Quaternion.LookRotation(tempAimInput, Vector3.up);
            }
            
        }

        public void OnFireInput(InputAction.CallbackContext ctx)
        {
            FireInput(ctx.ReadValueAsButton());
        }

        public void OnPauseInput(InputAction.CallbackContext ctx)
        {
            PauseInput(ctx.ReadValueAsButton());
        }
    }
}