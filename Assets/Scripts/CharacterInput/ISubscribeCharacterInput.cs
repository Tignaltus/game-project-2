namespace Clonk.CharacterInput
{
    public interface ISubscribeCharacterInput
    {
        void Subscribe(PublisherCharacterInput publisher);
        void Unsubscribe(PublisherCharacterInput publisher);
    }
}