using System;
using UnityEngine;

namespace Clonk.CharacterInput
{
    public class PublisherCharacterInput : MonoBehaviour
    {
        public Action<Vector3>     Move;
        public Action<Quaternion>  Aim;
        public Action<bool>        Fire;
        public Action<bool>        Pause;

        protected Vector3          pastMoveInput;
        protected Quaternion       pastAimInput;
        protected bool             pastFireInput;
        protected bool             pastPauseInput;

        protected Character myCharacter;
        
        protected void MoveInput(Vector3 moveInput)
        {
            if (pastMoveInput != moveInput)
            {
                Move?.Invoke(moveInput);
                pastMoveInput = moveInput;
            }
        }

        protected void AimInput(Quaternion aimInput)
        {
            if (pastAimInput != aimInput)
            {
                Aim?.Invoke(aimInput);
                pastAimInput = aimInput;
            }
        }

        protected void FireInput(bool fireInput)
        {
            if (pastFireInput != fireInput)
            {
                Fire?.Invoke(fireInput);
                pastFireInput = fireInput;
            }
        }

        protected void PauseInput(bool pauseInput)
        {
            if (pastPauseInput != pauseInput)
            {
                Pause?.Invoke(pauseInput);
                pastPauseInput = pauseInput;
            }
        }

        public void Initialize(Character character)
        {
            myCharacter = character;
        }
    }
}