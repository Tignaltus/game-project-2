using UnityEngine;
using UnityEngine.InputSystem;

namespace Clonk.CharacterInput
{
    public class PCInput : PublisherCharacterInput
    {
        public Vector3 moveInput;
        private Quaternion aimAngle = Quaternion.identity;
        public Quaternion aimInputGamepad = Quaternion.identity;

        void Update()
        {
            Plane playerPlane = new Plane(Vector3.up, transform.position);
            Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
            float hitDist = 0.0f;
            if (playerPlane.Raycast(ray,out hitDist))
            {
                Vector3 targetPoint = ray.GetPoint(hitDist);
                Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
                targetRotation.x = 0;
                targetRotation.z = 0;
                aimAngle = targetRotation;
            }

            MoveInput(moveInput);
            AimInput(aimAngle);
            //AimInput(aimInputGamepad);
        }

        public void OnMoveInput(InputAction.CallbackContext ctx)
        {
            Vector2 inputValue = ctx.ReadValue<Vector2>();
            moveInput = new Vector3(inputValue.x, 0, inputValue.y);

        }

        public void OnAimInputGamepad(InputAction.CallbackContext ctx)
        {
            Vector2 inputValue = ctx.ReadValue<Vector2>();

            if (!(inputValue.x == 0 && inputValue.y == 0))
            {
                var tempAimInput = new Vector3(inputValue.x, 0, inputValue.y);
                aimInputGamepad = Quaternion.LookRotation(tempAimInput, Vector3.up);
            }
        }

        public void OnFireInput(InputAction.CallbackContext ctx)
        {
            FireInput(ctx.ReadValueAsButton());
        }
    }
}