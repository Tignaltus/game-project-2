using System;
using System.Collections;
using System.Collections.Generic;
using Clonk;
using Clonk.CharacterInput;
using UnityEngine;
using Random = UnityEngine.Random;

public class AIManager : MonoBehaviour
{   
    public CharacterSpawner enemySpawner;
    public List<AIInputManager> enemyList;
    public List<AIInputManager> removeList = new List<AIInputManager>();
    public Action<List<PowerUpData>> onBossSpawned;

    // === Public Methods ===

    public void CreateEnemy(CharacterScriptableObject characterSO, Vector3 position, Quaternion rotation)
    {
        var ch = enemySpawner.Spawn(characterSO, position, rotation, 2);

        ch.Character.OnDeath += () =>
        {
            AudioManager.Instance.Play(ch.Character.DeathSound);
            removeList.Add(ch.Input as AIInputManager);
            //PowerUpManager.instance.SpawnNewPowerups();
        };
        ch.Character.onInitialized += (c) =>
        {
            foreach (var upgrade in PowerUpManager.GetEnemyPowerUps())
            {
                c.AddUpgrade(upgrade);
            }
        };


        enemyList.Add(ch.Input as AIInputManager);
    }
    public void CreateBoss(CharacterScriptableObject characterSO, Vector3 position, Quaternion rotation)
    {
        var ch = enemySpawner.Spawn(characterSO, position, rotation, 2);

        
        ch.Character.OnDeath += () =>
        {
            AudioManager.Instance.Play(ch.Character.DeathSound);
            removeList.Add(ch.Input as AIInputManager);
            //PowerUpManager.instance.SpawnNewPowerups();
        };
        ch.Character.onInitialized += (c) =>
        {
            foreach (var upgrade in PowerUpManager.GetBossPowerUps())
            {
                c.AddUpgrade(upgrade);
            }

            GameManager.instance.GameplayUIManager.BossHealthbar.UpdateHealthbarInstant(c.HealthCurrent, c.HealthMax);
            c.OnHealthUpdated += GameManager.instance.GameplayUIManager.BossHealthbar.UpdateHealthbar;
            GameManager.instance.GameplayUIManager.BossHealthbar.UpdateHealthbar(c.HealthCurrent, c.HealthMax);
            GameManager.instance.GameplayUIManager.ShowBossUI(true);
            onBossSpawned?.Invoke(c.GetUpgrades());
        };
        enemyList.Add(ch.Input as AIInputManager);
    }
    
    // === Private Methods ===

    private void FixedUpdate()
    {
        UpdateEnemy();
    }

    private void UpdateEnemy()
    {
        foreach (var enemy in removeList)
        {
            enemyList.Remove(enemy);
        }
        removeList.Clear();
        foreach (var enemy in enemyList)
        {
            enemy.UpdateAI(enemyList);
        }
    }
    
}
