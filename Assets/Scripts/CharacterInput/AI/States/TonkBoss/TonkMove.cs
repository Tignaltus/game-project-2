using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;

public class TonkMove : AIState
{
    /*private float timeUntilSpawn = 6;
    private bool spawnDone = true;*/

    public TonkIdle idleState;
    public TonkAction actionState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        inputManager.isPreformingAction = false;

        if (inputManager.target == null)
        {
            return idleState;
        }
        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) > inputManager.triggerAreaDistance)
        {
            inputManager.target = null;
            return idleState;
        }
        inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);
        var node = inputManager.navigator.path.corners[1];
        inputManager.movementCurrent = (node - character.transform.position).normalized;

        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) <= character.myWeapon.WeaponStats.GetStatValue(StatType.Range) + 2)
        {
            
            inputManager.isPreformingAction = true;
        }

        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) <= 7/*character.myWeapon.WeaponStats.GetStatValue(StatType.Range) -3*/)
        {
            return actionState;
        }

        /*timeUntilSpawn = spawnDone ? character.myWeapon.WeaponStats.GetStatValue(StatType.FireRate) : timeUntilSpawn - Time.fixedDeltaTime;
        spawnDone = (timeUntilSpawn <= 0);

        if (spawnDone)
        {
            character.enemySpawner.Spawn();
        }*/

        return this;
    }
}