using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrawlerAttack : AIState
{

    public BrawlerIdle idleState;
    public BrawlerChase chaseState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        inputManager.movementCurrent = Vector3.zero;
        inputManager.isPreformingAction = false;
        if (inputManager.target == null)
        {
            return idleState;
        }


        inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);

        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) <= 2.5f)
        {
            inputManager.isPreformingAction = true;
            return this;
        }

        return chaseState;
    }
}
