using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BrawlerIdle : AIState
{
    private float changeDirTime = 1;
    private float timeUntilChangeDir = 0f;
    private bool changeDirDone = false;
    Vector3 finalPosition = Vector3.zero;
    private Quaternion aimTo = Quaternion.identity;

    private bool inRange = false;

    public BrawlerChase chaseState;
    public BrawlerAttack attackState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        /*inputManager.isPreformingAction = false;
        inputManager.movementCurrent = Vector3.zero;
        if (inputManager.target == null)
        {
            return this;
        }
        var distance = Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position);

        var character = inputManager.GetCharacter();


        if (distance > inputManager.triggerAreaDistance)
        {
            inputManager.target = null;
            inRange = false;
        }
        else if (distance < inputManager.triggerAreaDistance)
        {
            inRange = true;
        }

        *//*if (inputManager.target == null || position < inputManager.FleeDistance)
        {
            //inputManager.target = null;
            return roamingState;
        }*//*


        if (!inRange) return this;
        //inputManager.GetCharacter().Animator.SetTrigger("Alarmed");
        return chaseState;*/

        if (inputManager.navigator == null)
        {
            return this;
        }
        inputManager.aimCurrent = Quaternion.Slerp(inputManager.aimCurrent, aimTo, 0.8f);
        inputManager.movementCurrent = inputManager.aimCurrent * Vector3.forward / 4;
        inputManager.isPreformingAction = false;

        timeUntilChangeDir = changeDirDone ? changeDirTime : timeUntilChangeDir - Time.fixedDeltaTime;
        changeDirDone = (timeUntilChangeDir <= 0);
        if (changeDirDone)
        {
            inputManager.navigator.SetDestination(RandomNavmeshLocation(inputManager.GetCharacter().transform, 45f));
            changeDirTime = UnityEngine.Random.Range(0.8f, 1.5f);
            //Debug.Log(inputManager.navigator.destination);
        }
        if (inputManager.navigator.path.corners.Length >= 2)
        {
            var node = inputManager.navigator.path.corners[1];
            var dirVector = (node - inputManager.GetCharacter().transform.position).normalized;
            aimTo = ExtensionMethods.DirectionFromVector(dirVector);
        }


        if (inputManager.target == null)
        {
            return this;
        }
        var distance = Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position);


        if (distance > inputManager.triggerAreaDistance)
        {
            inputManager.target = null;
            inRange = false;
        }
        else if (distance < inputManager.triggerAreaDistance)
        {
            inRange = true;
        }

        if (!inRange) return this;
        return chaseState;

    }

    public Vector3 RandomNavmeshLocation(Transform transform, float radius)
    {
        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;

        NavMesh.SamplePosition(randomDirection, out hit, radius, 1);

        finalPosition = hit.position;

        return finalPosition;
    }
}
