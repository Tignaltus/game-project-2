using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JuggerCharging : AIState
{

    private bool chargeDone = false;
    private float defaultTime = 2f;
    private float timeUntilAttack = 2f;
    
    public JuggerAttack attackState;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        
        inputManager.movementCurrent = Vector3.zero; 
        
        //look at where the target is there is a timer ticking down so when its on zero the enemy will be in attack state
        timeUntilAttack = chargeDone ? defaultTime : timeUntilAttack - Time.fixedDeltaTime;
        chargeDone = (timeUntilAttack<=0);
        
        if (character != null && inputManager.target != null)
        {
            inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position,inputManager.target.transform.position);
        }
        inputManager.isPreformingAction = false;

        if (chargeDone)
        {
            //character.characterMovement.AddKnockback(inputManager.aimCurrent*Vector3.forward.SetMagnitude(10).Flatten(0));
            character.characterMovement.AddKnockback(inputManager.aimCurrent*Vector3.forward.SetMagnitude(character.GetStatValue(StatType.MovementSpeed)).Flatten(0));
            /*character.rotationSpeed = 0;
            inputManager.movementCurrent = inputManager.aimCurrent * Vector3.forward;*/
            return attackState;
        }
        return this;
    }
}
