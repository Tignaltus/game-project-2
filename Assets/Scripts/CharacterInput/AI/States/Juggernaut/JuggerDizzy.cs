using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JuggerDizzy : AIState
{
    private bool dizzyDone = false;
    private float defaultTime = 2;
    private float timeUntilNormal = 2;

    public JuggerIdle idleState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        inputManager.movementCurrent = Vector3.zero;
        timeUntilNormal = dizzyDone ? defaultTime : timeUntilNormal - Time.fixedDeltaTime;
        dizzyDone = (timeUntilNormal<=0);
        
        inputManager.isPreformingAction = false;
        
        if (!dizzyDone) return this;
        inputManager.GetCharacter().Animator?.SetBool("Dizzy", false);
        return idleState;
    }
}
