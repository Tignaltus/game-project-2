using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JuggerRoaming : AIState
{

    public JuggerIdle idleState;
    //public JuggerRoaming roamingState;
    public JuggerCharging chargingState;
    public JuggerAttack attackState;
    public JuggerDizzy dizzyState;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        inputManager.isPreformingAction = false;

        if (inputManager.target == null)
        {
            return idleState;
        }
        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) > inputManager.triggerAreaDistance)
        {
            inputManager.target = null;
            return idleState;
        }
        /*var node = inputManager.navigator.path.corners[1];
        inputManager.movementCurrent = (node - character.transform.position).normalized;
        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) < character.myWeapon.WeaponStats.GetStatValue(StatType.Range))
        {
            return chargingState;
        }*/
        return this;
    }
}
