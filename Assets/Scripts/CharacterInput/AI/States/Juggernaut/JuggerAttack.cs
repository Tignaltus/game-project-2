using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JuggerAttack : AIState
{
    public LayerMask collisionLayer;
    public JuggerDizzy dizzyState;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        inputManager.movementCurrent = inputManager.aimCurrent * Vector3.forward;
        var character = inputManager.GetCharacter();
        
        //if the jugger touches something example: a wall, player or an enemy it will become dizzy and deal damage to player or enemy. and deal knockback
        AIState returnState = this;
        if(character.characterMovement.collisions.colliders.Count > 0)
        {
            foreach(var collider in character.characterMovement.collisions.colliders)
            {
                var entity = collider.GetComponent<Entity>();
                if (entity != null)
                {
                    HitEntity(character,entity,character.myWeapon.WeaponStats.GetStatValue(StatType.Damage)*2, inputManager.aimCurrent * Vector3.forward);
                }
            }
            returnState = dizzyState;
            //effects and animations that plays
            character.characterMovement.AddKnockback(-inputManager.movementCurrent.SetMagnitude(10));
            GameManager.instance.EffectManager.CreateEffect(EffectType.StunEffect,character.transform.position.Flatten(2),Quaternion.identity, Vector3.one,character.transform);
            AudioManager.Instance.Play(AudioManager.Sounds.Impact_Juggernaut);
            inputManager.GetCharacter().Animator?.SetTrigger("Impact");
            inputManager.GetCharacter().Animator?.SetBool("Dizzy", true);
        }
        
        inputManager.isPreformingAction = false;
        
        return returnState;
    }
    private void HitEntity(Character character,Entity otherEntity, float damage, Vector3 knockback)
    {
        otherEntity.ChangeHealth(-damage, true);
        if (otherEntity is Character otherCharacter)
        {
            Vector3 knockbackDir = otherCharacter.transform.position.Flatten() - character.transform.position.Flatten();
            //otherCharacter.characterMovement.AddKnockback(knockbackDir.SetMagnitude(30));
            otherCharacter.characterMovement.AddKnockback(knockback * character.myWeapon.WeaponStats.GetStatValue(StatType.Knockback));
        }

    }
}
