using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WizzardAction : AIState
{
    private bool shootDone = false;
    private float defaultTime = 2f;
    private float timeUntilAttack = 2f;

    Vector3 finalPosition = Vector3.zero;
    public AIState idleState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();

        inputManager.movementCurrent = Vector3.zero;
        timeUntilAttack = shootDone ? defaultTime : timeUntilAttack - Time.fixedDeltaTime;
        shootDone = (timeUntilAttack <= 0);
        if (character != null && inputManager.target != null)
        {
            inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);
        }
        inputManager.isPreformingAction = false;

        if (shootDone)
        {
            inputManager.isPreformingAction = true;
            inputManager.navigator.SetDestination(RandomNavmeshLocation(inputManager.GetCharacter().transform, 45f));
            //inputManager.transform.(inputManager.navigator.destination);
            //inputManager.transform.localPosition = inputManager.navigator.destination;
            //inputManager.transform.position = inputManager.navigator.destination;

            //inputManager.transform.position = new Vector3(inputManager.navigator.destination.x, inputManager.navigator.destination.y, inputManager.navigator.destination.z);
            //inputManager.transform.position.Equals(inputManager.navigator.destination);
            inputManager.transform.position.Set(inputManager.navigator.destination.x, inputManager.navigator.destination.y, inputManager.navigator.destination.z);
            //inputManager.movementCurrent = new Vector3(inputManager.navigator.destination.x, 0, inputManager.navigator.destination.z) * 10;
            Debug.Log(inputManager.navigator.destination);
            return idleState;
        }
        return this;
    }

    public Vector3 RandomNavmeshLocation(Transform transform, float radius)
    {
        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;

        NavMesh.SamplePosition(randomDirection, out hit, radius, 1);

        finalPosition = hit.position;

        return finalPosition;
    }

}
