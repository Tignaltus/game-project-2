using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;

public abstract class AIState : MonoBehaviour
{
    //public abstract AIState StateBehaviour(Character character, ref Character targetObject, ref float alarmRange);
    public abstract AIState StateBehaviour(AIInputManager inputManager);
}
