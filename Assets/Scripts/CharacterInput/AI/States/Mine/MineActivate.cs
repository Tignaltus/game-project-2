using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineActivate : AIState
{
    public AIState mineTouching;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        return mineTouching;
    }
}
