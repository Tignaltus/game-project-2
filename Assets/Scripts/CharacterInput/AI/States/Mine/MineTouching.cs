using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;

public class MineTouching : AIState
{
    public AIState activateState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();

        
        //inputManager.movementCurrent = Vector3.zero;

        if (character != null && inputManager.target != null)
        {
            var node = inputManager.navigator.path.corners[1];
            inputManager.movementCurrent = (node - character.transform.position).normalized;
            inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);
        }


        if (character.characterMovement.collisions.colliders.Count > 0)
        {
            foreach (var collider in character.characterMovement.collisions.colliders) //if bombot collides with something
            {
                var entity = collider.GetComponent<Entity>();
                if (entity != null)
                {
                    //entity.ChangeHealth(-character.myWeapon.WeaponStats.GetStatValue(StatType.Damage), true);
                    HitEntity(entity, character.myWeapon.WeaponStats.GetStatValue(StatType.Damage)); //hit the character
                    character.ChangeHealth(0, false); //kill the mine
                }
            }
        }

        return this;
    }

    private void HitEntity(Entity otherEntity, float damage)
    {
        otherEntity.ChangeHealth(-damage, true); //deal damage to the character bombot hits
    }
}
