using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombExplode : AIState
{
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        inputManager.GetCharacter().ChangeHealth(0, false);
        return this;
    }
}
