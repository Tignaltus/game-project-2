using System;
using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class StateMove : AIState
{
    public StateAction actionState;
    public StateIdle idleState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        inputManager.isPreformingAction = false;
        
        if (inputManager.target == null)
        {
            return idleState;
        }
        if(Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) > inputManager.triggerAreaDistance)
        {
            inputManager.target = null;
            return idleState;
        }
        var node = inputManager.navigator.path.corners[1];
        inputManager.movementCurrent = (node - character.transform.position).normalized;
        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) < character.myWeapon.WeaponStats.GetStatValue(StatType.Range))
        {
            return actionState;
        }
        return this;
    }
}
