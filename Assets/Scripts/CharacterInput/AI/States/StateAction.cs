using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;

public class StateAction : AIState
{
    public StateMove moveState;
    public StateIdle idleState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        inputManager.movementCurrent = Vector3.zero;
        inputManager.isPreformingAction = false;
        if (inputManager.target == null)
        {
            return idleState;
        }
        
        
        inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position,inputManager.target.transform.position);
        
        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) <= character.myWeapon.WeaponStats.GetStatValue(StatType.Range))
        {
            inputManager.isPreformingAction = true;
            return this;
        }
        
        return moveState;
    }

}
