using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;

public class BombbotAction : AIState
{

    public AIState moveState;
    public AIState idleState;
    
    private bool inRange = true;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var target = inputManager.target;
        var character = inputManager.GetCharacter();
        
        if (target == null) return idleState;

        if (Vector3.Distance(character.transform.position, target.transform.position) >= inputManager.triggerAreaDistance / 5 * 2)
        {
            inRange = false;
        }
        else if (Vector3.Distance(character.transform.position, target.transform.position) <= inputManager.triggerAreaDistance / 5)
        {
            inRange = true;
        }
        

        if (character != null && target != null)
        {
            inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position,target.transform.position);
        }


        if (character.characterMovement.collisions.colliders.Count > 0)
        {
            foreach (var collider in character.characterMovement.collisions.colliders)
            {
                var entity = collider.GetComponent<Entity>();
                if (entity != null)
                {
                    HitEntity(entity, character.myWeapon.WeaponStats.GetStatValue(StatType.Damage));
                    character.ChangeHealth(0, false);
                }
            }
        }
        //inputManager.isPreformingAction = true;
        return !inRange ? moveState : this;
    }

    private void HitEntity(Entity otherEntity, float damage)
    {
        otherEntity.ChangeHealth(-damage, true);
    }
}
