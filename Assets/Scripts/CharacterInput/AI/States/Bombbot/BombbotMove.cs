using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;

public class BombbotMove : AIState
{
    public AIState actionState;
    public AIState idleState;
    public AIState alarmedState;
    private bool inRange = false;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {

        var character = inputManager.GetCharacter();
        var node = inputManager.navigator.path.corners[1];
        inputManager.movementCurrent = (node - character.transform.position).normalized;

        /*if (inputManager.target == null)
        {
            return idleState;
        }*/

        /*if (Vector3.Distance(character.transform.position, inputManager.target.transform.position) > inputManager.triggerAreaDistance)
        {
            inputManager.target = null;
            return idleState;
        }*/

        if (character != null && inputManager.target != null)
        {
            inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);
        }

        if (character.characterMovement.collisions.colliders.Count > 0) 
        {
            foreach (var collider in character.characterMovement.collisions.colliders) //if bombot collides with something
            {
                var entity = collider.GetComponent<Entity>();
                if (entity != null)
                {
                    HitEntity(entity, character.myWeapon.WeaponStats.GetStatValue(StatType.Damage)); //hit the character
                    character.ChangeHealth(0, false); //kill the bombot
                }
            }
        }

        return this;
    }

    private void HitEntity(Entity otherEntity, float damage)
    {
        otherEntity.ChangeHealth(-damage, true); //deal damage to the character bombot hits
    }
}
