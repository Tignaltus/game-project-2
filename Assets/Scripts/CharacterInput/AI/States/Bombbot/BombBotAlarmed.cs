using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBotAlarmed : AIState
{

    private bool chargeDone = false;
    private float defaultTime = 1f;
    private float timeUntilAttack = 1f;

    public AIState idleState;

    public AIState moveState;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();

        inputManager.movementCurrent = Vector3.zero;

        timeUntilAttack = chargeDone ? defaultTime : timeUntilAttack - Time.fixedDeltaTime;
        chargeDone = (timeUntilAttack <= 0);

        if (character != null && inputManager.target != null)
        {
            inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);
        }
        inputManager.isPreformingAction = false;

        if (chargeDone)
            return moveState;
        return this;
    }
}
