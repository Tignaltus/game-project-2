using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;

public class BombbotIdle : AIState
{

    public AIState moveState;
    public AIState alarmedState;

    private bool inRange = false;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        //inputManager.GetCharacter().ChangeHealth(0, false);
        if (inputManager.target == null)
        {
            inRange = false;
        }
        else if(Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) > inputManager.triggerAreaDistance)
        {
            inRange = false;
        }
        else
        {
            inRange = true;
            GameManager.instance.EffectManager.CreateEffect(EffectType.AlertEffect,inputManager.GetCharacter().transform.position.Flatten(3),Quaternion.identity, Vector3.one*1.5f);
            AudioManager.Instance.Play(AudioManager.Sounds.Alarmed_BoomBot);
        }
        inputManager.movementCurrent = Vector3.zero;
        inputManager.isPreformingAction = false;
        
        
        return inRange ? alarmedState : this;
    }

}
