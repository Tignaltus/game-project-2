using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TornShooting : AIState
{
    //float timeSinceLastCall;
    public TornIdle idleState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        float rotationSpeed = 1;
        /*if (inputManager.GetCharacter().myWeapon == null)
        {
            return idleState;
        }*/
        //float timeSinceLastCall;

        //inputManager.GetCharacter().myWeapon.sh

        /*if (0.8f >= (character.HealthCurrent / character.HealthMax))
        {
            //character.boxCollider.center = new Vector3(0, character.boxCollider.center.y + 1.5f, 0);
            //character.transform.localPosition = new Vector3(0, -1.6f, 0);
        }*/

        inputManager.aimCurrent.eulerAngles = new Vector3(0, inputManager.aimCurrent.eulerAngles.y + rotationSpeed * Time.fixedDeltaTime, 0);
        inputManager.movementCurrent = Vector3.zero;

        inputManager.isPreformingAction = true;


        /*timeSinceLastCall += Time.fixedDeltaTime;
        if (timeSinceLastCall >= 2)
        {
            Debug.Log("Test");
            timeSinceLastCall = 0;   // reset timer back to 0
        }*/
        return this;
    }

}
