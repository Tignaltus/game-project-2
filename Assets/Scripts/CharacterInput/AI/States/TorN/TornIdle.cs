using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TornIdle : AIState
{

    public TornShooting shootingState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        if (!inputManager.GetCharacter().imobilized)
        {
            return shootingState;
        }
        return this;
    }
}
