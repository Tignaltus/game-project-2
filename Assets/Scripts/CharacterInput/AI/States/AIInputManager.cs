using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Clonk;
using Clonk.CharacterInput;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.PlayerLoop;
using UnityEngine.Serialization;

[RequireComponent(typeof(NavMeshAgent))]
public class AIInputManager : PublisherCharacterInput
{
    public AIState currentState;
    public Character target;
    public NavMeshAgent navigator;

    public bool isPreformingAction;
    public Vector3 movementCurrent;
    public Quaternion aimCurrent;
    public LayerMask targetLayer;
    
    
    public float triggerAreaDistance = 10;
    public float FleeDistance = 10;

    private void Awake()
    {
        navigator = gameObject.GetComponent<NavMeshAgent>();
        
        navigator.speed = 0;
        navigator.updateRotation = false;
    }

    public void UpdateAI(List<AIInputManager> listOfEnemies)
    {
        if (target == null)
        {
            target = FindTarget(triggerAreaDistance);

        }
        else if (target.IsDead)
        {
            target = FindTarget(triggerAreaDistance);
            if (target != null)
            {
                navigator.SetDestination(target.gameObject.transform.position);
            }
        }

        if (target != null)
        {
            navigator.SetDestination(target.gameObject.transform.position);
        }

        AIState returnState = currentState?.StateBehaviour(this);

        if (returnState != currentState)
        {
            ChangeState(returnState);
        }

        if (currentState != null)
        {            
            AimInput(aimCurrent);
            MoveInput(movementCurrent);
            FireInput(isPreformingAction);
        }
    }

    public void ChangeState(AIState newState)
    {
        currentState = newState;
    }

    public Character GetCharacter()
    {
        return myCharacter;
    }

    private Character FindTarget(float alarmRange = 10)
    {
        if (myCharacter == null)
        {
            return null;
        }

        var foundColliders = Physics.OverlapSphere(myCharacter.transform.position, alarmRange, targetLayer);
        var closesetDistance = alarmRange;

        Character target = null;
        foreach (var collider in foundColliders)
        {
            var entity = collider.GetComponent<Character>();
            if (entity != null)
            {
                if (entity.Team != myCharacter.Team && entity != myCharacter && !entity.IsDead)
                {
                    var distance = Vector3.Distance(myCharacter.transform.position.Flatten(), entity.transform.position.Flatten());
                    if (distance < closesetDistance)
                    {
                        closesetDistance = distance;
                        target = entity;
                    }
                }
            }
        }
        return target;
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(1f, 1f, 1f, .05f);
        Gizmos.DrawWireSphere(myCharacter.transform.position, triggerAreaDistance);
        Gizmos.DrawWireSphere(myCharacter.transform.position, FleeDistance);
    }
}
