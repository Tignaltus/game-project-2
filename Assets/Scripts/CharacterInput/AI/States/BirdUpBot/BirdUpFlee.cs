using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;

public class BirdUpFlee : AIState
{

    public BirdUpIdle idleState;
    public BirdUpMove moveState;
    public BirdUpAction actionState;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();

        if (character != null && inputManager.target != null)
        {
            inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);
            inputManager.movementCurrent = inputManager.aimCurrent * Vector3.forward * -1f;
        }
        /*inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);
        inputManager.movementCurrent = inputManager.aimCurrent * Vector3.forward * -1f;*/
        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) < inputManager.FleeDistance)
        {
            return actionState;
        }
        return this;
    }
}
