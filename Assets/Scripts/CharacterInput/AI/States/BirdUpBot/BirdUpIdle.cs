using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BirdUpIdle : AIState
{
    private float changeDirTime = 1;
    private float timeUntilChangeDir = 0f;
    private bool changeDirDone = false;
    Vector3 finalPosition = Vector3.zero;
    private Quaternion aimTo = Quaternion.identity;

    private bool inRange = false;

    public BirdUpMove moveState;
    public BirdUpAction actionState;
    public BirdUpFlee fleeState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        //character = inputManager.GetCharacter();
        /*inputManager.isPreformingAction = false;
        inputManager.movementCurrent = Vector3.zero;

        if (inputManager.target == null)
        {
            return this;
        }
        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) < inputManager.triggerAreaDistance)
        {
            return moveState;
        }
        return this;*/

        if (inputManager.navigator == null)
        {
            return this;
        }
        inputManager.aimCurrent = Quaternion.Slerp(inputManager.aimCurrent, aimTo, 0.8f);
        inputManager.movementCurrent = inputManager.aimCurrent * Vector3.forward / 2;
        inputManager.isPreformingAction = false;

        timeUntilChangeDir = changeDirDone ? changeDirTime : timeUntilChangeDir - Time.fixedDeltaTime;
        changeDirDone = (timeUntilChangeDir <= 0);
        if (changeDirDone)
        {
            inputManager.navigator.SetDestination(RandomNavmeshLocation(inputManager.GetCharacter().transform, 25f));
            changeDirTime = UnityEngine.Random.Range(0.8f, 1.3f);
        }
        if (inputManager.navigator.path.corners.Length >= 2)
        {
            var node = inputManager.navigator.path.corners[1];
            var dirVector = (node - inputManager.GetCharacter().transform.position).normalized;
            aimTo = ExtensionMethods.DirectionFromVector(dirVector);
        }


        if (inputManager.target == null)
        {
            return this;
        }
        var distance = Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position);


        if (distance > inputManager.triggerAreaDistance)
        {
            inputManager.target = null;
            inRange = false;
        }
        else if (distance < inputManager.triggerAreaDistance)
        {
            inRange = true;
        }

        if (!inRange) return this;
        return actionState;
    }

    public Vector3 RandomNavmeshLocation(Transform transform, float radius)
    {
        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;

        NavMesh.SamplePosition(randomDirection, out hit, radius, 1);

        finalPosition = hit.position;

        return finalPosition;
    }
}
