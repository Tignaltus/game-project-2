using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;
using UnityEngine.AI;

public class BirdUpAction : AIState
{
    private bool movingRight = true;

    public BirdUpIdle idleState;
    public BirdUpMove moveState;
    public BirdUpFlee fleeState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        //inputManager.movementCurrent = Vector3.zero;
        inputManager.isPreformingAction = false;
        if (inputManager.target == null)
        {
            return idleState;
        }

        //look at the player
        inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);

        //if the player is in the enemys flee distance
        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) <= inputManager.FleeDistance)
        {
            //move right while looking at the player if movingRight is false then it moves left
            //sets a navigation that the player moves to
            inputManager.navigator.SetDestination(GetFleePosition(character, Quaternion.LookRotation(inputManager.aimCurrent * (movingRight ? Vector3.right : Vector3.left), Vector3.up)));
            if(Random.Range(0f,  200f) > 199f) //changes movingRight bool radomly
            {
                movingRight = !movingRight;
            }
            var node = inputManager.navigator.path.corners[1];
            inputManager.movementCurrent = (node - character.transform.position).normalized; //moves the player


            inputManager.isPreformingAction = true; //isPreformingAction makes it so the enemy shoots
            return this;
        }

        return moveState;
    }

    public Vector3 GetFleePosition(Character character, Quaternion fleeDirection)
    {
        Vector3 fleeVector = fleeDirection * Vector3.back * character.GetStatValue(StatType.MovementSpeed) * 3;
        fleeVector += character.transform.position;
        NavMeshHit hit;

        NavMesh.SamplePosition(fleeVector, out hit, character.GetStatValue(StatType.MovementSpeed) * 3, 1);

        return hit.position;
    }
}
