using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;

public class StateIdle : AIState
{
    private Quaternion currentAim;
    private Character target;
    private Character character;

    public StateMove moveState;
    public StateAction actionState;
    
    private bool inRange = false;
    private float speed = 20;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        
        character = inputManager.GetCharacter();
        inputManager.isPreformingAction = false;
        inputManager.movementCurrent = Vector3.zero;

        if (inputManager.target == null)
        {
            return this;
        }
        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) < inputManager.triggerAreaDistance)
        {
            return moveState;
        }
        return this;
    }
}
