using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;

public class SmolTonkMove : AIState
{

    public SmolTonkIdle idleState;
    public SmolTonkAction actionState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        inputManager.isPreformingAction = false;

        if (inputManager.target == null)
        {
            return idleState;
        }
        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) > inputManager.triggerAreaDistance)
        {
            inputManager.target = null;
            return idleState;
        }
        inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);
        /*var node = inputManager.navigator.path.corners[1];
        inputManager.movementCurrent = (node - character.transform.position).normalized;*/
        if (inputManager.navigator.path.corners.Length > 1)
        {
            var node = inputManager.navigator.path.corners[1];
            inputManager.movementCurrent = (node - character.transform.position).normalized;
        }

        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) <= character.myWeapon.WeaponStats.GetStatValue(StatType.Range) - 0.5f)
        {
            inputManager.isPreformingAction = true;
        }

        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) <= 2/*character.myWeapon.WeaponStats.GetStatValue(StatType.Range) -3*/)
        {
            return actionState;
        }
        return this;
    }
}
