using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;

public class SmolTonkIdle : AIState
{
    public float changeDirTime = 2;
    public float timeUntilChangeDir;
    private bool changeDirDone = true;

    public float randomNumber;

    public SmolTonkMove moveState;
    public SmolTonkAction actionState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {

        var character = inputManager.GetCharacter();
        inputManager.isPreformingAction = false;

        inputManager.movementCurrent = inputManager.aimCurrent * Vector3.forward / 2;

        timeUntilChangeDir = changeDirDone ? changeDirTime : timeUntilChangeDir - Time.fixedDeltaTime;
        changeDirDone = (timeUntilChangeDir <= 0);
        if (changeDirDone)
        {
            /*randomXnumber = Random.Range(-360.0f, 360.0f);
            randomZnumber = Random.Range(-360.0f, 360.0f);*/
            randomNumber = Random.Range(0f, 360f);
        }

        //inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, new Vector3(randomXnumber, 0, randomZnumber).normalized);
        inputManager.aimCurrent = Quaternion.Euler(0, randomNumber, 0);

        if (inputManager.target == null)
        {
            return this;
        }
        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) < inputManager.triggerAreaDistance)
        {
            return moveState;
        }
        return this;
    }
}
