using System.Collections;
using System.Collections.Generic;
using Clonk;
using UnityEngine;

public class SmolTonkAction : AIState
{

    public SmolTonkIdle idleState;
    public SmolTonkMove moveState;
    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        inputManager.movementCurrent = Vector3.zero;
        inputManager.isPreformingAction = false;
        if (inputManager.target == null)
        {
            return idleState;
        }


        inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);

        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) <= 3/*character.myWeapon.WeaponStats.GetStatValue(StatType.Range) - 2*/)
        {
            inputManager.isPreformingAction = true;
            return this;
        }

        /*if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) < inputManager.FleeDistance)
        {
            
            return fleeState;
        }*/

        return moveState;
    }
}
