using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperIdle : AIState
{

    private float changeDirTime = 2;
    private float timeUntilChangeDir;
    private bool changeDirDone = true;
    private float randomNumber;

    private bool inRange = false;

    private bool fleeRange = false;
    private bool aimRange = false;

    public SniperAim aimState;
    public SniperFlee fleeState;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        inputManager.isPreformingAction = false;
        inputManager.movementCurrent = Vector3.zero;

        timeUntilChangeDir = changeDirDone ? changeDirTime : timeUntilChangeDir - Time.fixedDeltaTime;
        changeDirDone = (timeUntilChangeDir <= 0);
        if (changeDirDone)
        {
            /*randomXnumber = Random.Range(-360.0f, 360.0f);
            randomZnumber = Random.Range(-360.0f, 360.0f);*/
            randomNumber = Random.Range(0f, 360f);
            changeDirTime = Random.Range(1f, 2f);
        }
        inputManager.aimCurrent = Quaternion.Euler(0, randomNumber, 0);

        if (inputManager.target == null)
        {
            return this;
        }

        if(Vector3.Distance(character.transform.position, inputManager.target.transform.position) > inputManager.triggerAreaDistance)
        {
            inputManager.target = null;
            return this;
        }
        else if (Vector3.Distance(character.transform.position, inputManager.target.transform.position) < inputManager.triggerAreaDistance)
        {
            inRange = true;
            if (Vector3.Distance(character.transform.position, inputManager.target.transform.position) < inputManager.FleeDistance)
            {
                fleeRange = true;
                aimRange = false;
            }
            else
            {
                aimRange = true;
                fleeRange = false;
            }
        }
        if (aimRange && !fleeRange)
        {
            return aimState;
        }
        if (fleeRange && !aimRange)
        {
            return fleeState;
        }

        return this;
    }
}
