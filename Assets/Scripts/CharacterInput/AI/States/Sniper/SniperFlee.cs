using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SniperFlee : AIState
{

    private bool inShootRange = false;

    private bool shootDone = true;
    private float timeUntilShoot = 3;

    public SniperIdle idleState;
    public SniperAim aimState;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        inputManager.isPreformingAction = false;
        
        if (inputManager.target == null)
            return idleState;
        
        if (Vector3.Distance(character.transform.position, inputManager.target.transform.position) > inputManager.triggerAreaDistance)
        {
            inputManager.target = null;
            return idleState;
        }

        if (Vector3.Distance(character.transform.position, inputManager.target.transform.position) > inputManager.FleeDistance)
        {
            //inShootRange = Vector3.Distance(character.transform.position, inputManager.target.transform.position) > character.myWeapon.WeaponStats.GetStatValue(StatType.Range);
            inShootRange = true;
        }
        else
        {
            inShootRange = false;
        }

        if (inShootRange)
        {

            return idleState;
        }
        /*if(inputManager.target != null)
        {

        }*/
        inputManager.navigator.SetDestination(GetFleePosition(character, inputManager.aimCurrent));
        //var node = inputManager.navigator.path.corners[1];
        inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);

        if (inputManager.navigator.path.corners.Length > 1)
        {
            var node = inputManager.navigator.path.corners[1];
            inputManager.movementCurrent = (node - character.transform.position).normalized;
            //inputManager.movementCurrent = (node - character.transform.position).normalized;
        }
        //inputManager.movementCurrent = (node - character.transform.position).normalized;

        /*timeUntilShoot = shootDone ? character.myWeapon.WeaponStats.GetStatValue(StatType.FireRate) * 3 : timeUntilShoot - Time.fixedDeltaTime;
        shootDone = (timeUntilShoot <= 0);
        inputManager.isPreformingAction = shootDone;*/

        return this;
    }


    public Vector3 GetFleePosition(Character character, Quaternion fleeDirection)
    {
        Vector3 fleeVector = fleeDirection * Vector3.back * character.GetStatValue(StatType.MovementSpeed) * 3;  
        fleeVector += character.transform.position;
        NavMeshHit hit;

        NavMesh.SamplePosition(fleeVector, out hit, character.GetStatValue(StatType.MovementSpeed) * 3, 1);

        return hit.position;
    }
}
