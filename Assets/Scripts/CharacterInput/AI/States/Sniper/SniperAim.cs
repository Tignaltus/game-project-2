using Clonk;
using Clonk.Weapons;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SniperAim : AIState
{

    private bool inShootRange;
    private bool shootDone = true;
    private float defaultTime = 3;
    private float timeUntilShoot = 3;



    public SniperIdle idleState;
    public SniperFlee fleeState;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        inputManager.isPreformingAction = false;


        if (inputManager.target == null)
        {
            return idleState;
        }
        
        inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position,inputManager.target.transform.position);

        if (Vector3.Distance(character.transform.position, inputManager.target.transform.position) > character.myWeapon.WeaponStats.GetStatValue(StatType.Range) /*inputManager.FleeDistance*/)
        {
            inputManager.target = null;
            inShootRange = false;
            return idleState;
        }

        if (Vector3.Distance(character.transform.position, inputManager.target.transform.position) > inputManager.FleeDistance)
        {
            inShootRange = true;
        }
        else
        {
            inputManager.target = null;
            inShootRange = false;
        }


        timeUntilShoot = shootDone ? character.myWeapon.WeaponStats.GetStatValue(StatType.FireRate) : timeUntilShoot - Time.fixedDeltaTime;
        shootDone = (timeUntilShoot<=0);
        inputManager.isPreformingAction = shootDone;
        if (inShootRange || !shootDone)
        {
            return this;
        }
        return idleState;
    }
}
