using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAction : AIState
{

    public TurretIdle idleState;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        inputManager.isPreformingAction = false;


        if (inputManager.target == null)
        {
            inputManager.GetCharacter().Animator.SetBool("Open", false);
            return idleState;
        }
        
        inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position,inputManager.target.transform.position);
        
        if (Vector3.Distance(character.transform.position, inputManager.target.transform.position) > inputManager.triggerAreaDistance)
        {
            inputManager.target = null;
            inputManager.GetCharacter().Animator.SetBool("Open", false);
            return idleState;
        }

        if (inputManager.target == null) 
            return this;
        
        if (Vector3.Distance(character.transform.position, inputManager.target.transform.position) < inputManager.triggerAreaDistance)
        {
            inputManager.isPreformingAction = Vector3.Distance(character.transform.position, inputManager.target.transform.position) < character.myWeapon.WeaponStats.GetStatValue(StatType.Range);
        }

        return this;
    }
}
