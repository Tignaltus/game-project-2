using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretIdle : AIState
{
    public TurretAction actionState;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        inputManager.isPreformingAction = false;

        if (inputManager.target == null)
        {
            return this;
        }
        if (Vector3.Distance(inputManager.GetCharacter().transform.position, inputManager.target.transform.position) < inputManager.triggerAreaDistance)
        {
            inputManager.GetCharacter().Animator.SetBool("Open", true);
            return actionState;
        }
        return this;
    }
}
