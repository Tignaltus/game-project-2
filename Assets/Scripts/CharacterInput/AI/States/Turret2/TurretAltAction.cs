using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretAltAction : AIState
{
    private Quaternion currentAim;
    public AIState idleState;

    public override AIState StateBehaviour(AIInputManager inputManager)
    {
        var character = inputManager.GetCharacter();
        inputManager.isPreformingAction = false;


        if (inputManager.target == null)
        {
            inputManager.GetCharacter().Animator.SetBool("Open", false);
            return idleState;
        }

        //inputManager.aimCurrent = ExtensionMethods.DirectionBetweenTwoFlatPoints(character.transform.position, inputManager.target.transform.position);
        //inputManager.aimCurrent.eulerAngles = new Vector3(0, 230 * Time.deltaTime, 0);
        
        //inputManager.aimCurrent = Quaternion.Euler(0, 5 * Time.deltaTime, 0);

        var temprotation = currentAim;
        temprotation.eulerAngles = new Vector3(0, temprotation.eulerAngles.y + 200 * Time.fixedDeltaTime, 0);
        currentAim = temprotation;
        inputManager.aimCurrent = currentAim;
        //Debug.Log(inputManager.aimCurrent);


        if (Vector3.Distance(character.transform.position, inputManager.target.transform.position) > inputManager.triggerAreaDistance)
        {
            inputManager.target = null;
            inputManager.GetCharacter().Animator.SetBool("Open", false);
            return idleState;
        }

        if (inputManager.target == null)
            return this;

        if (Vector3.Distance(character.transform.position, inputManager.target.transform.position) < inputManager.triggerAreaDistance)
        {
            inputManager.isPreformingAction = Vector3.Distance(character.transform.position, inputManager.target.transform.position) < character.myWeapon.WeaponStats.GetStatValue(StatType.Range);
        }

        return this;
    }
}