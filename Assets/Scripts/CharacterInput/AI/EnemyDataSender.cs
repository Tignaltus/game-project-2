using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

public class EnemyDataSender : MonoBehaviour
{
    [SerializeField]private Vector3 spawnPosition;
    [SerializeField]private Quaternion spawnRotation;
    [SerializeField]private int amount = 0;
    [SerializeField]private float width_distance = 15;
    [SerializeField]private float height_distance = 5;
    
    public CharacterScriptableObject[] enemyType;
    public AIManager manager;

    private void Start()
    {
        for (int i = 0; i < amount; i++)
        {
            spawnPosition = new Vector3(Random.Range(-width_distance,width_distance), 0, Random.Range(-height_distance, height_distance));
            manager.CreateEnemy(enemyType[Random.Range(0, enemyType.Length)], spawnPosition, spawnRotation);
        }
    }
}
