using System;
using System.Collections;
using System.Collections.Generic;
using Clonk;
using Clonk.CharacterInput;
using Unity.Mathematics;
using UnityEngine;

//TODO: Choose team, and find the ones in the opposite team
//TODO: Make decisions based on situation and purpose

public class AIInput : PublisherCharacterInput
{
    public GameObject target;
    private Quaternion currentAim;

    public float speed;

    //=== Private Methods ===
    
    private void Start() 
    {
        //TODO: Define target
        //currentAim = Quaternion.identity;
    }

    private Quaternion Aiming()
    {
        if (myCharacter != null && target != null)
        {
            Vector3 targetPos = new Vector3(target.transform.position.x, 0, target.transform.position.z);
            Vector3 myPos = new Vector3(myCharacter.transform.position.x, 0, myCharacter.transform.position.z);
            
            Vector3 direction = (targetPos - myPos).normalized;
            currentAim = Quaternion.LookRotation(direction, Vector3.up);
        }

        return currentAim;
    }
    
    private Vector3 Movement()
    {
        Vector3 direction = currentAim * Vector3.forward;

        return direction * speed * Time.deltaTime;
    }

    private new bool Fire()
    {
        return false;
    }
    
    //=== Public Methods ===
        
    public void AIUpdate()
    {            
        AimInput(Aiming());
        MoveInput(Movement());
        //FireInput(Fire());
    }

    private void FixedUpdate() //Temporary
    {
        AIUpdate();
    }
}
