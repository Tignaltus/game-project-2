using System;
using UnityEngine;
using System.Collections;

namespace Clonk
{
    public static class ExtensionMethods
    {
        public static Vector3 Flatten(this Vector3 vector)
        {
            return new Vector3(vector.x, 0, vector.z);
        }
        public static Vector3 Flatten(this Vector3 vector, float y)
        {
            return new Vector3(vector.x, y, vector.z);
        }

        public static Vector2 FlatVector3ToVector2(this Vector3 vector)
        {
            return new Vector2(vector.x, vector.z);
        }

        public static Vector3 ConvertVector2ToFlatVector3(this Vector2 vector, float y = .1f)
        {
            return new Vector3(vector.x, y, vector.y);
        }

        public static Quaternion DirectionFromVector(Vector3 a)
        {
            
            return Quaternion.LookRotation(a.Flatten(0).normalized, Vector3.up);
        }

        public static Quaternion DirectionBetweenTwoFlatPoints(Vector3 a, Vector3 b)
        {
            Vector3 direction = (b.Flatten() - a.Flatten()).normalized;
            return Quaternion.LookRotation(direction, Vector3.up);
        }

        public static Vector3 SetMagnitude(this Vector3 vector3, float magnitude)
        {
            var returnVector = vector3.normalized * magnitude;
            return returnVector;
        }

        public static Vector2 Rotate(this Vector2 v, float degrees) {
            float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
            float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);
         
            float tx = v.x;
            float ty = v.y;
            v.x = (cos * tx) - (sin * ty);
            v.y = (sin * tx) + (cos * ty);
            return v;
        }
        
    }
}