using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpPanel : MonoBehaviour
{
    public PowerUpData powerUpData {get; private set;}
    public PowerUpData healing { get; private set; }

    [SerializeField] private Image[] Icons;
    [SerializeField] private TextMeshProUGUI Name;
    [SerializeField] private TextMeshProUGUI Description;

    public Action<PowerUpData> onClicked;


    public void Initialize(PowerUpData pd, Action<PowerUpData> onClicked)
    {
        powerUpData = pd;
        foreach (var Icon in Icons)
        {
            Icon.sprite = pd.powerUpImage;
        }
        Name.text = pd.name;
        Description.text = pd.description;
        this.onClicked = onClicked;
    }

    public void PowerUpPressed()
    {
        /*if(powerUpData.typeID == 1)
        {
            Debug.Log("healing");
            //onClicked?.Invoke(healing);
        } else
        {
            onClicked?.Invoke(powerUpData);
        }*/
        onClicked?.Invoke(powerUpData);
    }
    
    













}
