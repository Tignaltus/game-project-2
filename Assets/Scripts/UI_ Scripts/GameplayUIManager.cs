using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

public class GameplayUIManager : MonoBehaviour
{
    private enum GameState
    {
        Playing,
        Paused,
        GameOver
    }


    public GameObject playingUI;
    public GameObject gameOverUI;
    public GameObject pausedUI;
    public GameObject gameManager;
    public GameObject gameOverUIButton;

    public GameObject pausedUINoButton;
    [Space]

    public UIMinimap minimap;

    [Space] 
    public GameObject BossUI;
    public PlayerHealthbar BossHealthbar;

    [Space]
    public TMP_Text finalScore;
    public TMP_Text finalRoom;
    public TMP_Text maxCombo;
    public TMP_Text GameOverText;


    public bool isGameoverTriggered = false;
    private void Update()
    {
        if (GameManager.instance.GetPlayer().Character.IsDead && !isGameoverTriggered)
        {
            isGameoverTriggered = true;
            Debug.Log("You died, right??");
            StartCoroutine("Die");
            //TriggerGameOver();
        }
    }

    IEnumerator Die()
    {
        yield return StartCoroutine(One());
        yield return StartCoroutine(Two());
    }

    IEnumerator One()
    {
        yield return new WaitForSeconds(3f);
    }
    IEnumerator Two()
    {
        TriggerGameOver();
        yield return null;
    }

    public void TriggerGameOver(string text = "GaMe OveR") //formatted like this because it looks the best with the font
    {
        BossUI.SetActive(false);
        isGameoverTriggered = true;
        GameOverText.text = text;
        playingUI.SetActive(false);
        pausedUI.SetActive(false);
        
        GameManager.instance.PauseGame();
        gameOverUI.SetActive(true);
        if(gameOverUIButton != null)
        {
            EventSystem.current.SetSelectedGameObject(gameOverUIButton);
        }

        string scoreText = GameManager.instance.ScoreManager.GetCurrentScore().ToString("D10");

        string formattedText = "<color=#222222>";
        bool scoreStart = false;
        foreach (var character in scoreText)
        {
            if (!scoreStart && character != '0')
            {
                scoreStart = true;
                formattedText += "</color>";
            }

            formattedText += character;
        }

        maxCombo.text = GameManager.instance.ScoreManager.MaxCombo.ToString();
        finalScore.text = formattedText;
        //finalRoom.text = GameManager.instance.RoomManager.GetAmmountOfRooms().ToString();
    }

    public void UnloadGameplay()
    {
        SceneManager.UnloadSceneAsync("Gameplay_Scene");
        GameManager.instance.UnPauseGame();
        if (gameManager != null)
        {
            Destroy(gameManager);
        }
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("Main_Menu");
    }

    public void ShowBossUI(bool active)
    {
        BossUI.SetActive(active);
    }

    public void TriggerPause()
    {
        pausedUI.SetActive(!pausedUI.activeInHierarchy);
        if (pausedUINoButton != null)
        {
            EventSystem.current.SetSelectedGameObject(pausedUINoButton);
        }
    }
}
