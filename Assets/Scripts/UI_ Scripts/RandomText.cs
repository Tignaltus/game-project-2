using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class RandomText : MonoBehaviour
{

    public List<string> GameOverQuotes;
    private TextMeshProUGUI textBox;

    // Start is called before the first frame update
    void Start()
    {
        textBox = GetComponent<TextMeshProUGUI>();
        textBox.text = GameOverQuotes[Random.Range(0, GameOverQuotes.Count)];
    }

    public string PrefixText()
    {
        string prefix = GameOverQuotes[Random.Range(0, GameOverQuotes.Count)];
        return prefix;
    }
}
