using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Icon_Rotate : MonoBehaviour

{
    public AnimationCurve animCurve;
    private float n = 0;
    public float rotation;
    void Update()
    {
        n += Time.deltaTime;
        n = Mathf.Repeat(n, 1);
        transform.Rotate(new Vector3(0f, 0f, animCurve.Evaluate(n)*rotation));
    }

    private void Start()
    {
        
    }
}

