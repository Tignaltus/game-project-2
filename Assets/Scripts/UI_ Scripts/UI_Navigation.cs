using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class UI_Navigation : MonoBehaviour
{
    
 public void LoadInGameScene()
    {
        SceneManager.LoadScene("Gameplay_Scene");
    }

    public void UnloadScene()
    {
        //SceneManager.UnloadSceneAsync("Main_Menu");
    }
}
