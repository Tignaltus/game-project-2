using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.UIElements;
public class Disp_Room_Info : MonoBehaviour
{
    public TMP_Text RoomCounter;
    public TMP_Text RoomDifficulty;

    public void Display(int roomCount, int roomDiff)
    {
        RoomCounter.text = "R00m: "  +  roomCount;
        RoomDifficulty.text = "R00m Difficulty: " + roomDiff;
    }
   

}
