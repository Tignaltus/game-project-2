using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;


//Placeholder for code:&& !GameManager.instance.isGamePaused//
  
public class Countdown_Clock : MonoBehaviour
{
    public TMP_Text textTimer;
    public TMP_Text flavorText;
    public GameObject UI_Cogs;

    private float timer;
    public float timerStartValue = 421.0f;
    public float flavorTextVisibilityTime = 5f;
    public float fadeTime = 3f;
    private bool IsCountingDown = false;
    public Action OnCountdownDone;
    private bool IsCogsGone = false;
    private bool StartFade = false;

    private void Start()
    {
        timer = timerStartValue;
        IsCountingDown = true;
        textTimer.CrossFadeAlpha(0, 0, true);
        flavorText.CrossFadeAlpha(1, 0, true);
    }
    void Update()
    {
       if(IsCountingDown && !GameManager.instance.isGamePaused)
       {
           timer -= Time.deltaTime;
           if(timer <= timerStartValue - flavorTextVisibilityTime && !StartFade)
           {
               SwitchFade();
               StartFade = true;
           }

           DisplayTime();
       }

       if (!(timer <= 0f) || IsCogsGone) return;
       timer = 0f;
       IsCountingDown = false;
       OnCountdownDone?.Invoke();
       textTimer.alpha = 0;
       IsCogsGone = true;
       UI_Cogs.SetActive(false);


    }

    private void SwitchFade()
    {
        flavorText.CrossFadeAlpha(0, fadeTime * 0.5f, true);
        textTimer.CrossFadeAlpha(1, fadeTime, true);

    }
    void DisplayTime()
    { 
        int minutes = Mathf.FloorToInt(timer / 60.0f);
        int seconds = Mathf.FloorToInt(timer - minutes * 60);
        textTimer.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    public void StartTimer()
    { 
        IsCountingDown = true; 
    }

    public void StopTimer()
    {
        IsCountingDown = false;      
    }
}
