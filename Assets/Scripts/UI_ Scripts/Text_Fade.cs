using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Text_Fade : MonoBehaviour
{
    public TMP_Text Text;
    public bool show;

    public void TextFadeOut()

    {       
        {Text.CrossFadeAlpha(0f, 1f, false);}
    }

}
