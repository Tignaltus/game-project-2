using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

public class LightFader : MonoBehaviour
{
    private Light[] lights;
    private Dictionary<Light, float> lightBaseIntensity = new Dictionary<Light, float>();


    private void Start()
    {
        lights = GetComponentsInChildren<Light>();
        foreach (var light in lights)
        {
            lightBaseIntensity.Add(light,light.intensity);
            light.intensity = 0;
        }
    }

    public void SetFade(bool LightsOn)
    {
        StartCoroutine(Fade(LightsOn));
    }

    private IEnumerator Fade(bool LightsOn)
    {
        if (lights == null) yield break;
        for (float ft = 0f; ft <= 1; ft += .5f * Time.deltaTime)
        {
            foreach (var light in lights)
            {
                float value = 0;
                lightBaseIntensity.TryGetValue(light, out value);
                var intesityTo = LightsOn ? value : 0f;
                var intesityFrom = !LightsOn ? value : 0f;
                light.intensity = Mathf.Lerp(intesityFrom, intesityTo, ft);
            }

            yield return null;
        }
    }




}
