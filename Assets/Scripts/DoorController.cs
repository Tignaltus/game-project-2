using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{

    private Animator animator;

    

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if(animator != null)
        {
            animator.SetBool("Open", !(GameManager.instance.AIManager.enemyList.Count > 0));
        }
    }
}
