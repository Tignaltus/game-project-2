using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
#if UNITY_EDITOR
using UnityEditorInternal;
#endif
public class EnumFlagsAttribute : PropertyAttribute {
    public EnumFlagsAttribute() { }
}

#if UNITY_EDITOR
namespace Clonk.Internal
{

    using UnityEditor;

    [CustomPropertyDrawer(typeof(EnumFlagsAttribute))]
    public class EnumFlagsAttributeDrawer : PropertyDrawer {
        
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return 100.0f;
        }
        
        public override void OnGUI(Rect pos, SerializedProperty prop, GUIContent label) {
            // -1 accounts for the Default value wich should always be 0, thus not displayed
            int perRow = 3;
            
            bool[] buttons = new bool[prop.enumNames.Length - 1];
            int rows = buttons.Length / (Math.Max(1,perRow-1)); 
            float buttonWidth = ((pos.width - (label == GUIContent.none ? 0 : EditorGUIUtility.labelWidth)) / (buttons.Length+1)) *(rows-1);
            float buttonHeight = (pos.height / rows);
            if (label != GUIContent.none) {
                EditorGUI.LabelField(new Rect(pos.x, pos.y, EditorGUIUtility.labelWidth, pos.height), label);
            }

            // Handle button value
            EditorGUI.BeginChangeCheck();

            int row = 0;
            int rowindex = 0;
            int buttonsValue = 0;
            for (int i = 0; i < buttons.Length; i++)
            {
                if (rowindex >= perRow)
                {
                    rowindex = 0;
                    row++;
                }

                // Check if the button is/was pressed 
                if ((prop.intValue & (1 << i)) == (1 << i)) {
                    buttons[i] = true;
                }

                // Display the button
                Rect buttonPos = new Rect(pos.x + (label == GUIContent.none ? 0 : EditorGUIUtility.labelWidth) + buttonWidth * rowindex, pos.y+ row * buttonHeight, buttonWidth, buttonHeight);
                buttons[i] = GUI.Toggle(buttonPos, buttons[i], prop.enumNames[i + 1], "Button");

                if (buttons[i]) {
                    buttonsValue += 1 << i;
                }
                
                rowindex++;
            }

            // This is set to true if a control changed in the previous BeginChangeCheck block
            if (EditorGUI.EndChangeCheck()) {
                prop.intValue = buttonsValue;
            }
        }
    }
}
#endif