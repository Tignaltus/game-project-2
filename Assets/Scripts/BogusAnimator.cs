using Clonk;
using Clonk.Animators;
using UnityEngine;

public class BogusAnimator : CharacterAnimator
{
    public Transform Rotator;


    private void Start()
    {
        AudioManager.Instance.Stop(AudioManager.Sounds.Music_Clonk);
        AudioManager.Instance.Play(AudioManager.Sounds.Music_Bogus);


        GetComponent<Character>().OnDeath += () =>
        {
            AudioManager.Instance.Stop(AudioManager.Sounds.Music_Bogus);
            AudioManager.Instance.Play(AudioManager.Sounds.Music_Clonk);
        };
    }

    protected void Update()
    {
        base.Update();
        Quaternion rotation = aimDirection;
        rotation.eulerAngles = new Vector3(aimDirection.eulerAngles.x, aimDirection.eulerAngles.y + offsetAngle, aimDirection.eulerAngles.z);
        Rotator.rotation = rotation;
    }

}
