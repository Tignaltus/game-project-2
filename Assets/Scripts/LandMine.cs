using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandMine : Entity
{
    public GameObject explosionPrefab;
    // Start is called before the first frame update
    void Start()
    {
        Physics.IgnoreLayerCollision(10, 8);
    }

    // Update is called once per frame
    void Update()
    {
        /*if(healthCurrent <= 0)
        {
            //Instantiate(explosionPrefab, transform.position, transform.rotation);
            ObjectPooler.SharedInstance.GetPooledObject(explosionPrefab, transform.position, transform.rotation);
            //Destroy(gameObject, 0);
            ObjectPooler.SharedInstance.Deactivate(gameObject, 0);
        }*/
    }
    private void HitEntity(Entity otherEntity)
    {
        //otherEntity.ChangeHealth(-damage, true);
        /*if (otherEntity is Character character)
        {
            Instantiate(explosionPrefab, transform.position, transform.rotation);
            Destroy(gameObject, 0);
        }*/

        /*Instantiate(explosionPrefab, transform.position, transform.rotation);
        Destroy(gameObject, 0);*/

        healthCurrent = 0;

        if(healthCurrent <= 0)
        {
            Instantiate(explosionPrefab, transform.position, transform.rotation);
            //ObjectPooler.SharedInstance.GetPooledObject(explosionPrefab, transform.position, transform.rotation);
            AudioManager.Instance.Play(AudioManager.Sounds.Boombot_Explode);
            Destroy(gameObject, 0);
            //ObjectPooler.SharedInstance.Deactivate(gameObject, 0);
        }

        /*if (otherEntity is Projectile)
        {
            Debug.Log("ded by explosion");
        }*/
    }

    private void OnTriggerEnter(Collider other)
    {

        var hitEntity = other.GetComponent<Entity>();

        if (hitEntity != null)
        {
            HitEntity(hitEntity);
        }
    }
}
