using Clonk;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DangerGrates : MonoBehaviour
{
    public float damage = 0.2f;
    public float delay = .1f;
    public float activeTime = 2;
    public BoxCollider boxCollider;
    public MeshRenderer meshRenderer;

    public List<Entity> entityQueue;

    void Start()
    {
        InvokeRepeating("EnableCollision", 5, 5);
    }

    void EnableCollision()
    {
        if(boxCollider != null)
        {
            boxCollider.enabled = true;
        }
        if(meshRenderer != null)
        {
            meshRenderer.enabled = true;
        }
        
        StartCoroutine(RemoveCollision(activeTime));
    }

    private void HitEntity(Entity otherEntity)
    {
        otherEntity.ChangeHealth(-damage, true);
        if (otherEntity is Character character)
        {
            character.Animator.DoSquash();
        }
    }

    IEnumerator RemoveCollision(float delay)
    {
        yield return new WaitForSeconds(delay);
        if(boxCollider != null)
        {
            boxCollider.enabled = false;
        }
        if(meshRenderer != null)
        {
            meshRenderer.enabled = false;
        }
        
    }

    IEnumerator removeCoroutine(float delay, Entity entityToRemove)
    {
        yield return new WaitForSeconds(delay);
        entityQueue.Remove(entityToRemove);
    }

    private void OnTriggerStay(Collider other)
    {

        var hitEntity = other.GetComponent<Entity>();

        if (hitEntity != null && !entityQueue.Contains(hitEntity))
        {
            GameManager.instance.EffectManager.CreateEffect(EffectType.GrindEffect, hitEntity.transform.position.Flatten(0), Quaternion.identity, Vector3.one);
            AudioManager.Instance.Play(AudioManager.Sounds.Grinder);
            HitEntity(hitEntity);
            entityQueue.Add(hitEntity);
            StartCoroutine(removeCoroutine(delay, hitEntity));
        }
    }
}
