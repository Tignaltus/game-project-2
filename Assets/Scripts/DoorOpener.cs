using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class DoorOpener : MonoBehaviour
{
    public Transform lid1, lid2;
    public Collider entrance;
    
    public float speed = 0.01f;

    //public bool Open;
    
    public bool Open
    {
        get => !entrance.enabled;
        set => entrance.enabled = !value;
    }
    
    private float doorSpeed;
    
    void Awake()
    {
        entrance = GetComponent<Collider>();
    }
    
    private void Update()
    {
        //Debug.Log(lid1.localPosition.x);
        if(Open && lid1.localPosition.x <= 0)
        {
            doorSpeed = speed;
        } else if (Open && lid1.localPosition.x >= 1)
        {
            doorSpeed = 0;
        }

        if(!Open && lid1.localPosition.x >= 1)
        {
            doorSpeed = -speed;
        } else if (!Open && lid1.localPosition.x <= 0)
        {
            doorSpeed = 0;
        }

        lid1.Translate(doorSpeed, 0, 0);
        lid2.Translate(doorSpeed, 0, 0);
    }
}
